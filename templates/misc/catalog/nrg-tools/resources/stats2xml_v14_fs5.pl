#!/usr/bin/perl
# $Id: stats2xml.pl,v 1.5 2007/03/20 16:29:23 karchie Exp $
# Copyright (c) 2007 Washington University
# Author: Kevin A. Archie <karchie@npg.wustl.edu>

# Modified by MRH on 2011-03-28 to include output directory as input parameter

#use strict;
use Getopt::Std;

#use lib "/data/cninds01/data2/nrg-tools/fs2xml";
use lib "/data/intradb/pipeline/nrg-tools";
require FreeSurfer::Stats;

my $VC_ID = '$Id: stats2xml.pl,v 1.5 2007/03/20 16:29:23 karchie Exp $';

my %opts;
getopts('p:x:t:b:d:o:hv', \%opts);

if (exists $opts{v}) {
   print STDOUT "$VC_ID\n"; 
   exit 0;
}


if (exists $opts{h}) {
    print STDERR <<EndOfHelp;
$VC_ID
Generates XNAT XML from FreeSurfer stats files
Options:

  -p <project>   Define project name (required).
  -x <XNAT ID>   Define XNAT ID of the MRSession  (required).
  -t <Freesurfer or LongitudinalFS> Define which type of XML you want to create
  -b Path to FREESURFER_HOME
  -d Datetime stamp
  -o Output directory (Optional)

  -h             Show this help information
EndOfHelp
}



my $project = $opts{p};
my $xnat_id = $opts{x};
my $xmlType = $opts{t};
my $freesurfer_home = $opts{b};
my $datetime = $opts{d};
my $outdir = $opts{o};
print "$freesurfer_home is $opts{b}\n";
die "No project defined (use -p option)\n" unless defined $project;
die "No XNAT ID defined (use -x option)\n" unless defined $xnat_id;
die "No XMLTYPE defined (use -t option [Freesurfer|LongitudinalFS)\n" unless defined $xmlType;
die "No FREESURFER_HOME defined (use -b option)\n" unless defined $freesurfer_home;
die "No datestamp supplied for the assessor ID (use -d option)\n" unless defined $datetime;


print STDOUT "all right then.\n";

my @provenance_fields = (
    ['prov:program', 'generating_program', 'version', 'program_version',
     'arguments', 'cmdargs'],
    ['prov:timestamp', 'timestamp'],
    ['prov:cvs', 'cvs_version'],
    ['prov:user', 'user'],
    ['prov:machine', 'hostname'],
    ['prov:platform', 'sysname'],
    );
    
my @aseg_in_files = (
    ['segmentation volume', 'SegVolFile'],
    ['in volume', 'InVolFile'],
    ['pv volume', 'PVVolFile'],
    );

my @aseg_parameters = (
    ['InVolFrame', 'InVolFrame'],
    ['ExcludeSegId', 'ExcludeSegId'],
    ['VoxelVolume_mm3', 'VoxelVolume_mm3'],
    );

my @aseg_global_measures = (
    ['fs:ICV', 'ICV'],
['fs:lhCortexVol','lhCortexVol'], 
['fs:rhCortexVol','rhCortexVol'], 
['fs:CortexVol', 'CortexVol'], 
['fs:SubCortGrayVol', 'SubCortGrayVol'], 
['fs:TotalGrayVol', 'TotalGrayVol'],
['fs:SupraTentorialVol','SupraTentorialVol'], 
['fs:lhCorticalWhiteMatterVol','lhCorticalWhiteMatterVol'], 
['fs:rhCorticalWhiteMatterVol', 'rhCorticalWhiteMatterVol'],
['fs:CorticalWhiteMatterVol','CorticalWhiteMatterVol'], 

    );

my @aseg_region_attrs = (
    ['SegId', 'SegId'],
    ['name', 'StructName']
);

my @aseg_region_measures = (
    ['fs:NVoxels', 'NVoxels'],
    ['fs:Volume', 'Volume_mm3'],
    ['fs:normMean', 'normMean'],
    ['fs:normStdDev', 'normStdDev'],
    ['fs:normMin', 'normMin'],
    ['fs:normMax', 'normMax'],
    ['fs:normRange', 'normRange'],
    );


my @aparc_in_files = (
    ['annotation', 'AnnotationFile'],
    );

my @aparc_parameters = ();

my @aparc_global_measures = (
    ['fs:NumVert', 'NumVert'],
    ['fs:SurfArea', 'SurfArea'],
    );

my @aparc_region_attrs = (
    ['name', 'StructName'],
);

my @aparc_region_measures = (
    ['fs:NumVert', 'NumVert'],
    ['fs:SurfArea', 'SurfArea'],
    ['fs:GrayVol', 'GrayVol'],
    ['fs:ThickAvg', 'ThickAvg'],
    ['fs:ThickStd', 'ThickStd'],
    ['fs:MeanCurv', 'MeanCurv'],
    ['fs:GausCurv', 'GausCurv'],
    ['fs:FoldInd', 'FoldInd'],
    ['fs:CurvInd', 'CurvInd'],
);


# arguments:
# filename
# file type ("aseg"/"aparc")
# provenance fields (array ref)
# in_files descs (array ref)
# parameters desc (array ref)
# global measures desc (array ref)
# region attributes desc (array ref)
# region measures desc (array ref)


sub insertAsegProvenance($$$) {
    my ($XMLFILE, $meta, $provenance_fields) = @_;
    print $XMLFILE "\t\t<prov:processStep>\n";

    for my $tagdef (@{$provenance_fields}) {
	next unless exists $meta->{$tagdef->[1]};
	print $XMLFILE "\t\t\t<", $tagdef->[0];
	for (my $attr_idx = 2; $attr_idx <= $#{$tagdef}; $attr_idx += 2) {
	    print $XMLFILE ' ', $tagdef->[$attr_idx], '="',
	    $meta->{$tagdef->[$attr_idx+1]}, '"';
	}
	print $XMLFILE ">", $meta->{$tagdef->[1]}, "</", $tagdef->[0], ">\n";
    }
    print $XMLFILE "\t\t</prov:processStep>\n";

}



sub insertAparcProvenance($$$) {
    my ($XMLFILE, $aparc_meta_ref, $provenance_fields) = @_;
    my %lr = ( 'lh' => 'left', 'rh' => 'right' );
    my %aparc_meta = %$aparc_meta_ref;
	
	for my $hemi ( keys %lr ) {
	    print XMLFILE "\t\t<prov:processStep>\n";
            my $aparc_hemi_meta = ${$aparc_meta{$hemi}};
	    for my $tagdef (@{$provenance_fields}) {
	        next unless exists $aparc_hemi_meta->{$tagdef->[1]};
		print $XMLFILE "\t\t\t<", $tagdef->[0];
		for (my $attr_idx = 2; $attr_idx <= $#{$tagdef}; $attr_idx += 2) {
		    print $XMLFILE ' ', $tagdef->[$attr_idx], '="',
		    $aparc_hemi_meta->{$tagdef->[$attr_idx+1]}, '"';
		}
		print $XMLFILE ">", $aparc_hemi_meta->{$tagdef->[1]}, "</", $tagdef->[0], ">\n";
	    }
	    print $XMLFILE "\t\t</prov:processStep>\n";
	}  	
}

sub insertAsegInFiles($$$$) {
    my ($XMLFILE, $meta, $in_files, $root_folder) = @_;

	    for my $tagdef (@{$in_files}) {
		print XMLFILE "\t\t<xnat:file xsi:type=\"xnat:imageResource\" ";
		print XMLFILE 'content="', $tagdef->[0], '" ';
		print XMLFILE 'format="MGZ" ';
		print XMLFILE 'URI="', $root_folder, '/',
					# ### dir should be same as location
					# ### of aseg file instead?
		$meta->{$tagdef->[1]}, '"/>', "\n";
	    }

}

sub insertAparcInFiles($$$$) {
    my ($XMLFILE, $aparc_meta_ref, $in_files, $root_folder) = @_;
    my %lr = ( 'lh' => 'left', 'rh' => 'right' );
    my %aparc_meta = %$aparc_meta_ref; 
	for my $hemi ( keys %lr ) {
            my $aparc_hemi_meta = ${$aparc_meta{$hemi}};
	    for my $tagdef (@{$in_files}) {
                my $path = $aparc_hemi_meta->{$tagdef->[1]}; $path =~s/^\.\.\///;
		print XMLFILE "\t\t<xnat:file xsi:type=\"xnat:imageResource\" ";
		print XMLFILE 'content="', $tagdef->[0], '" ';
		print XMLFILE 'format="MGZ" ';
		print XMLFILE 'URI="', $root_folder, '/',
					# ### dir should be same as location
					# ### of aseg file instead?
		$path, '"/>', "\n";

	    }
	 }
}

sub writeParameters($$$) {
  my ($XMLFILE,$meta, $parameters) =@_;
   if ($#{$parameters} >= 0) {
	for my $tagdef (@{$parameters}) {
	    print $XMLFILE "\t\t<xnat:addParam name=\"$tagdef->[0]\">";
	    print $XMLFILE $meta->{$tagdef->[1]};
	    print $XMLFILE "</xnat:addParam>\n";
	}
    }

}

sub insertAsegMeasures($$$$$$) {
    my ($XMLFILE, $meta, $results, $global_measures, $region_attrs, $region_measures) = @_;
	    for my $measure (@{$global_measures}) {
		print $XMLFILE "\t<$measure->[0]>";
		my ($val, $units) = @{$results->{GLOBALS}->{$measure->[1]}};
	        print $XMLFILE $val;
		print $XMLFILE "</$measure->[0]>\n";
	    }

	    print $XMLFILE "\t<fs:regions>\n";
	    for my $region (@{$results->{REGIONS}}) {
		print XMLFILE "\t\t<fs:region";
		for my $attr (@{$region_attrs}) {
		    print XMLFILE " $attr->[0]=\"$region->{$attr->[1]}\"";
		}

		# hemisphere is a little complicated, sadly.
		    my $hemisphere;
		    if ($region->{StructName} =~ /\ALeft-/) {
			$hemisphere = 'left';
		    } elsif ($region->{StructName} =~ /\ARight-/) {
			$hemisphere = 'right';
		    }
		    print $XMLFILE " hemisphere=\"$hemisphere\"" if defined $hemisphere;

		print $XMLFILE ">\n";

		for my $measure (@{$region_measures}) {
		    print $XMLFILE "\t\t\t<$measure->[0]>";
		    print $XMLFILE $region->{$measure->[1]};
	            print $XMLFILE "</$measure->[0]>\n";
		}
		print $XMLFILE "\t\t</fs:region>\n";
	    }
	    print $XMLFILE "\t</fs:regions>\n";

}

sub insertAparcMeasures($$$$$) {
    my ($XMLFILE,  $aparc_results_ref, $global_measures, $region_attrs, $region_measures) = @_;
    my %lr = ( 'lh' => 'left', 'rh' => 'right' );
    my %aparc_results = %$aparc_results_ref;

	for my $hemi ( keys %lr ) {
	        my $aparc_hemi_results = ${$aparc_results{$hemi}};

		print XMLFILE "\t<fs:hemisphere name=\"".$lr{$hemi} ."\">\n";
   	        for my $measure (@{$global_measures}) {
			print $XMLFILE "\t<$measure->[0]>";
			my ($val, $units) = @{$aparc_hemi_results->{GLOBALS}->{$measure->[1]}};
			print $XMLFILE $val;
			print $XMLFILE "</$measure->[0]>\n";
		}
	        print $XMLFILE "\t<fs:regions>\n";
	        for my $region (@{$aparc_hemi_results->{REGIONS}}) {
		  print $XMLFILE "\t\t<fs:region";
		  for my $attr (@{$region_attrs}) {
		    print $XMLFILE " $attr->[0]=\"$region->{$attr->[1]}\"";
		  }
		  print $XMLFILE ">\n";

		  for my $measure (@{$region_measures}) {
		    print $XMLFILE "\t\t\t<$measure->[0]>";
		    print $XMLFILE $region->{$measure->[1]};
	            print $XMLFILE "</$measure->[0]>\n";
		  }
		 print $XMLFILE "\t\t</fs:region>\n";
	       }
	      print $XMLFILE "\t</fs:regions>\n";

	  print $XMLFILE "\t</fs:hemisphere>\n";	
	}

}


sub createXMLHeader {
    my $XMLFILE = shift;
    my $subjectLabel = shift;
    my $datetime = shift;
    my $cur_time = gmtime;
    # Start printing the XML document.
print $XMLFILE <<EndOfHeader;
<?xml version="1.0" encoding="UTF-8"?>
<!-- XNAT XML generated by $VC_ID on $cur_time -->
<fs:$xmlType xmlns:xnat="http://nrg.wustl.edu/xnat"
xmlns:prov="http://www.nbirn.net/prov"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:fs="http://nrg.wustl.edu/fs"
xsi:schemaLocation="http://nrg.wustl.edu/fs ../schemas/cnda_xnat/fs.xsd"
ID="${xnat_id}_freesurfer_${datetime}" label="$subjectLabel" project="$project"
EndOfHeader
        print $XMLFILE ">\n";
}


sub printToXML {
   my $XMLFILE = shift;
   my $xmlElement = shift;
   my $value = shift;
   print $XMLFILE "\t<${xmlElement}>$value</${xmlElement}>\n";
}

sub printToXMLOpenTag {
   my $XMLFILE = shift;
   my $tag = shift;
    print $XMLFILE "\t<".$tag.">\n";
}

sub printToXMLCloseTag {
   my $XMLFILE = shift;
   my $tag = shift;
    print $XMLFILE "\t</".$tag.">\n";
}

sub getFreesurferVersion {
  my $build_file_name = "/build-stamp.txt";
  open (BUILDFILE, $freesurfer_home.$build_file_name) || die ("Could not open file <br> $!");
  my $text = <BUILDFILE>;
  close (BUILDFILE); 
  chomp($text);
print $text;
  return $text;
}


for my $dir (@ARGV) {
    print STDOUT "Processing $dir...";
    $dir =~s/\/$//;
    my $root_folder = $dir;
    $root_folder =~s/\/stats//;	 

   #my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    #$year += 1900;
    #my $datetime = ${year}.${mon}.${mday}; 

    my $xmlfile = undef;
    if (defined $outdir) {
	$xmlfile = "${outdir}/${xnat_id}_freesurfer5.xml";
    }else{
	$xmlfile = "${xnat_id}_freesurfer5.xml";
    }

    open(XMLFILE, ">$xmlfile") || die "Can't open XML file $xmlfile: $!";

    my $asegResults = Stats::read($dir."/aseg.stats");
    my $asegMeta = $asegResults->{METADATA};

    my %aparc_results = ();
     my %aparc_meta = ();
     my %lr = ( 'lh' => 'left', 'rh' => 'right' );
       my $lh_aparc_results = Stats::read($dir."/lh.aparc.stats");
       my $rh_aparc_results = Stats::read($dir."/rh.aparc.stats");
       $aparc_results{"lh"} = \$lh_aparc_results; #ref to a record
       $aparc_results{"rh"} = \$rh_aparc_results; #ref to a record
       my $meta_lh_aparc = $lh_aparc_results->{METADATA}; #ref to a hash
       my $meta_rh_aparc = $rh_aparc_results->{METADATA}; #ref to a hash
       $aparc_meta{"lh"} = \$meta_lh_aparc; 
       $aparc_meta{"rh"} = \$meta_rh_aparc;
       my $aparcMetaData = $lh_aparc_results->{METADATA};			 

 
    createXMLHeader(*XMLFILE,$asegMeta->{subjectname}."_freesurfer_".${datetime},${datetime});
    my $exptdate = undef;
    if (defined $asegMeta->{exptdate}) {
    	$exptdate = $asegMeta->{exptdate};
    }elsif (defined $aparcMetaData->{exptdate} ) {
	$exptdate = $aparcMetaData->{exptdate};
    }

    if (defined $exptdate) {   
       printToXML(*XMLFILE,"xnat:date",$exptdate);
    }
    printToXMLOpenTag(*XMLFILE,"xnat:provenance");
    
   insertAsegProvenance(*XMLFILE, $asegMeta,\@provenance_fields);
   insertAparcProvenance(*XMLFILE, \%aparc_meta, \@provenance_fields);
   printToXMLCloseTag(*XMLFILE,"xnat:provenance");
 
    #printToXMLOpenTag(*XMLFILE,"xnat:in");
     
   # insertAsegInFiles(*XMLFILE, $asegMeta,\@aseg_in_files, $root_folder);
   # insertAparcInFiles(*XMLFILE, \%aparc_meta,\@aparc_in_files,$root_folder);
   # printToXMLCloseTag(*XMLFILE,"xnat:in");
    printToXML(*XMLFILE,"xnat:imageSession_ID",$xnat_id);
    
    if($#{@aseg_parameters} >= 0) {
      printToXMLOpenTag(*XMLFILE,"xnat:parameters");
      writeParameters(*XMLFILE, $asegMeta, \@aseg_parameters);
      printToXMLCloseTag(*XMLFILE,"xnat:parameters");
    }
    
    my $fs_version = getFreesurferVersion();
    printToXML(*XMLFILE,"fs:version",$fs_version);


      printToXMLOpenTag(*XMLFILE,"fs:measures");
      printToXMLOpenTag(*XMLFILE,"fs:volumetric");
    insertAsegMeasures(*XMLFILE, $asegMeta, $asegResults,\@aseg_global_measures,\@aseg_region_attrs, \@aseg_region_measures) ;
      printToXMLCloseTag(*XMLFILE,"fs:volumetric");
      printToXMLOpenTag(*XMLFILE,"fs:surface");
    
    insertAparcMeasures(*XMLFILE,  \%aparc_results,\@aparc_global_measures,\@aparc_region_attrs, \@aparc_region_measures);
      printToXMLCloseTag(*XMLFILE,"fs:surface");
      printToXMLCloseTag(*XMLFILE,"fs:measures");
    
     printToXMLCloseTag(*XMLFILE,"fs:$xmlType");
      
 
   close XMLFILE || print STDERR "Unable to close $xmlfile: $!";

    print STDOUT "done.\n";}

exit 0;
