#!/bin/bash

PACKAGES_HOME=/nrgpackages
source ${PACKAGES_HOME}/scripts/AFNI_setup.sh
BXH_XCEDE_TOOLS_HOME=${PACKAGES_HOME}/tools.release/bxh_xcede_tools-1.10.3-lsb30.x86_64
PATH=${BXH_XCEDE_TOOLS_HOME}/bin:${PATH}
export PATH
