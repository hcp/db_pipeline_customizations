#!/bin/tcsh -f

set PACKAGES_HOME=/nrgpackages
source ${PACKAGES_HOME}/scripts/AFNI_setup.csh
set BXH_XCEDE_TOOLS_HOME=${PACKAGES_HOME}/tools.release/bxh_xcede_tools-1.10.3-lsb30.x86_64
set path=(${BXH_XCEDE_TOOLS_HOME}/bin ${path})
