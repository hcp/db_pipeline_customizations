<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd"
	xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">

	<name>rfMRIFIXHCP</name>
	<location>rfMRIFIXHCP</location>
	<description>ICA FIX Pipeline</description>

	<xnatInfo appliesTo="xnat:mrSessionData" />
	<outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(),'/rfMRIFIXHCP')^</outputFileNamePrefix>

	<parameters>
		<parameter>
			<name>icafix_builddir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),
					'/ICAFIX/',/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),'/')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>layer1_workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='icafix_builddir']/values/unique/text(),
					'DATA/LAYER1/')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='icafix_builddir']/values/unique/text(),
					'DATA/LAYER1/LAYER2/')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>func_workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),
					/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),'/')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>logdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='icafix_builddir']/values/unique/text(),
					'logs')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>db_workdir</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),/Pipeline/parameters/parameter[name='cluster_builddir_prefix']/values/unique/text(),/Pipeline/parameters/parameter[name='db_builddir_prefix']/values/unique/text())^</unique>
			</values>
		</parameter>

		<parameter>
			<name>xnat_host</name>
			<values>
				<unique>^if
					(count(/Pipeline/parameters/parameter[name='alias_host']/values/unique/text())>0)
					then
					/Pipeline/parameters/parameter[name='alias_host']/values/unique/text()
					else
					/Pipeline/parameters/parameter[name='host']/values/unique/text()^</unique>
			</values>
		</parameter>

		<parameter>
			<name>xnat_host_root1</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text(),'http://','')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>xnat_host_root2</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root1']/values/unique/text(),':8080/','')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>xnat_host_root3</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root2']/values/unique/text(),'https://','')^</unique>
			</values>
		</parameter>

		<parameter>
			<name>xnat_host_root</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root3']/values/unique/text(),'/','')^</unique>
			</values>
		</parameter>
	</parameters>

	<!--///STEPS/// -->
	<steps>
		<!--///T1 SETUP ROUTINE: Make dir, make session dirs, copy nifti, unzip... 
			/// -->
		<step id="0" description="Create a FIX data directory">
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="0a" description="Create a FIX data directory"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>^/Pipeline/parameters/parameter[name='functseries']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="1" description="Run getHCPResources and save results">
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='sessionid']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>^concat(/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'_preproc')^</value>
				</argument>
				<argument id="Files">
					<value>^concat('MNINonLinear/Results/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'.nii.gz,MNINonLinear/Results/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'/Movement_Regressors.txt,MNINonLinear/Results/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'_Atlas.dtseries.nii')^
					</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="2" description="Run getHCPResources and save results">
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='sessionid']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Structural_preproc</value>
				</argument>
				<argument id="Files">
					<value>MNINonLinear/T1w_restore_brain.nii.gz,MNINonLinear/wmparc.nii.gz,MNINonLinear/T1w.nii.gz,MNINonLinear/T2w.nii.gz</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='layer1_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="3" description="Run FIX"
			workdirectory="^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^">
			<resource name="rfMRIFIXHCPResources" location="WU_L1A/rfMRIFIX/resources">
				<argument id="File">
					<value>^concat(/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text(),
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'.nii.gz')^</value>
				</argument>
				<argument id="BP">
					<value>^/Pipeline/parameters/parameter[name='BP']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="4" description="Stripout password info from provenance file">
			<resource name="ParseProvenanceResource" location="ToolsHCP/resources">
				<argument id="InputDirectoryFile">
					<value>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(),'/',
						/Pipeline/name/text(), '_', /Pipeline/name/text(), '.xml')^</value>
				</argument>
			</resource>
		</step>

		<step id="5" description="Copy Log files" awaitApprovalToProceed="true"
			workdirectory="^/Pipeline/parameters/parameter[name='logdir']/values/unique/text()^">
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '*.err')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '*.log')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '_Provenance.xml')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/parameters/parameter[name='layer1_workdir']/values/unique/text(),'/*.*')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='func_workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="6" description="Delete previous output files"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="XnatDataClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>DELETE</value>
				</argument>
				<argument id="remote">
					<value>
						^concat('"',
						/Pipeline/parameters/parameter[name='host']/values/unique/text(),
						'/REST/projects/',
						/Pipeline/parameters/parameter[name='project']/values/unique/text(),
						'/subjects/',
						/Pipeline/parameters/parameter[name='subjects']/values/unique/text(),
						'/experiments/',
						/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),
						'/resources/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'_FIX"')^
					</value>
				</argument>
			</resource>
		</step>

		<step id="7" description="Upload ICAFIX output files to XNAT"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">

			<resource name="XnatDataClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>
						^concat('"',
						/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text(),
						'/REST/projects/',
						/Pipeline/parameters/parameter[name='project']/values/unique/text(),
						'/subjects/',
						/Pipeline/parameters/parameter[name='subjects']/values/unique/text(),
						'/experiments/',
						/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),
						'/resources/',
						/Pipeline/parameters/parameter[name='functseries']/values/unique/text(),
						'_FIX/files?event_reason=ICAFIXPipeline&amp;reference=',
						/Pipeline/parameters/parameter[name='db_workdir']/values/unique/text(),
						'"')^
					</value>
				</argument>
			</resource>
		</step>

		<step id="CLEANUP" description="Remove the workdir">
			<resource name="rm" location="commandlineTools">
				<argument id="file">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
				<argument id="r" />
			</resource>
		</step>

		<step id="END-Notify" description="Notify">
			<resource name="Notifier" location="notifications">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="to">
					<value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
				</argument>
				<argument id="cc">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="from">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>
						^concat(/Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(),
						' update FIX HCP Preprocessing Pipeline Completed: NIFTI and data
						files generated for ',
						/Pipeline/parameters/parameter[name='subjects']/values/unique/text()
						)^
					</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
				</argument>
				<argument id="body">
					<value>
						^concat('Dear ',
						/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),
						',&lt;br&gt; &lt;p&gt;',
						' NIFTI files have been generated for ',
						/Pipeline/parameters/parameter[name='subjects']/values/unique/text(),
						' by FIX HCP Pipeline. Details of the session are available &lt;a
						href="',
						/Pipeline/parameters/parameter[name='host']/values/unique/text(),
						'/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/',
						/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),
						'"&gt;',
						' here. &lt;/a&gt; &lt;/p&gt;&lt;br&gt;',
						' &lt;/p&gt;&lt;br&gt;',
						'XNAT Team.',
						/Pipeline/parameters/parameter[name='scantype']/values/unique/text())^
					</value>
				</argument>
			</resource>
		</step>

	</steps>
</Pipeline>

