//QAnotepad.cpp
//
//Author: David Koh
//Date: 8/31/06
//

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <gtk/gtk.h>

#include "QA_Option.h"
#include "Data.h"
#include "DrawingFuncs.h"
#include "CustomDrawingFuncs.h"
#include "GTKEventHandlers.h"
#include "Config.h"

using std::cout;
using std::cerr;
using std::endl;
using std::setw;
using std::left;
using std::string;
using std::ifstream;
using std::ofstream;
using std::istringstream;
using std::ostringstream;
using std::vector;
using std::iterator;

//Function prototypes
static void handle_args( int argc, char *argv[], string &filename, string &user, string &configFile, vector<string> &subjectList, string &subjectFile, bool &dbupdate, bool &appendExt );
static void setupCustomOpts( string configFile, vector<string*> &customOpts, vector<string::size_type> &customOptLen, int &procedureId, string &title );
static void readSubjectFile( string subjectFile, vector<string> &subjectList );

//----------------------
//--- Event Handlers ---
//----------------------

gboolean delete_event( GtkWidget *widget, GdkEvent *event, gpointer data )
{
    //sets endProg to true in order to quit program loop, quits main loop
    bool *endProg = static_cast<bool*>(data);
    *endProg = true;
    gtk_main_quit();
    return true;
}

gboolean destroy( GtkWidget *widget, gpointer data )
{
    //sets endProg to true in order to quit program loop, quits main loop
    bool *endProg = static_cast<bool*>(data);
    *endProg = true;
    gtk_main_quit();
    return true;
}

void submit_option( GtkWidget *button, gpointer opt )
{
    //submits the option to main and quits main loop
    static_cast<QA_Option*>(opt)->setTrue();
    gtk_main_quit(); 
}

void set_data( GtkWidget *widget, gpointer data )
{
    //submits the data to main
    static_cast<SetData*>(data)->set();
}

void set_freetext_data( GtkWidget *widget, gpointer data )
{
    //sets freetext to the contents of the text box
    string freetext = gtk_entry_get_text( GTK_ENTRY( widget ) );
    if( freetext == "" )
	freetext = "-";
    //sets the _setData variable for this button to that new text
    static_cast<SetData*>(data)->setNewData( freetext );
    //resubmits the data to main if the variable is already set
    if( static_cast<SetData*>(data)->lastSet() )
	set_data( widget, data );
}    

void delete_window( GtkWidget *widget, gpointer data )
{
    //hides the window and quits the main loop
    gtk_widget_hide( widget );
    gtk_main_quit();
}

//--- main() Function ---
//
//Calls: 
//  In file QAnotepad.cpp (this file): handle_args, readSubjectFile, setupCustomOpts
//  In file CustomDrawingFuncs.cpp: pickOptions, drawQA
//  In file DrawingFuncs.cpp: pickOptions, drwaQA
//  Also calls the perl script ~/scripts/perl/load_strQA.pl
//
int main( int argc, char *argv[] ) 
{

    //create main window object
    GtkWidget *window;

    //initialize gtk
    gtk_init(&argc, &argv);

    //define variables for handle_args
    bool dbupdate = false;
    bool endprog = false;
    bool appendExt = true;
    int procedureId = -1;
    string title = "QA Notepad";
    string filename = "";
    string user = "";
    string configFile = "";
    string subjectFile;
    vector<string> subjectList;
    vector<string*> customOpts;
    vector<string::size_type> customOptLen;

    //if there are arguments, pass them to handle_args.
    //handle_args passes back the values by reference.
    if( argv[1] )
        handle_args( argc, argv, filename, user, configFile, subjectList, subjectFile, dbupdate, appendExt );

    //if no custom output file specified, and a config file is specified, die and return error
    if( filename == "" && configFile != "" ) {
        cerr << "ERROR: Must specify custom output file to use custom options" << endl;
	exit(1);
    }
    //if no custom output file or config file specified, use standard output file
    else if( filename == "" && configFile == "" )
        filename = FILENAME;

    //if the file doesn't already end it .qa, append that on unless otherwise specified
    if( filename.substr( filename.length() - 3, filename.length() ) != ".qa" && appendExt )
	filename += ".qa";

    cout << "Output file: " << filename << endl;
    if( configFile != "" )
	cout << "Config file: " << configFile << endl;

    //if there is a subject file, read it in.
    if( subjectFile != "" )
	readSubjectFile( subjectFile, subjectList );

    if( subjectList.empty() )
	cout << "No subjects specified." << endl;
    else {    
	//print out a list of the subjects specified.
	cout << "Subjects: ";
	vector<string>::iterator s_itr;
	for( s_itr = subjectList.begin(); s_itr != subjectList.end(); ++s_itr )
	    cout << *s_itr << " ";
	cout << endl;
    }

    //if user is not specified, get username from shell
    if( user == "" ) {
	//read current shell user into a temp file
	system("id -un > user_tmp_file");
	ifstream user_file( "user_tmp_file" );
	if( user_file )
	    //read the file into the user variable
	    user_file >> user;
	user_file.close();
	//remove the file
	system("rm user_tmp_file");
    }

    
    //If there is a config file, determine custom field widths 
    // for each field, based on largest element in the field.
    vector<int> fieldWidths;
    if( configFile != "" ) {
	//read custom options from file into customOpts
	setupCustomOpts( configFile, customOpts, customOptLen, procedureId, title );
	
	vector<string*>::iterator itr1;
	vector<string::size_type>::iterator itr2;
	int index = 0;
        for( itr1 = customOpts.begin(), itr2 = customOptLen.begin(); itr1 != customOpts.end(); ++itr1, ++itr2 ) {
	    //get the number of options in this element
	    int numOpts = static_cast<int>(*itr2);
	    int maxFieldwidth = 0;
	    for( int i = 0; i < numOpts; ++i ) {
		//if the length of the current option string + the field padding is larger than the 
		// current max fieldwidth for this element, make that length the new max fieldwidth.
		string::size_type length;
		if( (*itr1)[i] == "$freetext" )
		    //if the option is freetext, use freetext maximum length.
		    length = FIELDWIDTH - FIELD_PADDING;
		else
		    length = (*itr1)[i].length() + FIELD_PADDING;
		if( static_cast<int>( length ) > maxFieldwidth )
		    maxFieldwidth = static_cast<int>( length );
	    }
	    //add the maxFieldwidth to the fieldWidths vector
	    fieldWidths.push_back( maxFieldwidth );

	    //debug code- prints the config file data
	    //cout << "customOpts[" << index << "]: ";
	    //for( int i = 0; i < numOpts; ++i )
	    //    cout << (*itr1)[i] << " ";
	    //cout << "\n";

	    ++index;
	}
    }

    //creates new toplevel window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    //set the default size of the window, so it's not tiny
    gtk_window_set_default_size( GTK_WINDOW( window ), 256, 256 );

    //set the window title (default QA notepad, specified in config file)
    gtk_window_set_title( GTK_WINDOW( window ), title.c_str() );
    
    //connects the delete_event and destroy event handlers to their respective events
    g_signal_connect (G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), &endprog);
    g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (destroy), &endprog);
    
    int option = 0;
    string data = "";

    if( configFile != "" )
	//display each element of customOpts as a button
        pickOptions( window, option, customOpts );
    else
	//display standard interface
        pickOptions( window, option );

    //show the window
    gtk_widget_show( window );

    //endprog is set in the delete_event or destroy event handlers
    //program loop
    while( ! endprog ) {

	//main loop
	gtk_main();
	if( endprog )
	    break;

	//create a new window to hold options returned from main loop
	GtkWidget *window2 = gtk_window_new( GTK_WINDOW_TOPLEVEL );
	//connect delete_event and destroy events of second window to delete_window function
	g_signal_connect (G_OBJECT(window2), "delete_event", G_CALLBACK(delete_window), NULL);
	g_signal_connect (G_OBJECT (window2), "destroy", G_CALLBACK (delete_window), NULL);
	if( configFile != "" )
	    //calls drawQA with custom options
	    gtk_container_add( GTK_CONTAINER( window2 ), drawQA( option, window2, data, filename, user, &subjectList, &customOpts, customOptLen, fieldWidths ) );
	else
	    //calls drawQA with default options
	    gtk_container_add( GTK_CONTAINER( window2 ), drawQA( option, window2, data, filename, user, &subjectList ) );

	//show second window
	gtk_widget_show( window2 );
	//main loop 2
	gtk_main();
    }
   
    //if --dbupdate flag was set, update the memage database via the perl script
    if( dbupdate ) {
	//checks to make sure they're not using custom options
	if( configFile != "" && procedureId == -1 ) {
	    cerr << "ERROR: Cannot write custom options to default database procedure" << endl;
	}
	else if( configFile != "" && procedureId != -1 ) {
	    ostringstream procIdStr;
	    procIdStr << procedureId;
	    string cmd = "~/scripts/perl/load_QA.pl --procnum=" + procIdStr.str() + " " + filename;
	    cout << "Updating the database...\n" 
		 << "Running " << cmd << "\n";
	    cout << "\n\nNOTE: You may get a message saying that this program is not responding,\n"
		 << "however, it it simply updating the database.\n"
		 << "Do not close the program until it closes on it's own.\n" << endl;
	    system( cmd.c_str() );
	}
	else {
	    string cmd = "~/scripts/perl/load_QA.pl " + filename;
	    cout << "Running " << cmd << "\n";
	    system( cmd.c_str() );
	}
    }
    
    vector<string*>::iterator itr;
    for( itr = customOpts.begin(); itr != customOpts.end(); ++itr ) 
	delete [] *itr;
    
    return 0;
}

//--- setupCustomOpts ---
//
//Reads options in from the configFile and puts
//each line into a string[], which in turn is stored
//inside the customOpts vector.  Stores the length of 
//the string[] inside customOptLen at the same index.
// 
static void setupCustomOpts( string configFile, vector<string*> &customOpts, vector<string::size_type> &customOptLen, int &procedureId, string &title )
{
    string line;
    istringstream strm;
    string opt;
    //open config file
    ifstream config( configFile.c_str() );
    //if config file isn't open, die...
    if( ! config ) {      
        cerr << "ERROR: Unable to open config file: " << configFile << endl;
        exit(1);
    }
    //...otherwise, read in options
    else {
	//read in lines until the end of file
	while( getline( config, line ) ) {

	    //put the line info into the stringstream
	    strm.str( line );
	    //starts at one, because we read first word to
	    //check for custom procedure
	    int numOptions = 1;

	    //check to see if this line defines the procedure id
	    if( strm >> opt ) {
		//if it does, set procedureId, reset strm, and
		//get next line.
		if( opt == "$procedure_id" ) {
		    strm >> opt;
		    procedureId = atoi( opt.c_str() );
		    strm.clear();
		    continue;
		}
		else if( opt == "$title" ) {
		    strm >> opt;
		    title = opt;
		    while( strm >> opt ) {
			title += " ";
			title += opt;
		    }
		    strm.clear();
		    continue;
		}
	    }
		    
	    //count the number of words in the stringstream
	    while( strm >> opt ) 
		++numOptions;

	    

	    //reset the stringstream
	    strm.clear();
	    strm.str( line );

	    //if there are an invalid number of options, skip this line
	    if( numOptions == 0 || numOptions == 1 )
		continue;

	    string *opts = new string[numOptions];
	    for( int i = 0; i < numOptions; ++i ) {
		//read each word into opt, and set opts to that word
		strm >> opt;
		opts[i] = opt;
	    }
	    //add opts[] to the customOpts vector, add the length to customOptLen
	    customOpts.push_back( opts );
	    customOptLen.push_back( static_cast<string::size_type>( numOptions ) );

	    strm.clear();
	}
	config.close();
    }
}

//--- readSubjectFile ---
//
//Reads subjects from subjectFile into the subjectList vector.
//Returns subjectList by reference.
//
static void readSubjectFile( string subjectFile, vector<string> &subjectList )
{
    string subj;
    //open subjectFile
    ifstream infile( subjectFile.c_str() );
    //if subjectFile isn't open, die...
    if( ! infile ) {
	cerr << "ERROR: Unable to open subjects file: " << subjectFile << endl;
        exit(1);
    }
    //...else read in the subjects
    else {
	//while there are still subjects, if they aren't blank, add them to subjectList
	while( infile >> subj ) {
	    if( subj != "" ) 
		subjectList.push_back( subj );
	}
    }
    infile.close();
}
    
//--- handle_args ---
//
//Processes the arguments and options passed from the command line.
//
static void handle_args( int argc, char *argv[], string &filename, string &user, string &configFile, vector<string> &subjectList, string &subjectFile, bool &dbupdate, bool &appendExt )
{
    //declares argument var, argument to an option var
    string arg, optarg;
    int argswitch = 0;
    int argCount = 0;
    
    //constants defined for each option
    const int UNDEFINED = -1;
    const int FILE = 1;
    const int USER = 2;
    const int CONFIG = 3;
    const int SUBJECT_LIST = 4;
    const int SUBJECT_FILE = 5;
    const int HELP = 6;
    const int DB_UPDATE = 7;
    const int NO_APPEND = 8;
    
    //goes through all the arguments
    while ( argCount < argc - 1 ) {
        ++argCount;
	arg = argv[argCount];
	//if the argument isn't an option, die
	if( arg.substr( 0, 1 ) != "-" ) {
	    cerr << "ERROR: Invalid option " << arg << endl;;
	    exit(1);
	}
	else {
	    //determine which option it is, and set the switch to the appropriate value
	    if( arg == "--file" || arg == "-f" )
		argswitch = FILE;
	    else if( arg == "--user" || arg == "-u" )
		argswitch = USER;
	    else if( arg == "--config" || arg == "-c" )
		argswitch = CONFIG;
	    else if( arg == "--subj" || arg == "-s" )
		argswitch = SUBJECT_LIST;
	    else if( arg == "--subjfile" )
		argswitch = SUBJECT_FILE;
	    else if( arg == "--help" || arg == "-h" )
		argswitch = HELP;
	    else if( arg == "--dbupdate" )
		argswitch = DB_UPDATE;
	    else if( arg == "--noappend" )
		argswitch = NO_APPEND;
	    else
		argswitch = UNDEFINED;
	    switch( argswitch ) {
	       
		//--file <output file>
	    case FILE:
		//if the option is not the last argument and the argument to this option is not
		// an option itself, set filename to the option's argument (optarg).
		if( argCount != argc - 1 && (optarg = argv[++argCount]).substr( 0, 1 ) != "-" )
		    filename = optarg;
		//otherwise, die
		else {
		    cerr << "ERROR: Missing argument to option " << arg << endl;
		    exit(1);
		}
		break;
		//--user <username>
	    case USER: 
		if( argCount != argc - 1 && (optarg = argv[++argCount]).substr( 0, 1 ) != "-" )
		    user = optarg;
		else {
		    cerr << "ERROR: Missing argument to option " << arg << endl;
		    exit(1);
		}
		break;
		//--config <config file>
	    case CONFIG:
		if( argCount != argc - 1 && (optarg = argv[++argCount]).substr( 0, 1 ) != "-" )
		    configFile = optarg;
		else {
		    cerr << "ERROR: Missing argument to option " << arg << endl;
		    exit(1);
		}		
		break;
		//--subj <subj1> <subj2> ...
	    case SUBJECT_LIST:
		if( argCount != argc - 1 && (optarg = argv[++argCount]).substr( 0, 1 ) != "-" ) {
		    //set current subject to optarg, and while next arg is not an option, add
		    // current subject to subjectList vector and set current subj to next arg.
		    string subj = optarg;
		    while( argCount != argc - 1 && subj.substr( 0, 1 ) != "-" ) {
			subjectList.push_back( subj );
			subj = argv[++argCount];
		    }
		    //if we've hit the last argument in the list and it's not an option,
		    //it's a subject, so add it to the list.
		    if( argCount == argc - 1 && subj.substr( 0, 1 ) != "-" )
			subjectList.push_back( subj );
		    else 
			//otherwise, dec argCount (will be inc'd at beginning of loop)
			--argCount;
		}
		else if ( argCount == argc - 1 )
		    break;
		else {
		    cerr << "ERROR: Missing argument to option " << arg << endl;
		    exit(1);
		}		
		break;
		//--subjfile <subject file>
	    case SUBJECT_FILE:
		if( argCount != argc - 1 && (optarg = argv[++argCount]).substr( 0, 1 ) != "-" )
		    subjectFile = optarg;
		else {
		    cerr << "ERROR: Missing argument to option " << arg << endl;
		    exit(1);
		}		
		break;
	    case HELP:
		cout << "USAGE: QAnotepad <options>\n\n"
		    
		     << "Options:\n"
		     << "   -h, --help: displays a help message\n"
		     << "   -f, --file <filename>: name of output file (default: QAnotes)\n"
		     << "   -s, --subj <subject1> <subject2> ... : enters a list of subjects to choose\n"
		     << "                                          from when recording data.\n"
		     << "   --subjfile <subject file>: specifies a file with a list of subjects.\n"
		     << "   -u, --user <username>: name of user\n"
		     << "   -c, --config <config file>: input config file to customize interface\n"
		     << "                               It is required to enter a custom output filename\n"
		     << "                               if you use this option.\n"
		     << "   --noappend: \".qa\" will not be appended to the filename.\n"
		     << "   --dbupdate: if this flag is set, QAnotepad will automatically update the\n"
                     << "               memage database via ~dkoh/scripts/perl/load_QA.pl.\n\n"
		    
		     << "Config file:  \n"
		     << "  The config file should be formatted with the option header in the first column,\n"
		     << "  followed by the desired options.  The option header will not appear unless it\n"
		     << "  has at least one option.  See the wiki for more details on config files.\n" << endl;
		exit(0);
		//--dbupdate
	    case DB_UPDATE:
		dbupdate = true;		
		break;
		
	    case NO_APPEND:
		appendExt = false;
		break;
		
	    default:
		//usage exit
		cerr << "ERROR: Unrecognzied option " << arg << "\n";
		cout << "USAGE: QAnotepad <options>\n\n"
		    
		     << "Options:\n"
		     << "   -h, --help: displays a help message\n"
		     << "   -f, --file <filename>: name of output file (default: QAnotes)\n"
		     << "   -s, --subj <subject1> <subject2> ... : enters a list of subjects to choose\n"
		     << "                                          from when recording data.\n"
		     << "   --subjfile <subject file>: specifies a file with a list of subjects.\n"
		     << "   -u, --user <username>: name of user\n"
		     << "   -c, --config <config file>: input config file to customize interface\n"
		     << "                               It is required to enter a custom output filename\n"
		     << "                               if you use this option.\n" 
		     << "   --noappend: \".qa\" will not be appended to the filename.\n"
		     << "   --dbupdate: if this flag is set, QAnotepad will automatically update the\n"
                     << "               memage database via ~dkoh/scripts/perl/load_QA.pl." << endl;
		exit(0);
	    }
	}
    }
}
	    

