# ORIGINAL VERSION BY Jenni Pacheco
# MODIFIED BY David Koh
#
# DEPENDENCIES:
#    - Snapshots:
#	- snap_tkmedit.csh $s
#	- snap_tksurfer.csh $s
#	- snap_tkmedit.tcl
#       - snap_tksurfer.tcl
#
#
# CHANGE LOG:
# 2006-06-06: (steph)
#    - added -o argument so the user can specify an output html file.  If -o
#      flag is not used, snapshots.html is output with all images in the 
#      $SUBJECTS_DIR/rgb/snaps directory
# 2006-08-30: (david)
#    - added -overwrite flag so the user can specify to re-take previously
#      captured images
#2006-10-12: (jenni)
#   -i wrote new snapshot scripts, so that it would go faster. I've put those
#    in and taken out the old snapshot scripts.
#
#
#
#
#

#!/bin/tcsh -f
set echo=1

set subjects = ();
# default html output file
set outputFile = ()

set DetailedSnapshots = 0
set overwrite = 0

goto parse_args;
parse_args_return:

goto check_params;
check_params_return:

# ---


echo "I am about to take snap shots of all your outputs - make sure you have the screen saver turned off and no windows overlapping the tkmedit and tksurfer screens that will open.  Ctrl-C now if you are not ready!"

sleep 5

foreach s ($subjects)

    if (! -e $SUBJECTS_DIR/$s/rgb/ ) then
	mkdir -p $SUBJECTS_DIR/$s/rgb/
    endif
    if (! -e $SUBJECTS_DIR/$s/rgb/snaps/ ) then
	mkdir -p $SUBJECTS_DIR/$s/rgb/snaps/
    endif
    echo "snaps for $s"
    
    set takeSnaps = 0 
    # tkmedit   
    foreach reg (aseg-cor1 aseg-cor2 aseg-cor3 aseg-cor4 aseg-cor5 aseg-cor6 aseg-cor7 aseg-templh aseg-temprh)
	if ( (! -e $SUBJECTS_DIR/$s/rgb/snaps/${s}_${reg}.gif) && (! -e $SUBJECTS_DIR/$s/rgb/snaps/${s}_${reg}.rgb) || ($overwrite == 1) ) then
	    set takeSnaps = 1
	endif
    end
    if($takeSnaps == 1) then
        $RECON_CHECKER_SCRIPTS/snap_tkmedit.csh $s
    else
	echo "Tkmedit images exist. Skipping these..."
#	sleep 3
    endif

    #tksurfer
    set takeSnaps = 0
    foreach reg (parc_lh_lat parc_lh_med parc_lh_inf parc_rh_lat parc_rh_med parc_rh_inf)
	if ( (! -e $SUBJECTS_DIR/$s/rgb/snaps/${s}_${reg}.gif) && (! -e $SUBJECTS_DIR/$s/rgb/snaps/${s}_${reg}.rgb) || ($overwrite == 1) ) then
	    set takeSnaps = 1
	endif
    end
    if ($takeSnaps == 1) then
        $RECON_CHECKER_SCRIPTS/snap_tksurfer.csh $s
    else
	echo "Tksurfer images exist. Skipping..."
#	sleep 3
    endif
end

if ($DetailedSnapshots == 1) then
    foreach s ($subjects)
	if( ! -e $SUBJECTS_DIR/$s/rgb/snaps/snapshot-talairach-C-128.gif || ($overwrite == 1) ) $RECON_CHECKER_SCRIPTS/takesnapshotpreset.sh -twac $s
    end
endif

echo "converting to tiff and making html page"


# images: <$SUBJID>_<region>.gif
# if no output file, just use Jenni's script
if ( $#outputFile == 0 ) then

    set outputFile = snapshots.html
    $RECON_CHECKER_SCRIPTS/snaps_html.csh

else
    foreach s ($subjects)
	cd $SUBJECTS_DIR/$s/rgb/snaps
	set subjOutputFile = ${s}.html
	if (`ls | egrep -c "\.rgb"`) then
	    foreach f (*.rgb)
	       convert -scale 300x300 $f ${f:r}.gif
		rm $f
	    end
	endif

	# write html
	echo '<html>\n<head>\n<style>body {font-family:Verdana;}</style>' > $subjOutputFile
	echo "<title>Snapshots</title>" >> $subjOutputFile
	echo '<center><font size="+2">'$s Snapshots'</font></center>' >> $subjOutputFile
	echo '</head>\n<body bgcolor="#C0C0C0">\n<center>\n<TABLE>' >> $subjOutputFile

	set titlelist = ( "talairach" "aseg" "surfs" "inflated" "curv" "parc" )
	set titleindex = 1
	foreach reglist ( "cor sag hor" "aseg-cor1 aseg-cor2 aseg-cor3 aseg-cor4 aseg-cor5 aseg-cor6 aseg-cor7 aseg-templh aseg-temprh" "cor1 cor2 cor3 cor4 cor5 cor6 cor7 templh temprh" "lh_lat lh_med lh_inf rh_lat rh_med rh_inf" "curv_lh_lat curv_lh_med curv_lh_inf curv_rh_lat curv_rh_med curv_rh_inf" "parc_lh_lat parc_lh_med parc_lh_inf parc_rh_lat parc_rh_med parc_rh_inf" )
	    echo '\n<TR>'  >> $subjOutputFile
	    if( $DetailedSnapshots == 1 && $titleindex <= 3 ) then
		set linkname = "$s-$titlelist[$titleindex]-QA.html"
		if( $titleindex == 3 ) set linkname = "$s-whitematter-QA.html"
		echo '<TD VALIGN=CENTER><A HREF="'$linkname'">'"$titlelist[$titleindex]</A></TD>" >> $subjOutputFile
	    else
		echo "<TD VALIGN=CENTER>$titlelist[$titleindex]</TD>" >> $subjOutputFile
	    endif
	    @ titleindex = $titleindex + 1
	    foreach reg ( $reglist )
		echo '<TD ALIGN=CENTER VALIGN=BOTTOM>' >> $subjOutputFile

		# take the string, get rid of the aseg-cor1 part, replace it with $reg
		# to form the image name
		echo '<IMG SRC="'$SUBJECTS_DIR/$s/rgb/snaps/${s}_${reg}.gif'" width="300" height="300">' >> $subjOutputFile

		echo '<div style="text-align: center">'" $reg</div></A></TD>" >> $subjOutputFile
	    end
	    echo '</TR>' >> $subjOutputFile
	end

	echo '</TABLE>' >> $subjOutputFile
	echo '</center></body>\n</html>' >> $subjOutputFile    
    end
endif

cd $SUBJECTS_DIR

echo '<html>\n<head>' > $outputFile
echo '   <title>Subject Index</title><center><font size="+2">Subject Index</font></center>\n</head>\n<body>' >> $outputFile
foreach s ($subjects)
    echo '   <a href="'$SUBJECTS_DIR/$s/rgb/snaps/${s}.html'">''<font size="+1">'"$s snapshots</font></a> <br />" >> $outputFile
end

echo "   </body>\n</html>" >> $outputFile

echo "you can now view your page at file: $outputFile"


# ----

exit 0;

############--------------##################
parse_args:
set cmdline = ($argv);
while( $#argv != 0 )

  set flag = $argv[1]; shift;

  switch($flag)

    case "-s":
    case "-subjid":
        if ( $#argv == 0) goto arg1err;
        set proceed = 1;
        while ( $#argv != 0 && $proceed )
            set subjects = ($subjects $argv[1]); shift;
            if ( $#argv != 0 ) then
                set proceed = `echo "$argv[1]" | gawk '{ if (substr($1, 1, 1) == "-") {print "0"} else {print "1"} }'`;
            endif
        end
      breaksw

    case "-sf":
      if ( $#argv == 0) goto arg1err;
      set subjects = `cat $argv[1]`; shift;
      breaksw

    # output file
    case "-o":
	if ( $#argv == 0) goto arg1err;
	set outputFile = $argv[1]; shift;
    breaksw

    case "-detailed"
      set DetailedSnapshots = 1;
      breaksw

    case "-overwrite"
      set overwrite = 1;
      breaksw
    # ----

    default:
        echo ERROR: Flag $flag unlegal.
        echo $cmdline
        exit 1
    breaksw
    endsw

end
goto parse_args_return;



############--------------##################
############--------------##################
check_params:
    if($#subjects == 0) then
        echo "USAGE ERROR: must specify a subjid"
	exit 1;
    endif
goto check_params_return;
############--------------##################

