#!/bin/bash
#
### Wrapper scripts for running Xvfb
#
# -- our name ---
#$ -N xvfb_wrapper
#$ -S /bin/bash

#XVFB_OPTS="-fbdir /tmp/fb"
XVFB_OPTS=

#scriptdir=`dirname $0`
#scriptdir=`readlink -f $scriptdir`
##$scriptdir/xvfb-run -a -s  "-screen 0 1024x1500x24 $XVFB_OPTS" $*

/data/intradb/pipeline/catalog/freesurfer_tools/QAtools/data_checker/xvfb-run -a -s  "-screen 0 1024x1500x24 $XVFB_OPTS" $*

