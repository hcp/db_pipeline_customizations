#!/bin/bash

###########################################################################################################
## Check for fieldmap images.  Per Matt, two 3D fieldmap magnitude images should be merged into a single ##
## 4D image.  This program assumes "fieldmap" will be part of the series description.                    ##
###########################################################################################################

## SET UP FSL
export FSLDIR=/nrgpackages/tools.release/fsl-4.1.9-nrg
source /nrgpackages/tools.release/fsl-4.1.9-nrg/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

if [ $# -lt 1 ] ; then
   echo "1.0"
   exit 0
fi

export OUT_DIR=$1
export PATTERN=fieldmap*.nii*

if [ ! -d "$OUT_DIR" ] ; then
   echo "ERROR:  Specified directory does not exist"
   exit 1
fi

pushd $OUT_DIR

## PULL BASENAME OUT OF FILES, EXIT IF NO FIELDMAP FILES

BASENAME=`find . -name "*.nii*" | sed -e "s/^.*\///" | sed -e "s/[.].*$//" | sort | head -1`

if [ -z "$BASENAME" ] ; then
   echo "No FieldMap image files found files exist under specified directory, exiting."
   exit 0
fi

## CONTINUE ONLY IF EXACTLY 2 NIFTI FILES (THEN COMBINE THEM)
FCOUNT=`find . -iname "$PATTERN" | wc -l`

if [ $FCOUNT -ne 2 ] ; then
   echo "$FCOUNT files found (not two), exiting."
   exit 0
fi

## SEPARATE VARIABLES FOR FILE NAMES

for FIL in $(find . -type f -iname "$PATTERN") 
do
   if [ `echo $FIL | xargs basename | sed -e "s/[.].*$//"` == "$BASENAME" ] ; then
      export FILE1=$FIL
   else 
      export FILE2=$FIL
   fi
done

echo BASENAME=$BASENAME
echo FILE1=$FILE1
echo FILE2=$FILE2

## DO MERGE 

if [ -n "$FILE1" ] ; then

   fslmerge -t $FILE1 $FILE1 $FILE2
   rm $FILE2

fi

popd

