#!/bin/bash

export USER=$1
export PASSWORD=$2
export HOST=$3
export PROJECT=$4
export SUBJECT=$5
export SESSION=$6
export SCAN=$7

if [ $# -lt 7 ] ; then
   echo "1.0"
   exit 0
fi

die(){
  echo >&2 "$@"
  exit 1
}

## See if DICOM resource exists
echo curl -f -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DICOM
curl -f -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DICOM

if [ $? -eq 0 ] ; then

   	curl -f -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DICOM/files?format=zip\&structure=legacy > ${SESSION}_${SCAN}.zip || die "Couldn't get scan DICOM files"
   	unzip ${SESSION}_${SCAN}.zip || die "Couldn't unzip downloaded file"
	## CLEAN UP ##
   	rm ${SESSION}_${SCAN}.zip 

fi

