#!/usr/bin/python

try:
        import json
except ImportError:
        import simplejson as json
import os
import shlex
import sys
import subprocess as sp
import zipfile
import logging
import shutil

## If fewer than required arguments, just print version number
if (len(sys.argv)<10):
	print ("1.0")
	sys.exit(0)

## Assign variables

scriptpath=sys.path[0]

url=sys.argv[1]
user=sys.argv[2]
pw=sys.argv[3]
workdir=sys.argv[4]

projId=sys.argv[5]
subjId=sys.argv[6]
expId=sys.argv[7]
expLbl=sys.argv[8]
fileName=sys.argv[9]
optFileName=sys.argv[10]
rserver=sys.argv[11]
ruser=sys.argv[12]

## Add separator if one's not there
if not url.endswith('/'):
   url+='/'

## Set up logging
logf = workdir + '/preprocess_script.log'
logging.basicConfig(filename=logf,level=logging.INFO,filemode='w')

## Create working directory (Temporary try/catch?)
try:
	currworkdir = workdir + '/' + expLbl + '/' 
	os.mkdir(workdir + '/' + expLbl + '/') 
except OSError:
	print('\n\nWARNING  Working directory already exists')

try:
	os.mkdir(currworkdir + 'NIFTI') 
except OSError:
	pass
try:
	os.mkdir(currworkdir + 'FreeSurfer') 
except OSError:
	pass
try:
	os.mkdir(currworkdir + 'SurfaceReg') 
except OSError:
	pass

## Get list of scans
logging.info('\ncurl ' + ' -u ' + user + ':xxxxxx' + ' ' + url + 'REST/experiments/' + expId + '/resources?all=true&file_stats=true&format=json')
pipeout = sp.Popen(['curl','-u',user + ':' + pw,url + 'REST/experiments/' + expId + '/resources?all=true&file_stats=true&format=json' ], stdout = sp.PIPE)
curlout=pipeout.stdout.read()
jsonobj=json.loads(curlout)

## Search for MPRAGE scans 
results=jsonobj.get('ResultSet').get('Result')

resource=''
any=False
for r in results:
	if r.get('cat_desc')=='MPRAGE': 
		resource = resource + ',' + r.get('xnat_abstractresource_id')
	 	any=True

## Return list of files, see if contains matching file name (or optional (skull-stripped)) file name

## Per Dan, we'll just use the skull-stripped files
workFile=fileName

##workFile=''
##isSkullStripped=True
##if any:
##	logging.info('\ncurl'+'-u'+user + ':xxxxxx ' + url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files?format=json')
##	pipeout = sp.Popen(['curl','-u',user + ':' + pw,url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files?format=json'],stdout=sp.PIPE)
##	curlout=pipeout.stdout.read()
##	jsonobj=json.loads(curlout)
##	logging.info('\n' + curlout)
##	results=jsonobj.get('ResultSet').get('Result')
##	for r in results:
##		if r.get('Name')==fileName:
##			workFile=fileName
##		elif r.get('Name')==optFileName:
##			if len(workFile)<1:
##				workFile=optFileName
##				isSkullStripped=False
##else:
##	logging.error('\n\nERROR:  Required scan and NIfTI output do not exist')
##	sys.exit(1)

if len(workFile)<1:
	logging.error('\n\nERROR:  Required input files do not exist')
	sys.exit(1)

## Copy file to working directory (if it's not there)
if not os.path.exists(currworkdir + 'NIFTI/' + workFile):
	workf = open(currworkdir + 'NIFTI/' + workFile,'w',0)
	spout = sp.call(['curl','-u',user + ':' + pw,url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files/' + workFile],stdout=workf)
	workf.close()

## Matt's Surface Freg program expects input NIFTI file to be gz compressed.  (the fslmaths section creates a gz file,
## but it will not be used if we don't gz it first).  Since we're working with a copy, we'll gz it here.
## FreeSurfer can work with the gzipped file
if workFile.lower().endswith('.nii'):
	fsrc = sp.call(['gzip','-f',currworkdir + 'NIFTI/' + workFile])
	if fsrc==0 and os.path.exists(currworkdir + 'NIFTI/' + workFile + '.gz'):
		workFile += '.gz'
	else:
		logging.error('\n\nERROR:  Surface processing currently requires a zipped NIFTI file and the zipping process failed')
		sys.exit(1)

if not workFile.lower().endswith('.nii.gz'):
	logging.error('\n\nERROR:  File type is invalid (requires .nii.gz file)')
	sys.exit(1)

# Run FreeSurfer
try:

	## Per Dan, we'll just use the skull-stripped files
	## MODIFIED PER MATT, 2011-05-13, we'll use only non-skull-stripped files
	fsresult = sp.Popen([scriptpath + '/includes/run_freesurfer.sh',rserver,ruser,currworkdir + 'NIFTI/' + workFile,expLbl,currworkdir + 'FreeSurfer'],stdout=sp.PIPE)
	fsout=fsresult.stdout.read()
	logging.info(fsout)
	fsresult.wait()
	if fsresult.returncode!=0:
		logging.error('\n\nERROR:  FreeSurfer run was unsuccessful - returnCode=' + str(fsresult.returncode))
		sys.exit(fsresult.returncode)

##	if not isSkullStripped:
##		## NOTE TEMPORARY dontrun option
##		#fsresult = sp.Popen(['recon-all','-i',currworkdir + 'NIFTI/' + workFile,'-subject',expLbl,'-sd',currworkdir + 'FreeSurfer','-dontrun','-all'],stdout=sp.PIPE)
##		#fsresult = sp.Popen(['recon-all','-i',currworkdir + 'NIFTI/' + workFile,'-subject',expLbl,'-sd',currworkdir + 'FreeSurfer','-all'],stdout=sp.PIPE)
##		fsresult = sp.Popen([scriptpath + '/includes/run_freesurfer.sh',currworkdir + 'NIFTI/' + workFile,expLbl,currworkdir + 'FreeSurfer'],stdout=sp.PIPE)
##		fsout=fsresult.stdout.read()
##		logging.info(fsout)
##		fsresult.wait()
##		if fsresult.returncode!=0:
##			logging.error('\n\nERROR:  FreeSurfer run was unsuccessful - returnCode=' + str(fsresult.returncode))
##			sys.exit(1)
##	else: 
##		## Initial run checking skull-stripped output
##		fsresult = sp.Popen(['recon-all','-i',currworkdir + 'NIFTI/' + workFile,'-subject',expLbl,'-sd',currworkdir + 'FreeSurfer','-autorecon1','-noskullstrip'],stdout=sp.PIPE)
##		fsout=fsresult.stdout.read()
##		logging.info(fsout)
##		fsresult.wait()
##		if fsresult.returncode!=0:
##			logging.error('\n\nERROR:  FreeSurfer run was unsuccessful - returnCode=' + str(fsresult.returncode))
##			sys.exit(1)
##		## Perform necessary file copies for following step
##		shutil.copyfile(currworkdir + 'FreeSurfer/' + expLbl + '/mri/T1.mgz',currworkdir + 'FreeSurfer/' + expLbl + '/mri/brainmask.auto.mgz')
##		shutil.copyfile(currworkdir + 'FreeSurfer/' + expLbl + '/mri/T1.mgz',currworkdir + 'FreeSurfer/' + expLbl + '/mri/brainmask.mgz')
##		## Run the remainder of the freesufer process
##		fsresult = sp.Popen(['recon-all','-i',currworkdir + 'NIFTI/' + workFile,'-subject',expLbl,'-sd',currworkdir + 'FreeSurfer','-autorecon2','-autorecon3','-force'],stdout=sp.PIPE)
##		fsout=fsresult.stdout.read()
##		logging.info(fsout)
##		fsresult.wait()
##		if fsresult.returncode!=0:
##			logging.error('\n\nERROR:  FreeSurfer run was unsuccessful - returnCode=' + str(fsresult.returncode))
##			sys.exit(1)

except OSError, e:
	logging.error('\n\nERROR:  FreeSurfer run was unsuccessful - ' + e)

## TEMPORARY - copy freesurfer files from already ran location
#fsrc = sp.call('cp -r /home/michael/xnat_files/build/Test1000/20110317_133645/ANNA04111001/FreeSurfer/ANNA04111_ANNA04111001/* ' + currworkdir + 'FreeSurfer/' + expLbl,shell=True)

