#!/bin/bash

##
## 2011/12/14 - Modified script to use unzipped .nii files for processing.  fsl was having significant ##
##              trouble processing from large zipped files.  Were seeing massive system loads          ##
##              resulting in the need to reboot machines.  May want to modify prior steps to pass in   ##
##              unzipped output.                                                                 (MRH) ##
##

if [ $# -lt 10 ] ; then
   echo "1.0"
   exit 0
fi

## SET UP FSL
export FSLDIR=/nrgpackages/tools.release/fsl-4.1.9-nrg
source /nrgpackages/tools.release/fsl-4.1.9-nrg/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

export USER=$1
export PASSWORD=$2
export HOST=$3
export OUT_DIR=$4
export QC_DIR=$5
export PROJECT=$6
export SUBJECT=$7
export SESSION=$8
export SCAN=$9
export KEEP_QC=${10}

pushd $OUT_DIR

## CHANGE FOR DEFACED PIPELINE ##
if [ `find . -type f | wc -l` -le 0 ] ; then

   ## Okay here.  Only T1, T2 should have files if this is a renaming run.
   exit 0

fi
## END CHANGE FOR DEFACED PIPELINE ##

## PER MATT G, 2012/11/08, REMOVING DE-OBLIQUING STEPS.  ONLY DOING fslreorient2std AND ONLY DOING THAT FOR T1's and T2's

SCANVAR=`curl -u ${USER}:${PASSWORD} -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}`
TYPEVAR=`echo -e "$SCANVAR" | grep -a "xnat:imageType>" | sed -e "s/<[^<]*>//g"`
SERIES_DESC=`echo -e "$SCANVAR" | grep -a "xnat:series_description>" | sed -e "s/<[^<]*>//g"`
SEQUENCE=`echo -e "$SCANVAR" | grep -a "xnat:sequence>" | sed -e "s/<[^<]*>//g"`

## ALSO NEED TO SKIP TRANSFORMATIONS FOR DIFFUSION SCAN SCOUT.  THE MOST RELIABLE WAY TO DO THIS CURRENTLY SEEMS TO BE TO LOOK FOR 
## A SEQUENCE NAME BEGINNING WITH epse2d.  THIS WAS, HOWEVER, USED EARLY ON FOR A FEW BOLD SCANS, SO WE WILL LOOK AT SERIES DESCRIPTION
## AND THROW OUT ANY THAT CONTAIN BOLD

if [[ !($TYPEVAR =~ ^[tT][12][Ww_]*.*) ]] && [[ !( $SERIES_DESC =~ ^[Tt][12][Ww_].*) ]]; then
   echo "SKIPPING TRANSFORMATION PROCESSING - NOT A STRUCTURAL SCAN"
   for NII in $(find . -name "*.nii") 
   do
      gzip $NII
   done
   exit 0
fi

## OTHERWISE, CONTINUE FOR STRUCTURAL SCANS

mkdir -p $QC_DIR
cd $OUT_DIR

for NII in $(find . -name "*.nii*") 
do

   FN=`basename $NII | sed 's/.nii.*$//'`

   ## BEST TO WORK WITH UNZIPPED FILES HERE.  WILL ZIP FILES AFTER PROCESSING ##
   if [ -e ${FN}.nii.gz -a ! -e ${FN}.nii ] ; then
      gzip -d ${FN}.nii.gz
   fi 
   
   mv ${FN}.nii ${QC_DIR}/${FN}_orig.nii

   pushd ${QC_DIR}

   #### PER MATT G, 2012-11-08, NO LONGER  NEED MATRIX OUTPUT
   ##$FSLDIR/bin/fslhd -x ${FN}_orig.nii > ${FN}_orig.xml
   ##$FSLDIR/bin/fslhd ${FN}_orig.nii | grep sto_xyz | sed 's/sto_xyz:.[         ]*//' > ${FN}_orig.mat

   ## Insert code from Mike H to skip fslreorient2std where not necessary (2012/07/13)
   # For some reason in bash (but not tcsh), the ?orient variables that are
   # returned by 'fslval' have a trailing space, which I've stripped out
   # using the 'tr' command
   xorient=`$FSLDIR/bin/fslval ${FN}_orig.nii qform_xorient | tr -d ' '`
   yorient=`$FSLDIR/bin/fslval ${FN}_orig.nii qform_yorient | tr -d ' '`
   zorient=`$FSLDIR/bin/fslval ${FN}_orig.nii qform_zorient | tr -d ' '`
   echo "${FN}_orig.nii"
   echo "   $xorient, $yorient, $zorient"
   if [[ "$xorient" != "Right-to-Left" && "$xorient" != "Left-to-Right" || \
                         "$yorient" != "Posterior-to-Anterior" || \
                         "$zorient" != "Inferior-to-Superior" ]] ; then
        echo "   Not LAS or RAS oriented.  Running fslreorient2std".
       $FSLDIR/bin/fslreorient2std ${FN}_orig.nii ${FN}_reorient.nii.gz
       gzip -d ${FN}_reorient.nii.gz
   else
       echo "   Image is LAS or RAS oriented.  Skipping fslreorient2std".
       cp -p ${FN}_orig.nii ${FN}_reorient.nii
   fi

   #### PER MATT G, 2012-11-08, NO LONGER  NEED MATRIX OUTPUT
   ##$FSLDIR/bin/fslreorient2std ${FN}_orig.nii > ${FN}_scanner2nifti.mat
   ##$FSLDIR/bin/convert_xfm -omat ${FN}_nifti2scanner.mat -inverse ${FN}_scanner2nifti.mat
   ##$FSLDIR/bin/fslhd -x ${FN}_reorient.nii > ${FN}_reorient.xml

#### INSERT SFORM/QFORM TRANSFORMATION SCRIPT ##
##
##im=${FN}_reorient.nii
##
##newim=`$FSLDIR/bin/remove_ext $im`_sformMod
##dx=`$FSLDIR/bin/fslval $im pixdim1`;
##dy=`$FSLDIR/bin/fslval $im pixdim2`;
##dz=`$FSLDIR/bin/fslval $im pixdim3`;
##ori=`$FSLDIR/bin/fslorient $im`;
##
##if [ $ori = RADIOLOGICAL ] ; then
##  dx=`echo "$dx * -1" | bc -l`;
##fi
##
##$FSLDIR/bin/fslhd $im | grep sto_xyz | sed 's/sto_xyz:.[    ]*//' > ${newim}_oldsform.mat
##
##cp -p $im ${newim}.nii
##
##$FSLDIR/bin/convert_xfm -omat ${newim}_oldsforminv.mat -inverse ${newim}_oldsform.mat
##origx=`sed -n 1p ${newim}_oldsforminv.mat | awk '{ print $4 }'`
##origy=`sed -n 2p ${newim}_oldsforminv.mat | awk '{ print $4 }'`
##origz=`sed -n 3p ${newim}_oldsforminv.mat | awk '{ print $4 }'`
##origx=`echo "-1 * $origx * $dx" | bc -l`
##origy=`echo "-1 * $origy * $dy" | bc -l`
##origz=`echo "-1 * $origz * $dz" | bc -l`
##$FSLDIR/bin/fslorient -setsform $dx 0 0 $origx 0 $dy 0 $origy 0 0 $dz $origz 0 0 0 1 $newim 
##$FSLDIR/bin/fslorient -copysform2qform $newim
##
##$FSLDIR/bin/fslhd $newim | grep sto_xyz | sed 's/sto_xyz:.[         ]*//' > ${newim}_newsform.mat
##
##$FSLDIR/bin/fslhd -x ${newim}.nii > ${newim}.xml
##
#### END INSERTED SCRIPT ##

#### OLD COPY SECTION (WHEN RUNNING DE-OBLIQUING SCRIPT)
##     gzip ${newim}.nii 
##     gzip ${FN}_orig.nii 
##     gzip ${FN}_reorient.nii
##     cp -p ${FN}_orig.mat ${OUT_DIR}
##     cp -p ${FN}_scanner2nifti.mat ${OUT_DIR}
##     cp -p ${FN}_nifti2scanner.mat ${OUT_DIR}
##     cp -p ${newim}.nii.gz ${OUT_DIR}/${FN}.nii.gz


## NEW COPY SESSION (AFTER REMOVAL OF DE-OBLIQUING SCRIPT) ##
     im=${FN}_reorient.nii
     newim=`$FSLDIR/bin/remove_ext $im`_sformMod
     cp -p $im ${newim}.nii
     gzip ${newim}.nii 
     gzip ${FN}_orig.nii 
     gzip ${FN}_reorient.nii
     cp -p ${newim}.nii.gz ${OUT_DIR}/${FN}.nii.gz
## END NEW COPY SECTION ##

   popd

done

## UPLOAD AS A RESOURCE, IF REQUESTED ##

if [[ $KEEP_QC == [Yy] ]] ; then

   pushd $QC_DIR
   zip -r ../${SCAN}_qc.zip .
   curl -u ${USER}:${PASSWORD} -X PUT -T "../${SCAN}_qc.zip" ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DCM2NII_QC/files/DCM2NII_QC?extract=true\&inbody=true\&format=MISC\&content=QC_OUTPUT
   rm ../${SCAN}_qc.zip
   popd

fi

