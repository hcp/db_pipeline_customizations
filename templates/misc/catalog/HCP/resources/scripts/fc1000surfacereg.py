#!/usr/bin/python

try:
        import json
except ImportError:
        import simplejson as json
import os
import shlex
import sys
import subprocess as sp
import zipfile
import logging

## If fewer than required arguments, just print version number
if (len(sys.argv)<11):
	print ("1.0")
	sys.exit(0)

## Assign variables

scriptpath=sys.path[0]

url=sys.argv[1]
user=sys.argv[2]
pw=sys.argv[3]
workdir=sys.argv[4]

projId=sys.argv[5]
subjId=sys.argv[6]
expId=sys.argv[7]
expLbl=sys.argv[8]

fileName=sys.argv[9]
optFileName=sys.argv[10]
rserver=sys.argv[11]
ruser=sys.argv[12]

## Add separator if one's not there
if not url.endswith('/'):
   url+='/'

## Set up logging
logf = workdir + '/surfacepreg_script.log'
logging.basicConfig(filename=logf,level=logging.INFO,filemode='w')

## working directory (Temporary try/catch?)
currworkdir = workdir + '/' + expLbl + '/' 

## Get subject label (needed later)
pipeout = sp.Popen(['curl','-u',user + ':' + pw,url + 'REST/subjects?ID=' + subjId + '&format=json' ], stdout = sp.PIPE)
curlout=pipeout.stdout.read()
jsonobj=json.loads(curlout)
results=jsonobj.get('ResultSet').get('Result')
subjLbl=subjId
any=False
for r in results:
	subjLbl = r.get('label')

logging.info('fileName=' + fileName)
logging.info('optFileName=' + optFileName)

## Get list of scans
logging.info('\ncurl ' + ' -u ' + user + ':xxxxxx' + ' ' + url + 'REST/experiments/' + expId + '/resources?all=true&file_stats=true&format=json')
pipeout = sp.Popen(['curl','-u',user + ':' + pw,url + 'REST/experiments/' + expId + '/resources?all=true&file_stats=true&format=json' ], stdout = sp.PIPE)
curlout=pipeout.stdout.read()
jsonobj=json.loads(curlout)

## Search for MPRAGE scans 
results=jsonobj.get('ResultSet').get('Result')

resource=''
any=False
for r in results:
	if r.get('cat_desc')=='MPRAGE': 
		resource = resource + ',' + r.get('xnat_abstractresource_id')
	 	any=True

## Return list of files, see if contains matching file name (or optional (skull-stripped)) file name

## Per Dan, we'll just use the skull-stripped files
workFile=fileName
##workFile=''
##isSkullStripped=True
##if any:
##	logging.info('\ncurl'+'-u'+user + ':xxxxxx ' + url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files?format=json')
##	pipeout = sp.Popen(['curl','-u',user + ':' + pw,url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files?format=json'],stdout=sp.PIPE)
##	curlout=pipeout.stdout.read()
##	jsonobj=json.loads(curlout)
##	logging.info('\n' + curlout)
##	results=jsonobj.get('ResultSet').get('Result')
##	for r in results:
##		if r.get('Name')==fileName:
##			workFile=fileName
##			isSkullStripped=False
##		elif r.get('Name')==optFileName:
##			if len(workFile)<1:
##				workFile=fileName
##else:
##	logging.error('\n\nERROR:  Required scan and NIfTI output do not exist')
##	sys.exit(1)
##
##if len(workFile)<1:
##	logging.error('\n\nERROR:  Required input files do not exist')
##	sys.exit(1)

## Copy file to working directory
workf = open(currworkdir + 'NIFTI/' + workFile,'w',0)
spout = sp.call(['curl','-u',user + ':' + pw,url +  'REST/experiments/' + expId + '/resources/' + resource[1:] + '/files/' + workFile],stdout=workf)
workf.close()

## Matt's Surface Freg program expects input NIFTI file to be gz compressed.  (the fslmaths section creates a gz file,
## but it will not be used if we don't gz it first).  Since we're working with a copy, we'll gz it here.
## FreeSurfer can work with the gzipped file
if workFile.lower().endswith('.nii'):
	fsrc = sp.call(['gzip','-f',currworkdir + 'NIFTI/' + workFile])
	if fsrc==0 and os.path.exists(currworkdir + 'NIFTI/' + workFile + '.gz'):
		workFile += '.gz'
	else:
		logging.error('\n\nERROR:  Surface processing currently requires a zipped NIFTI file and the zipping process failed')
		sys.exit(1)

if not workFile.lower().endswith('.nii.gz'):
	logging.error('\n\nERROR:  File type is invalid (requires .nii.gz file)')
	sys.exit(1)

logging.info('workFile=' + workFile)

## Run surface registration code
try:
	sregresult = sp.Popen([scriptpath + '/includes/run_surfacereg.sh',rserver,ruser,currworkdir + 'NIFTI/' + workFile,expLbl,currworkdir + 'SurfaceReg'],stdout=sp.PIPE)
	sregout=sregresult.stdout.read()
	logging.info(sregout)
	sregresult.wait()
	if sregresult.returncode!=0:
		logging.error('\n\nERROR:  Caret Conversion/Surface Registration processing run was unsuccessful - returnCode=' + str(sregresult.returncode))
		sys.exit(sregresult.returncode)

	##fsrc = sp.call(['/mnt/home/michael/projects/workspace_intradb/xdat_release_hcp_intradb/fspipeline/FreeSurfer2CaretConvertAndRegisterClean.sh',
	##	expLbl,currworkdir + 'SurfaceReg/caret',currworkdir + 'SurfaceReg/data',currworkdir + 'FreeSurfer/' + expLbl,currworkdir + 'NIFTI/' + workFile])
	##if fsrc!=0:
	##	logf.write('\n\nERROR:  Caret Conversion/Surface Registration processing could not be completed successfully - ' + str(fsrc))
except OSError, e:
	logf.write('\n\nERROR:  Caret Conversion/Surface Registration processing could not be completed successfully - ' + str(e))

## Zip up SurfaceReg output and add as resource files
os.chdir(currworkdir + 'SurfaceReg')
zrc = sp.call(['zip','-r','../surfacereg.zip','.'])
os.chdir(currworkdir)

spout = sp.call(['curl','-u',user + ':' + pw,'-X','PUT',url +  'REST/experiments/' + expId + '/resources/MPRAGE_SURFACE_REG'])
spout = sp.call(['curl','-u',user + ':' + pw,'-X','PUT','-T','surfacereg.zip',url +  'REST/experiments/' + expId + '/resources/MPRAGE_SURFACE_REG/files/surfacereg.zip?inbody=true&extract=true'])
        
## Clean-up
os.remove('surfacereg.zip')


