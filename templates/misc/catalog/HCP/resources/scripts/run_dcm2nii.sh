#!/bin/bash

if [ "${1}" == "-version" ] ||  [ "${1}" == "--version" ] ||  [ "${2}" == "-version" ] ||  [ "${2}" == "--version" ] ; then
   echo "1.0"
   exit 0
fi

## IMPORTANT:  When pointing to a new version, clear the dcm2nii.ini file in the ~/.dcm2nii directory and run the command
##             to generate a new one.  Then compare that file to the one we're using to look for changes that should be
##             made corresponding to the new version. 
COMMAND_LINE=/nrgpackages/tools.release/mricronlx64-2012.07.20/dcm2nii
OUT_DIR=`pwd`

while [ $# -ne 0 ] ; do
        if [ "${1}" == "-o" ]; then
                COMMAND_LINE="$COMMAND_LINE -o ."
                shift
                OUT_DIR="${1}"
        else
                COMMAND_LINE="$COMMAND_LINE $1"
        fi
        shift
done

pushd $OUT_DIR

$COMMAND_LINE

rc=$?

if [ $rc -ne 0 ] ; then

        $COMMAND_LINE
        rc=$?

fi

popd

exit $rc

