#!/bin/bash -x
set -e

#This script will generate a job file for DiffusionHCP processing

#DB Diffusion Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster

isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}


ARGS=6
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outFile Path_to_outPutJobFile MPP_PARAM_FILE_PATH"
  exit 1
fi


paramsFile=$1
mpp_pipelineparamsFile=$2
passwd=$3
outDir=$4
mppParamsFile=$5
jsession=$6

dirname=`dirname "$program"`
configdir="${dirname}/config"


source $paramsFile
source $mpp_pipelineparamsFile

source $mppParamsFile

PIPELINE_NAME="DiffusionHCP/DiffusionHCP.xml"


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi

if [ $compute_cluster = CHPC ] ; then
   configurationForPreEddyJobFile=$configdir/CHPC/diffusionHCP_preeddy.pbs.config  
   configurationForEddyJobFile=$configdir/CHPC/diffusionHCP_eddy.pbs.config  
   configurationForPostEddyJobFile=$configdir/CHPC/diffusionHCP_posteddy.pbs.config  
   configurationForPutJobFile=$configdir/CHPC/diffusionHCP_put.pbs.config  
   processingParamsFile=$configdir/CHPC/diffusionHCP.pbs.param   	
   logDirectiveFile=$configdir/CHPC/log.pbs.config	
elif [ $compute_cluster = NRG ] ; then
   configurationForPreEddyJobFile=$configdir/NRG/diffusionHCP_preeddy.sge.config  
   configurationForEddyJobFile=$configdir/NRG/diffusionHCP_eddy.sge.config  
   configurationForPostEddyJobFile=$configdir/NRG/diffusionHCP_posteddy.sge.config  
   configurationForPutJobFile=$configdir/NRG/diffusionHCP_put.sge.config  
   processingParamsFile=$configdir/NRG/diffusionHCP.sge.param   	
   logDirectiveFile=$configdir/NRG/log.sge.config	
fi

if [ ! -f $configurationForPreEddyJobFile ] ; then
  echo "File at $configurationForPreEddyJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $configurationForEddyJobFile ] ; then
  echo "File at $configurationForEddyJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $configurationForPostEddyJobFile ] ; then
  echo "File at $configurationForPostEddyJobFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $configurationForPutJobFile ] ; then
  echo "File at $configurationForPutJobFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $logDirectiveFile ] ; then
  echo "File at $logDirectiveFile doesnt exist. Aborting!"
  exit 1;
fi


#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet diffusion_EchoSpacing
isSet diffusion_PhaseEncodingDir
isSet diffusion_DiffusionDirDictNames[0]
isSet diffusion_DiffusionDirDictValues[0]
isSet diffusion_DiffusionScanDictValues[0]
isSet diffusion_DiffusionScanDictNames[0]

if [[ ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionDirDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictNames[@]}  ]]; then 
  echo " The array lengths of variables diffusion_DiffusionDirDictNames, diffusion_DiffusionDirDictValues, diffusion_DiffusionScanDictValues, diffusion_DiffusionScanDictNames dont match. Aborting! " 
  exit 1
fi 

###########################################################
# Continue - looks good
#
###########################################################

outPreEddyFile=${outDir}${subject}_pre_eddy.sh
outEddyFile=${outDir}${subject}_eddy.sh
outPostEddyFile=${outDir}${subject}_post_eddy.sh
outPutJobFile=${outDir}${subject}_diffusion_put.sh

touch $outPreEddyFile
touch $outEddyFile
touch $outPostEddyFile

touch $outPutJobFile

if [ ! -f $outPreEddyFile ] ; then
  echo "File at $outPreEddyFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $outEddyFile ] ; then
  echo "File at $outEddyFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $outPostEddyFile ] ; then
  echo "File at $outPostEddyFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $outPutJobFile ] ; then
  echo "File at $outPutJobFile doesnt exist. Aborting!"
  exit 1;
fi


cat $configurationForPreEddyJobFile > $outPreEddyFile
cat $logDirectiveFile >> $outPreEddyFile	
 
cat $configurationForEddyJobFile > $outEddyFile
cat $logDirectiveFile >> $outEddyFile	

cat $configurationForPostEddyJobFile > $outPostEddyFile
cat $logDirectiveFile >> $outPostEddyFile	


cat $configurationForPutJobFile > $outPutJobFile
cat $logDirectiveFile >> $outPutJobFile	


workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
if [ $? -ne 0 ] ; then
	echo "Fetching workflow for structural failed. Aborting!"
	exit 1
fi 

commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline $PIPELINE_NAME -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $useremail -notify $adminemail -parameter adminemail=$adminemail -parameter useremail=$useremail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject  -parameter templatesdir=$templatesdir  -parameter configdir=$configdir -parameter CaretAtlasDir=$CaretAtlasDir -parameter EchoSpacing=$diffusion_EchoSpacing -parameter PhaseEncodingDir=$diffusion_PhaseEncodingDir -parameter posData="$diffusion_posData" -parameter negData="$diffusion_negData" -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir  -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix -workFlowPrimaryKey $workflowID"

#Now for the 95, 96, 97 Direction scans append the parameters to the command

index=0
for diffParameter in "${diffusion_DiffusionDirDictNames[@]}"
do
	diffDirParameterValue=${diffusion_DiffusionDirDictValues[$index]}
	diffParameterValue=${diffusion_DiffusionScanDictValues[$index]}
	diffParameterName=${diffusion_DiffusionScanDictNames[$index]}
	
	commandStr=" $commandStr -parameter ${diffParameter}=${diffDirParameterValue} -parameter ${diffParameterName}=${diffParameterValue} "
	index=$((index+1))
done

eddyCommandStr="$commandStr -startAt 7b"
postEddyCommandStr="$commandStr -startAt 7c"

putCommandStr="$commandStr -startAt 12"

###########################################################
# Create PRE-EDDY JOB
#
###########################################################

echo "Creating $outPreEddyFile" 

echo "echo \" \"" >> $outPreEddyFile
echo "$commandStr" >> $outPreEddyFile
echo "rc_command=\$?" >> $outPreEddyFile

echo "echo \$rc_command \" \"" >> $outPreEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outPreEddyFile


echo "exit \$rc_command" >> $outPreEddyFile

chmod +x $outPreEddyFile


###########################################################
# Create EDDY JOB
#
###########################################################

echo "Creating $outEddyFile" 

echo "echo \" \"" >> $outEddyFile
echo "$eddyCommandStr" >> $outEddyFile
echo "rc_command=\$?" >> $outEddyFile

echo "echo \$rc_command \" \"" >> $outEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outEddyFile


echo "exit \$rc_command" >> $outEddyFile

chmod +x $outEddyFile


###########################################################
# Create POST-EDDY JOB
#
###########################################################

echo "Creating $outPostEddyFile" 

echo "echo \" \"" >> $outPostEddyFile
echo "$postEddyCommandStr" >> $outPostEddyFile

echo "rc_command=\$?" >> $outPostEddyFile

echo "echo \$rc_command \" \"" >> $outPostEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outPostEddyFile


echo "exit \$rc_command" >> $outPostEddyFile

chmod +x $outEddyFile

###########################################################
# Create PUT JOB
#
###########################################################

echo "Creating $outPutJobFile" 

echo "echo \" \"" >> $outPutJobFile
echo "$putCommandStr" >> $outPutJobFile
echo "rc_command=\$?" >> $outPutJobFile

echo "echo \$rc_command \" \"" >> $outPutJobFile
echo "echo \"Job finished  at \`date\`\"" >> $outPutJobFile


echo "exit \$rc_command" >> $outPutJobFile

chmod +x $outPutJobFile

exit 0

