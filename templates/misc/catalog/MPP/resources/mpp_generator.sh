#!/bin/bash -x
set -e

#This script will generate a job file for launching all pipelines for a specific Cluster (CHPC/NRG)

#DB Data Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file

program=$0

paramsFile=$1
mppparamsFile=$2
passwd=$3



path_to_scripts=`dirname "$program"`
configdir=$path_to_scripts/config

if [ ! -f $paramsFile ] ; then
  echo "File at paramsFile: $paramsFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $mppparamsFile ] ; then
  echo "File at mppparamsFile: $mppparamsFile doesnt exist. Aborting!"
  exit 1;
fi

source $paramsFile
source $mppparamsFile

#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi


if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/mpp.pbs.config  
   processingParamsFile=$configdir/CHPC/mpp.pbs.param   	
fi

if [ X$compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/NRG/mpp.sge.config
   processingParamsFile=$configdir/NRG/mpp.sge.param   	
fi


#Processing param file should contain path to the packaging_outdir

if [ ! -f $processingParamsFile ] ; then
  echo "File at processingParamsFile: $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

JSESSION=`curl  -u $user:$passwd ${host}data/JSESSION`
if [ $? -ne 0 ] ; then
	echo "Unable to get a JSESSION Aborting!"
	exit 1
fi


################################################
#Create the custom Structural launch script
################################################

if [ $launchStructural -eq 1 ] || [ $launchStructuralPackageOnly -eq 1 ] ; then
   $path_to_scripts/structural_call_generator.sh $paramsFile $mppparamsFile $passwd $workdir/${subject}_structural.sh $workdir/${subject}_structural_put.sh $processingParamsFile $JSESSION
   if [ $? -ne 0 ] ; then
      echo "Unable to launch the $path_to_scripts/structural_call_generator.sh Aborting!"
      exit 1
   fi
fi


################################################
#Create the custom Functional launch scripts
################################################

if [ $launchFunctional -eq 1 ] ; then
   $path_to_scripts/functional_call_generator.sh $paramsFile $mppparamsFile $passwd $workdir $processingParamsFile $JSESSION
   if [ $? -ne 0 ] ; then
      echo "Unable to launch the $path_to_scripts/functional_call_generator.sh Aborting!"
      exit 1
   fi
fi

	
################################################
#Create the custom Diffusion launch script
################################################

if [ $launchDiffusion -eq 1 ] ; then
  $path_to_scripts/diffusion_call_generator.sh $paramsFile $mppparamsFile $passwd $workdir $processingParamsFile $JSESSION
  if [ $? -ne 0 ] ; then
     echo "Unable to launch the $path_to_scripts/diffusion_call_generator.sh Aborting!"
     exit 1
  fi
fi



################################################
#Create the custom ICA+FIX Analysis launch script
################################################

if [ $launchICAFIX -eq 1 ] ; then
  $path_to_scripts/icafix_call_generator.sh $paramsFile $mppparamsFile $passwd $workdir $processingParamsFile $JSESSION
  if [ $? -ne 0 ] ; then
     echo "Unable to launch the $path_to_scripts/icafix_call_generator.sh Aborting!"
     exit 1
  fi
fi


################################################
#Create the custom Task-fMRI Analysis launch script
################################################

if [ $launchTask -eq 1 ] ; then
  echo "Launching Task"
  $path_to_scripts/task_fmri_call_generator.sh $paramsFile $mppparamsFile $passwd $workdir $processingParamsFile $JSESSION
  if [ $? -ne 0 ] ; then
     echo "Unable to launch the $path_to_scripts/task_fmri_call_generator.sh Aborting!"
     exit 1
  fi
fi







################################################
#Now submit jobs and take care of dependency
################################################


if [ $compute_cluster = CHPC ] ; then
   $path_to_scripts/queue_chpc_jobs.sh $paramsFile $passwd $workdir
elif [ X$compute_cluster = NRG ] ; then
   $path_to_scripts/queue_sge_jobs.sh  $paramsFile $passwd $workdir
fi

exit 0;
