#!/bin/bash -x
set -e

#This script will generate a job file for FunctionalHCP pipeline processing

#DB Functional Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster

isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}

ARGS=6
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outDir MPP_PARAM_FILE_PATH"
  exit 1
fi

paramsFile=$1
mpp_pipelineparamsFile=$2
passwd=$3
outDir=$4
mppParamsFile=$5
jsession=$6

source $paramsFile
source $mpp_pipelineparamsFile

source $mppParamsFile


PIPELINE_NAME="FunctionalHCP/FunctionalHCP.xml"


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi


dirname=`dirname "$program"`
pipeline_configdir="${dirname}/config"

if [ $compute_cluster = CHPC ] ; then
   processingParamsFile=$pipeline_configdir/CHPC/functionalHCP.pbs.param   	
   logDirectiveFile=$pipeline_configdir/CHPC/log.pbs.config	
elif [ $compute_cluster = NRG ] ; then
   processingParamsFile=$pipeline_configdir/NRG/functionalHCP.sge.param   	
   logDirectiveFile=$pipeline_configdir/NRG/log.sge.config	
fi

if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $logDirectiveFile ] ; then
  echo "File at $logDirectiveFile doesnt exist. Aborting!"
  exit 1;
fi


#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile




###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet functional_rl_fieldmapseries
isSet functional_lr_fieldmapseries
isSet functional_TE
isSet functional_DistortionCorrection
isSet functional_scanid[0]
isSet functional_functionalseries[0]
isSet functional_DwellTimes[0]
isSet functional_UnwarpDir[0]

if [[ ${#functional_scanid[@]} -ne ${#functional_functionalseries[@]} || ${#functional_scanid[@]} -ne ${#functional_DwellTimes[@]} || ${#functional_scanid[@]} -ne ${#functional_UnwarpDir[@]} ]] ; then
  echo "The array lengths of variables functional_scanid, functional_functionalseries, functional_DwellTimes, functional_UnwarpDir do not match. Aborting!" 
  exit 1
fi

###########################################################
# Continue - looks good
#
###########################################################


configurationForPutJobFileName="functionalHCP_put"


index=0
for fscan in "${functional_scanid[@]}"
do
  #For each scan create the outfile of command to launch the Functional processing pipeline

	seriesDescription=${functional_functionalseries[$index]}
	configurationForJobFileName="functionalHCP_"${seriesDescription}

	fsf_configurationForJobFileName="functionalHCP_fsf"

	seriesRoot=`echo $seriesDescription | awk '{gsub(/_LR/,""); gsub(/_RL/,""); print}'`


	if [ $compute_cluster = CHPC ] ; then
	   configurationForJobFile=$pipeline_configdir/CHPC/${configurationForJobFileName}.pbs.config  
	   configurationForPutJobFile=$pipeline_configdir/CHPC/${configurationForPutJobFileName}.pbs.config  
	   fsf_configurationForJobFile=$pipeline_configdir/CHPC/${fsf_configurationForJobFileName}.pbs.config  
	   #processingParamsFile=$pipeline_configdir/CHPC/functionalHCP.pbs.param   	
	elif [ X$compute_cluster = NRG ] ; then
	   configurationForJobFile=$pipeline_configdir/NRG/${configurationForJobFileName}.sge.config
	   configurationForPutJobFile=$pipeline_configdir/NRG/${configurationForPutJobFileName}.sge.config  
	   fsf_configurationForJobFile=$pipeline_configdir/NRG/${fsf_configurationForJobFileName}.sge.config  
	   #processingParamsFile=$pipeline_configdir/NRG/functionalHCP.sge.param   	
	fi

	if [ ! -f $configurationForJobFile ] ; then
	  echo "File at $configurationForJobFile doesnt exist. Aborting!"
	  exit 1;
	fi

	if [ ! -f $configurationForPutJobFile ] ; then
	  echo "File at $configurationForPutJobFile doesnt exist. Aborting!"
	  exit 1;
	fi


	outFile=$outDir/${subject}_${seriesDescription}_functional.sh
	outPutJobFile=$outDir/${subject}_${seriesDescription}_functional_put.sh

	fsf_packaging_outFile=$outDir/${subject}_${seriesRoot}_end.sh

	touch $outFile
	touch $outPutJobFile

	if [ ! -f $outFile ] ; then
	  echo "File at $outFile doesnt exist. Aborting!"
	  exit 1;
	fi

	if [ ! -f $outPutJobFile ] ; then
	  echo "File at $outPutJobFile doesnt exist. Aborting!"
	  exit 1;
	fi
	
	

	cat $configurationForJobFile > $outFile
	cat $logDirectiveFile >> $outFile	
	
	cat $configurationForPutJobFile > $outPutJobFile
	cat $logDirectiveFile >> $outPutJobFile	
	
	
	
	rl_fieldmap=$functional_rl_fieldmapseries
	lr_fieldmap=$functional_lr_fieldmapseries
	dwellTime=${functional_DwellTimes[$index]}
	TE=$functional_TE
	unwarpDir=${functional_UnwarpDir[$index]}
	distortionCorrection=$functional_DistortionCorrection

	workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
	if [ $? -ne 0 ] ; then
		echo "Fetching workflow for functional failed. Aborting!"
		exit 1
	fi 


	commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline $PIPELINE_NAME -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $useremail -notify $adminemail -parameter adminemail=$adminemail -parameter useremail=$useremail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject  -parameter functionalscanid=$fscan -parameter TE=$TE  -parameter functionalseries=$seriesDescription -parameter lr_fieldmapseries=$lr_fieldmap -parameter rl_fieldmapseries=$rl_fieldmap  -parameter DwellTime=$dwellTime -parameter UnwarpDir=$unwarpDir -parameter DistortionCorrection=$distortionCorrection -parameter templatesdir=$templatesdir  -parameter configdir=$configdir -parameter CaretAtlasDir=$CaretAtlasDir -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix -workFlowPrimaryKey $workflowID"
	putCommandStr="$commandStr -startAt 33"

	echo "Creating $outFile" 

        echo "echo \" \"" >> $outFile
	echo "$commandStr" >> $outFile
	echo "rc_command=\$?" >> $outFile
	
	echo "echo \$rc_command \" \"" >> $outFile
	echo "echo \"Job finished  at \`date\`\"" >> $outFile

	echo "exit \$rc_command" >> $outFile
	
	chmod +x $outFile

###########################################################
# Create PUT JOB
#
###########################################################


	echo "Creating $outPutJobFile" 

        echo "echo \" \"" >> $outPutJobFile
	echo "$putCommandStr" >> $outPutJobFile
	echo "rc_command=\$?" >> $outPutJobFile
	
	echo "echo \$rc_command \" \"" >> $outPutJobFile
	echo "echo \"Job finished  at \`date\`\"" >> $outPutJobFile

	echo "exit \$rc_command" >> $outPutJobFile
	
	chmod +x $outPutJobFile

###########################################################
# Create FSF and Package Creation JOB
#
###########################################################

	if [ ! -f $fsf_packaging_outFile ] ; then
	   touch $fsf_packaging_outFile
	   cat $fsf_configurationForJobFile > $fsf_packaging_outFile
	   cat $logDirectiveFile >> $fsf_packaging_outFile	

	   xnat_hostRoot=`echo $xnat_host | awk '{gsub(/http:\/\//,""); gsub(/:8080\//,""); gsub(/https:\/\//,""); gsub(/\//,""); print}'`
	   fsf_builddir=$builddir/FSF/$seriesRoot
	   mkdir -p $fsf_builddir
           echo "Creating $fsf_packaging_outFile" 

	   if [[ $seriesRoot == tfMRI* ]] ; then
		   fsf_commandStr="$NRG_PACKAGES/tools/HCP/FSF/callCreateFSFs.sh --host $xnat_hostRoot --user $user --pw $passwd --buildDir $fsf_builddir --project $project --subject $subject --series $seriesRoot"

		   echo "echo \" \"" >> $fsf_packaging_outFile
		   echo "$fsf_commandStr" >> $fsf_packaging_outFile
	   	   echo "echo \" \"" >> $fsf_packaging_outFile

	   fi


           packaging_commandStr="source $SCRIPTS_HOME/epd-python_setup.sh; source $SCRIPTS_HOME/groovy_setup.sh;   $NRG_PACKAGES/tools/packaging/callPackager.sh --host $xnat_hostRoot --user $user --pw $passwd --outDir $packaging_outdir --buildDir $builddir/FUNCTIONAL/$fscan/$subject/ --project $project --subject $subject --outputFormat PACKAGE --outputType PREPROC --packet FUNCTIONAL --series $seriesRoot"

	   echo "echo \" \"" >> $fsf_packaging_outFile
	   echo "$packaging_commandStr" >> $fsf_packaging_outFile

	   echo "echo \" \"" >> $fsf_packaging_outFile
	   echo "echo \"Job finished  at \`date\`\"" >> $fsf_packaging_outFile

	   echo "exit 0;" >> $fsf_packaging_outFile
	   chmod +x $fsf_packaging_outFile
        fi
  
	index=$((index+1))


done


exit 0;

