import StringIO		# Read string buffers as if they were files (i.e. read in-memory files)
import csv			# Comma Separated Value (csv) reading

# Constants

# This CSV file exists in each unprocessed resource to let us know what scan
# numbers correspond to each image file in the resource
SCAN_NUMBER_MAPPING_FILE_NAME = 'filescans.csv'

# Resource label suffix indicating that a resource is an unprocessed resource
UNPROC_SUFFIX = "_unproc"

# File name extension for Compressed NIFTI files
COMPRESSED_NIFTI_EXTENSION = '.nii.gz'

def create_resource_file_name_to_scan_number_map( xnat_access ):
	"""
	create a map/dict which when given a file name will return the scan number that corresponds
	to that file name
	"""
	
	file_name_to_scan_number = dict()
	file_name_list = xnat_access.get_resource_file_name_list()
	if SCAN_NUMBER_MAPPING_FILE_NAME in file_name_list:
		scan_mapping_str = xnat_access.get_named_resource_file_content(SCAN_NUMBER_MAPPING_FILE_NAME)
		f = StringIO.StringIO(scan_mapping_str)
		reader = csv.reader(f, delimiter=',')
		for row in reader:
			key = row[1].replace("'", "")
			val = row[0].replace("'", "")
			if val != 'Scan':
				file_name_to_scan_number[key] = val
				
	return file_name_to_scan_number

def is_unprocessed_resource( resource_label ):
	return resource_label.endswith(UNPROC_SUFFIX)

def get_unprocessed_resource_base_name( label ):
	return label[:len(label)-len(UNPROC_SUFFIX)]

def get_primary_scan_name_for_resource( xnat_access, resource_name ):
	return xnat_access.session + '_' + resource_name + COMPRESSED_NIFTI_EXTENSION

