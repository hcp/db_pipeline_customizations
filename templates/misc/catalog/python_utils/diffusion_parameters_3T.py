from diffusion_parameters import DiffusionParameters
import debug_util

class DiffusionParameters3T(DiffusionParameters):
	
	def __init__(self, debugOn=False):
		super(DiffusionParameters3T, self).__init__(debugOn)
		self._debug_DiffusionParameters3T = debug_util.DebugUtil("DiffusionParameters3T")
		self._debug_DiffusionParameters3T.show_debug_msgs = debugOn
		
		# attributes inherited from superclass, but set uniquely for this subclass
		self._phase_encoding_dir = '1' # '1' ==> RL/LR; '2' ==> AP/PA
		self._pos_suffix = 'RL'
		self._neg_suffix = 'LR'
		self._detailed_outlier_stats = False
		self._replace_outliers = False
		
	def add_scan(self, scan_number, direction, scan_dir_number, echo_spacing):
		if direction == "LR":
			self._neg_dir_count = self._neg_dir_count + 1
			self._dir_names.append("LR_Dir" + str(self._neg_dir_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("LR_" + str(self._neg_dir_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)
			
		elif direction == "RL":
			self._pos_dir_count = self._pos_dir_count + 1
			self._dir_names.append("RL_Dir" + str(self._pos_dir_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("RL_" + str(self._pos_dir_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)
			
		else:
			print("DiffusionParameters3T: ERROR: Unrecognized direction: " + direction)
			sys.exit(1)
			
