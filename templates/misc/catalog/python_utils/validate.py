
def check_equal(actual, expected, fail_message):
    if (actual == expected):
        return True
    else:
        print fail_message 
        print "actual: '" + str(actual) + "' NOT EQUAL TO expected: '" + str(expected) + "'" 
        return False

def check_gte(actual, lower_bound, fail_message):
    if (actual >= lower_bound):
        return True
    else:
        print fail_message
        print "actual: '" + str(actual) + "' NOT GREATER THAN OR EQUAL TO lower_bound: '" + str(lower_bound) + "'"
        return False

def check_true(test_value, fail_message):
    if (test_value):
        return True
    else:
        print fail_message
        return False
