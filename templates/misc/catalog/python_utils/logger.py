import datetime

class Logger(object):

	def __init__(self):
		super(Logger, self).__init__()
		self._show_time = False
		
	# property: show_time
	
	def _get_show_time(self):
		return self._show_time
	
	def _set_show_time(self, new_tf_value):
		self._show_time = new_tf_value
		
	show_time=property(_get_show_time, _set_show_time)
	
	def log(self, msg):
		log_string = ""
		if self._show_time: 
			log_string = datetime.datetime.now().strftime("%Y-%m-%d %H%M%S") + " " + log_string
			
		log_string = log_string + str(msg)
		print(log_string)
