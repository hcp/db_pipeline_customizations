from structural_scan_parameters import StructuralScanParameters

class T1wParameters(StructuralScanParameters):
    """
    Class for containing and managing parameters associated with a T1w structural scan
    """
    def __init__(self):
        super(T1wParameters, self).__init__()
