
class DebugUtil(object):
	
	def __init__(self, module_name):
		super(DebugUtil, self).__init__()
		self._module_name = module_name
		self._show_debug_msgs = False
		
	# property show_debug_msgs
	
	def _get_show_debug_msgs(self):
		return self._show_debug_msgs
	
	def _set_show_debug_msgs(self, new_show_debug_msgs):
		self._show_debug_msgs = new_show_debug_msgs
		
	show_debug_msgs=property(_get_show_debug_msgs, _set_show_debug_msgs)
	
	def show(self, msg):
		if self.show_debug_msgs: print "DEBUG: " + str(self._module_name) + " - " + str(msg)
