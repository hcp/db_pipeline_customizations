from diffusion_parameters import DiffusionParameters
import debug_util

# Constants

KEY_DIFFUSION_DWINAME='diffusion_dwiname'

class DiffusionParameters7T(DiffusionParameters):
	
	def __init__(self, debugOn=False):
		super(DiffusionParameters7T, self).__init__(True) # debugOn
		self._debug_DiffusionParameters7T = debug_util.DebugUtil("DiffusionParameters7T")
		self._debug_DiffusionParameters7T.show_debug_msgs = True # debugOn
		
		# attributes inherited from superclass but set uniquely for this subclass
		self._phase_encoding_dir = '2' # '1' ==> RL/LR; '2' ==> AP/PA
		self._pos_suffix = 'PA'
		self._neg_suffix = 'AP'
		self._detailed_outlier_stats = True
		self._replace_outliers = False
		
		# new attributes of this class
		self._dwiname = "Diffusion_7T"
		
	def add_scan(self, scan_number, direction, scan_dir_number, echo_spacing):
		if direction == "PA":
			self._neg_dir_count = self._neg_dir_count + 1
			self._dir_names.append("PA_Dir" + str(self._neg_dir_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("PA_" + str(self._neg_dir_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)
			
		elif direction == "AP":
			self._pos_dir_count = self._pos_dir_count + 1
			self._dir_names.append("AP_Dir" + str(self._pos_dir_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("AP_" + str(self._pos_dir_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)
			
		else:
			print("DiffusionParameters7T: ERROR: Unrecognized direction: " + direction)
			sys.exit(1)
			
	def add_xnat_pipeline_parameters(self, parameters):
		super(DiffusionParameters7T, self).add_xnat_pipeline_parameters(parameters)
		parameters.addUniqueParameter(KEY_DIFFUSION_DWINAME, self._dwiname)
