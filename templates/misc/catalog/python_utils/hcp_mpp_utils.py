import time
import numpy
from workflow import Workflow

def get_build_dir_root(project):
	db_str = 'hcpdb'
	build_dir_root = '/data/%s/build/%s/' % (db_str, project)
	return build_dir_root

def get_timestamped_subject_dir(subject):
	sTime = time.time()
	timestamped_dir = str(numpy.asarray(round(sTime), dtype=numpy.uint64))
	timestamped_subject_dir = timestamped_dir + '_' + subject
	return timestamped_subject_dir

def get_parameter_file_path(project, timestamped_subject_dir, subject):
	build_dir_root = get_build_dir_root(project)
	timestamped_subject_dir = get_timestamped_subject_dir(subject)
	subject_build_dir = build_dir_root + timestamped_subject_dir
	parameter_file_path = '%s/%s.xml' % (subject_build_dir, subject)
	return parameter_file_path

# Constants
DB_STR = 'hcpdb'
MPP_PIPELINE_NAME = 'MPP/MPP.xml'
DATA_TYPE_STR = 'xnat:mrSessionData'
XNAT_SERVER_STR = 'ConnectomeDB'
NOTIFY_USER_STR = 'hilemanm@mir.wustl.edu'          # Need to make this configurable instead of hard coded
NOTIFY_ADMIN_STR = 'db-admin@humanconnectome.org'   # Need to make this configurable instead of hard coded
MAIL_HOST_STR = 'mail.nrg.wustl.edu'
USER_FULL_NAME_STR = 'MPPUser'

def build_pipeline_submission_str(
								user, password, server,
								project, xnat_session_id,
								subject, shadow_number,
								pipeline_project, timestamped_subject_dir, parameter_file_path,
								current_jsession):

	job_submitter = '/data/%s/pipeline/bin/schedule ' % (DB_STR)
	pipeline_launcher = '/data/%s/pipeline/bin/XnatPipelineLauncher ' % (DB_STR)
	
	mpp_pipeline_name = MPP_PIPELINE_NAME
	if pipeline_project:
		mpp_pipeline_name = pipeline_project + '/' + mpp_pipeline_name
	launcher_pipeline = ' -pipeline %s ' % (mpp_pipeline_name)
	
	launcher_hcp_id = ' -id %s ' % xnat_session_id
	data_type = ' -dataType %s ' % (DATA_TYPE_STR)
	host = ' -host http://db-shadow' + shadow_number + '.nrg.mir:8080'
	xnat_server = ' -parameter xnatserver=%s ' % (XNAT_SERVER_STR)
	launcher_external_project = ' -project %s ' % (project)
	launcher_user = ' -u %s ' % user
	launcher_password = ' -pwd %s ' % password
	suppress_notify = ' -supressNotification '
	notify_user = ' -notify %s ' % (NOTIFY_USER_STR)
	notify_admin = ' -notify %s ' % (NOTIFY_ADMIN_STR)
	admin_email = ' -parameter adminemail=%s ' % (NOTIFY_ADMIN_STR)
	user_email = ' -parameter useremail=%s ' % (NOTIFY_USER_STR)
	mail_host = ' -parameter mailhost=%s ' % MAIL_HOST_STR
	user_full_name = ' -parameter userfullname=%s ' % (USER_FULL_NAME_STR)
	build_dir_str = get_build_dir_root(project) + timestamped_subject_dir
	build_dir = ' -parameter builddir=%s ' % (build_dir_str)
	launcher_subject_parameter = ' -parameter subject=%s ' % subject
	launcher_parameter_file = ' -parameter paramfile=%s ' % (parameter_file_path)
	launcher_compute_cluster = ' -parameter compute_cluster=CHPC '
	
	workflow_obj = Workflow(user, password, 'https://'+server+'/', current_jsession)
	workflow_id = workflow_obj.createWorkflow(xnat_session_id, project, mpp_pipeline_name, 'Queued')
	launcher_workflow_primary_key = ' -workFlowPrimaryKey %s ' % workflow_id
	
	submit_str = \
		job_submitter + \
		pipeline_launcher + \
		launcher_pipeline + \
		launcher_hcp_id + \
		data_type + \
		host + \
		xnat_server + \
		launcher_external_project + \
		launcher_user + \
		launcher_password + \
		suppress_notify + \
		notify_user + \
		notify_admin + \
		admin_email + \
		user_email + \
		mail_host + \
		user_full_name + \
		build_dir + \
		launcher_subject_parameter + \
		launcher_parameter_file + \
		launcher_compute_cluster + \
		launcher_workflow_primary_key
		
	return submit_str

