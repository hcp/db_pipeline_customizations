
from xnat_access import XnatAccess

class DatabaseCredentials(object):
	"""
	Credentials for database access
	"""
	
	def __init__(self):
		super(DatabaseCredentials, self).__init__()
		self._server = ''
		self._username = ''
		self._password = ''
		
	def __init__(self, init_server, init_username, init_password):
		super(DatabaseCredentials, self).__init__()
		self._server = init_server
		self._username = init_username
		self._password = init_password
		
	# property server
	
	def _get_server(self):
		return self._server
	
	def _set_server(self, new_server):
		self._server = new_server
		
	server=property(_get_server, _set_server)
	
	# property username
	
	def _get_username(self):
		return self._username
	
	def _set_username(self, new_username):
		self._username = new_username
		
	username=property(_get_username, _set_username)
	
	# property password
	
	def _get_password(self):
		return self._password
	
	def _set_password(self, new_password):
		self._password = new_password
		
	password=property(_get_password, _set_username)
	
def get_token_credentials( orig_credentials ):
	xnat_access = XnatAccess(orig_credentials.server, orig_credentials.username, orig_credentials.password)
	new_username, new_password = xnat_access.get_new_tokens()
	new_db_credentials = DatabaseCredentials(orig_credentials.server, new_username, new_password)
	return new_db_credentials
