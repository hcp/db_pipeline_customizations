import debug_util
import xnat_pipeline_parameters

# Constants

KEY_XNAT_ID = 'xnat_id'
KEY_SUBJECT = 'subject'
KEY_SESSION_ID = 'sessionId'
KEY_LAUNCH_STRUCTURAL = 'launchStructural'
KEY_LAUNCH_FUNCTIONAL = 'launchFunctional'
KEY_LAUNCH_DIFFUSION = 'launchDiffusion'
KEY_LAUNCH_TASK_ANALYSIS = 'launchTask'
KEY_LAUNCH_ICAFIX = 'launchICAFIX'
KEY_MAIL_HOST = 'mailhost'
KEY_USER_EMAIL = 'useremail'
KEY_USER_FULL_NAME = 'userfullname'
KEY_XNAT_SERVER = 'xnatserver'
KEY_ADMIN_EMAIL = 'adminemail'
KEY_DATA_TYPE = 'dataType'
KEY_PROJECT = 'project'
KEY_LABEL = 'label'

class MppParameters(object):

	def __init__(self):
		super(MppParameters, self).__init__()
		self._debug_MppParameters = debug_util.DebugUtil('MppParameters')
		self._debug_MppParameters.show_debug_msgs = True

		self._subject = ''
		self._session = ''
		self._xnat_session_id = ''
		self._project = ''
		
		self._launch_structural = False
		self._structural_parameters = None
		
		self._launch_functional = False
		self._functional_parameters = None
		
		self._launch_diffusion = False
		self._diffusion_parameters = None
		
		self._launch_task_analysis = False
		self._task_analysis_parameters = None
		
		self._launch_ica_fix = False
		self._ica_fix_parameters = None
		
		self._launch_mpp = False
		
		self._mail_host = 'mail.nrg.wustl.edu'				# Should be configurable instead of hard coded
		self._user_email = 'hilemanm@mir.wustl.edu'			# Should be configurable instead of hard coded
		self._user_full_name = 'MPPUser'					# Should be configurable instead of hard coded
		self._xnat_server = 'ConnectomeDB'					# Should be configurable instead of hard coded
		self._admin_email = 'db-admin@humanconnectome.org'	# Should be configurable instead of hard coded
		self._data_type = 'xnat:mrSessionData'              # Should be configurable instead of hard coded
		
# 	# property: debug
# 	
# 	def _get_debug(self):
# 		return self._debug_MppParameters.show_debug_msgs
# 	
# 	def _set_debug(self, new_tf_value):
# 		if new_tf_value != self._debug.show_debug_msgs:
# 			self._debug.show_debug_msgs = True
# 			self._debug.show("changing debug status to: " + str(new_tf_value))
# 			self._debug.show_debug_msgs = new_tf_value
# 			
# 	debug=property(_get_debug, _set_debug, doc="Indication of whether or not debugging output is to be produced")
	
	# property: subject
	
	def _get_subject(self):
		return self._subject
	
	def _set_subject(self, new_subject):
		self._subject = new_subject
		self._debug_MppParameters.show("subject: " + str(self._subject))
		
	subject=property(_get_subject, _set_subject)
	
	# property: session
	
	def _get_session(self):
		return self._session
	
	def _set_session(self, new_session):
		self._session = new_session
		self._debug_MppParameters.show("session: " + str(self._session))
		
	session=property(_get_session, _set_session)
	
	# property: xnat_session_id
	
	def _get_xnat_session_id(self):
		return self._xnat_session_id
	
	def _set_xnat_session_id(self, new_xnat_session_id):
		self._xnat_session_id = new_xnat_session_id
		self._debug_MppParameters.show("xnat_session_id: " + str(self._xnat_session_id))
		
	xnat_session_id=property(_get_xnat_session_id, _set_xnat_session_id)
	
	# property: project
	
	def _get_project(self):
		return self._project
	
	def _set_project(self, new_project):
		self._project = new_project
		self._debug_MppParameters.show("project: " + str(self._project))
		
	project=property(_get_project, _set_project)
	
	# property: launch_structural
	
	def _get_launch_structural(self):
		return self._launch_structural
	
	def _set_launch_structural(self, new_tf_value):
		self._launch_structural = new_tf_value
		self._debug_MppParameters.show("launch_structural: " + str(self._launch_structural))
		
	launch_structural=property(_get_launch_structural, _set_launch_structural)
	
	# property: structural_parameters - settable, but unreadable
	
	def _set_structural_parameters(self, new_structural_parameters):
		self._structural_parameters = new_structural_parameters
		
	structural_parameters=property(None, _set_structural_parameters)
	
	# property: launch_functional
	
	def _get_launch_functional(self):
		return self._launch_functional
	
	def _set_launch_functional(self, new_tf_value):
		self._launch_functional = new_tf_value
		self._debug_MppParameters.show("launch_functional: " + str(self._launch_functional))
		
	launch_functional=property(_get_launch_functional, _set_launch_functional)
	
	# property: functional_parameters - settable, but unreadable
	
	def _set_functional_parameters(self, new_functional_parameters):
		self._functional_parameters = new_functional_parameters
		
	functional_parameters=property(None, _set_functional_parameters)
	
	# property: launch_diffusion
	
	def _get_launch_diffusion(self):
		return self._launch_diffusion
	
	def _set_launch_diffusion(self, new_tf_value):
		self._launch_diffusion = new_tf_value
		self._debug_MppParameters.show("launch_diffusion: " + str(self._launch_diffusion))
		
	launch_diffusion=property(_get_launch_diffusion, _set_launch_diffusion)
	
	# property: diffusion_parameters - settable, but unreadable
	
	def _set_diffusion_parameters(self, new_diffusion_parameters):
		self._diffusion_parameters = new_diffusion_parameters
		
	diffusion_parameters=property(None, _set_diffusion_parameters)
	
	# property: launch_task_analysis
	
	def _get_launch_task_analysis(self):
		return self._launch_task_analysis
	
	def _set_launch_task_analysis(self, new_tf_value):
		self._launch_task_analysis = new_tf_value
		self._debug_MppParameters.show("launch_task_analysis: " + str(self._launch_task_analysis))
		
	launch_task_analysis=property(_get_launch_task_analysis, _set_launch_task_analysis)
	
	# property: task_analysis_parameters - settable, but unreadable
	
	def _set_task_analysis_parameters(self, new_task_analysis_parameters):
		self._task_analysis_parameters = new_task_analysis_parameters
		
	task_analysis_parameters=property(None, _set_task_analysis_parameters)
	
	# property: launch_ica_fix
	
	def _get_launch_ica_fix(self):
		return self._launch_ica_fix
	
	def _set_launch_ica_fix(self, new_tf_value):
		self._launch_ica_fix = new_tf_value
		self._debug_MppParameters.show("launch_ica_fix: " + str(self._launch_ica_fix))
		
	launch_ica_fix=property(_get_launch_ica_fix, _set_launch_ica_fix)
	
	# property: ica_fix_parameters - settable, but unreadable
	
	def _set_ica_fix_parameters(self, new_ica_fix_parameters):
		self._ica_fix_parameters = new_ica_fix_parameters
		
	ica_fix_parameters=property(None, _set_ica_fix_parameters)
	
	# property: launch_mpp
	
	def _get_launch_mpp(self):
		return self._launch_mpp
	
	def _set_launch_mpp(self, new_tf_value):
		self._launch_mpp = new_tf_value
		self._debug_MppParameters.show("launch_ica_fix: " + str(self._launch_mpp))
		
	launch_mpp=property(_get_launch_mpp, _set_launch_mpp)
	
	def _show_attributes(self, prefix):
		print(prefix + "subject: " + str(self.subject))
		print(prefix + "session: " + str(self.session))
		print(prefix + "xnat_session_id: " + str(self.xnat_session_id))
		print(prefix + "project: " + str(self.project))
		
		print(prefix + "launch_structural: " + str(self.launch_structural))
		if self.launch_structural:
			print(prefix + "structural_parameters: ")
			self._structural_parameters.show("\t" + prefix)
			
		print(prefix + "launch_functional: " + str(self.launch_functional))
		if self.launch_functional:
			print(prefix + "functional_parameters: ")
			self._functional_parameters.show("\t" + prefix)
			
		print(prefix + "launch_diffusion: " + str(self.launch_diffusion))
		if self.launch_diffusion:
			print(prefix + "diffusion_parameters: ")
			self._diffusion_parameters.show("\t" + prefix)
			
		print(prefix + "launch_task_analysis: " + str(self.launch_task_analysis))
		if self.launch_task_analysis:
			print(prefix + "task_analysis_parameters: ")
			self._task_analysis_parameters.show("\t" + prefix)
			
		print(prefix + "launch_ica_fix: " + str(self.launch_ica_fix))
		if self.launch_ica_fix:
			print(prefix + "ica_fix_parameters: ")
			self._ica_fix_parameters.show("\t" + prefix)
			
		print(prefix + "launch_mpp: " + str(self.launch_mpp))
		
	def show(self, prefix=""):
		print prefix + self.__class__.__name__
		self._show_attributes(prefix + "\t")
		
	def valid(self):
		if self.launch_structural and not self._structural_parameters.valid():
			print("MPP Parameter Validation: Structural Parameters do not pass validation checks")
			return False
		
		if self.launch_functional and not self._functional_parameters.valid():
			print("MPP Parameter Validation: Functional Parameters do not pass validation checks")
			return False
		
		if self.launch_diffusion and not self._diffusion_parameters.valid():
			print("MPP Parameter Validation: Diffusion Parameters do not pass validation checks")
			return False
		
		if self.launch_task_analysis and not self._task_analysis_parameters.valid():
			print("MPP Parameter Validation: Task Analysis Parameters do not pass validation checks")
			return False
		
		if self.launch_ica_fix and not self._ica_fix_parameters.valid():
			print("MPP Parameter Validation: ICA FIX Parameters do not pass validation checks")
			return False
		
		# All tests passed
		return True
	
	def add_xnat_pipeline_parameters(self, parameters):
		self._debug_MppParameters.show("add_xnat_pipeline_parameters: Start")

		# Parameters that apply without regard to which pipelines are being launched
		parameters.addUniqueParameter(KEY_XNAT_ID, self.xnat_session_id)
		parameters.addUniqueParameter(KEY_SUBJECT, self.subject)
		parameters.addUniqueParameter(KEY_SESSION_ID, self.session)
		
		parameters.addUniqueParameter(KEY_MAIL_HOST, self._mail_host)
		parameters.addUniqueParameter(KEY_USER_EMAIL, self._user_email)
		parameters.addUniqueParameter(KEY_USER_FULL_NAME, self._user_full_name)
		parameters.addUniqueParameter(KEY_XNAT_SERVER, self._xnat_server)        
		parameters.addUniqueParameter(KEY_ADMIN_EMAIL, self._admin_email)
		parameters.addUniqueParameter(KEY_DATA_TYPE, self._data_type)
		
		parameters.addUniqueParameter(KEY_PROJECT, self.project)
		parameters.addUniqueParameter(KEY_LABEL, self.session)
		
		# Structural Preprocessing parameters
		parameters.addUniqueParameter(KEY_LAUNCH_STRUCTURAL, int(self.launch_structural))
		if self.launch_structural:
			self._debug_MppParameters.show("Adding structural xnat pipeline parameters")
			self._structural_parameters.add_xnat_pipeline_parameters(parameters)
			
		# Functional Preprocessing parameters
		parameters.addUniqueParameter(KEY_LAUNCH_FUNCTIONAL, int(self.launch_functional))
		if self.launch_functional:
			self._debug_MppParameters.show("Adding functional xnat pipeline parameters")
			self._functional_parameters.add_xnat_pipeline_parameters(parameters)
			
		# Diffusion Preprocessing parameters
		parameters.addUniqueParameter(KEY_LAUNCH_DIFFUSION, int(self.launch_diffusion))
		if self.launch_diffusion:
			self._debug_MppParameters.show("Adding diffusion xnat pipeline parameters")
			self._diffusion_parameters.add_xnat_pipeline_parameters(parameters)
			
		# Task Analysis parameters
		parameters.addUniqueParameter(KEY_LAUNCH_TASK_ANALYSIS, int(self.launch_task_analysis))
		if self.launch_task_analysis:
			self._debug_MppParameters.show("Adding task analysis xnat pipeline parameters")
			self._task_analysis_parameters.add_xnat_pipeline_parameters(parameters)
			
		# ICA FIX parameters
		parameters.addUniqueParameter(KEY_LAUNCH_ICAFIX, int(self.launch_ica_fix))
		if self.launch_ica_fix:
			self._debug_MppParameters.show("Adding ica fix xnat pipeline parameters")
			self._ica_fix_parameters.add_xnat_pipeline_parameters(parameters)
			
		self._debug_MppParameters.show("add_xnat_pipeline_parameters: End")
		
	def save_to_xml_file(self, path_to_xml_file):
		self._debug_MppParameters.show("save_to_xml_file: Start")
		parameters = xnat_pipeline_parameters.XNATPipelineParameters()
		self.add_xnat_pipeline_parameters(parameters)
		parameters.saveParameters(path_to_xml_file)
		self._debug_MppParameters.show("save_to_xml_file: End")

