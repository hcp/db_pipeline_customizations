from functional_parameters import FunctionalParameters

# Constants

KEY_FUNCTIONAL_AP_FIELDMAPSERIES='functional_ap_fieldmapseries'
KEY_FUNCTIONAL_PA_FIELDMAPSERIES='functional_pa_fieldmapseries'
KEY_FUNCTIONAL_EPI_TO_T1_DEGREES_OF_FREEDOM='functional_epi2t1_dof'
KEY_FUNCTIONAL_FMRINAMES='functional_fmrinames'

class FunctionalParameters7T(FunctionalParameters):

	def __init__(self):
		super(FunctionalParameters7T, self).__init__()
		
		# attributes inherited from superclass, but set uniquely for this subclass
		self._TE = 'NONE'
		self._distortion_correction_method = 'TOPUP'
		
		# new attributes of this class
		self._AP_fieldmapseries = 'SpinEchoFieldMap_AP'
		self._PA_fieldmapseries = 'SpinEchoFieldMap_PA'
		self._epi2t1_degrees_of_freedom = '12'
		
	def add_xnat_pipeline_parameters(self, parameters):
		super(FunctionalParameters7T, self).add_xnat_pipeline_parameters(parameters)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_AP_FIELDMAPSERIES, self._AP_fieldmapseries)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_PA_FIELDMAPSERIES, self._PA_fieldmapseries)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_EPI_TO_T1_DEGREES_OF_FREEDOM, self._epi2t1_degrees_of_freedom)
		
		fmrinames_list = list()
		
		for scan_info in self._scan_info_list:
			scan_type = scan_info["scan_type"]
			scan_type_pieces = scan_type.split('_')
			fmriname = str()
			for i in range(len(scan_type_pieces)):
				if i == len(scan_type_pieces)-1:
					fmriname += '7T_'
					fmriname += scan_type_pieces[i]
				else:
					fmriname += scan_type_pieces[i]
					fmriname += '_'
			
			fmrinames_list.append(fmriname)
			
		parameters.addListParameters(KEY_FUNCTIONAL_FMRINAMES, fmrinames_list)
