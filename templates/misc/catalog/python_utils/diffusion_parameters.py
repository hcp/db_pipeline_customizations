import debug_util

# Constants

KEY_DIFFUSION_ECHO_SPACING='diffusion_EchoSpacing'
KEY_DIFFUSION_PHASE_ENCODING_DIR='diffusion_PhaseEncodingDir'
KEY_DIFFUSION_DIR_DICT_NAMES='diffusion_DiffusionDirDictNames'
KEY_DIFFUSION_DIR_DICT_VALUES='diffusion_DiffusionDirDictValues'
KEY_DIFFUSION_SCAN_DICT_NAMES='diffusion_DiffusionScanDictNames'
KEY_DIFFUSION_SCAN_DICT_VALUES='diffusion_DiffusionScanDictValues'
KEY_DIFFUSION_NEG_DATA='diffusion_negData'
KEY_DIFFUSION_POS_DATA='diffusion_posData'
KEY_DIFFUSION_DETAILED_OUTLIER_STATS='diffusion_detailed_outlier_stats'
KEY_DIFFUSION_REPLACE_OUTLIERS='diffusion_replace_outliers'

class DiffusionParameters(object):
	"""
	Class for containing and managing parameters for the diffusion preprocessing part of the MPP pipeline
	"""

	def __init__(self, debugOn=False):
		super(DiffusionParameters, self).__init__()
		self._debug_DiffusionParameters = debug_util.DebugUtil('DiffusionParameters')
		self._debug_DiffusionParameters.show_debug_msgs = debugOn
		
		self._session = ''
		self._neg_dir_count = 0
		self._pos_dir_count = 0
		self._dir_names = list()
		self._dir_values = list()
		self._scan_names = list()
		self._scan_values = list()
		self._echo_spacing_values = list()
		
		self._phase_encoding_dir = None # '1' ==> RL/LR; '2' ==> AP/PA; Must be set in subclass
		self._pos_suffix = None
		self._neg_suffix = None
		self._detailed_outlier_stats = None
		self._replace_outliers = None
		
 	# property session
 	
 	def _get_session(self):
 		return self._session
 	
 	def _set_session(self, new_session):
 		self._session = new_session
 	
 	session=property(_get_session, _set_session)
	
	# property neg_dir_count
	
	def _get_neg_dir_count(self):
		return self._neg_dir_count
	
	neg_dir_count=property(_get_neg_dir_count)
	
	# property pos_dir_count
	
	def _get_pos_dir_count(self):
		return self._pos_dir_count
	
	pos_dir_count=property(_get_pos_dir_count)
	
	# property dir_names
	
	def _get_dir_names(self):
		return self._dir_names
	
	dir_names=property(_get_dir_names)
	
	# property dir_values
	
	def _get_dir_values(self):
		return self._dir_values
	
	dir_values=property(_get_dir_values)
	
	# property scan_names
	
	def _get_scan_names(self):
		return self._scan_names
	
	scan_names=property(_get_scan_names)
	
	# property scan_values
	
	def _get_scan_values(self):
		return self._scan_values
	
	scan_values=property(_get_scan_values)
	
	# property echo_spacing_values
	
	def _get_echo_spacing_values(self):
		return self._echo_spacing_values
	
	echo_spacing_values=property(_get_echo_spacing_values)
	
	# property phase_encoding_dir
	
	def _get_phase_encoding_dir(self):
		return self._phase_encoding_dir
	
	phase_encoding_dir=property(_get_phase_encoding_dir)
	
	# derived property echo_spacing
	
	def _get_echo_spacing(self):
		echo_spacing = '%s' % (sum(self.echo_spacing_values) / float(len(self.echo_spacing_values)))
		return echo_spacing
	
	echo_spacing=property(_get_echo_spacing)
	
	# utility methods
	
	def valid(self):
		# No tests defined yet
		
		# All tests passed
		return True
	
	def _show_attributes(self, prefix):
		print(prefix + "session: " + str(self.session))
		print(prefix + "phase_encoding_dir: " + str(self.phase_encoding_dir))
		print(prefix + "pos_dir_count: " + str(self.pos_dir_count))
		print(prefix + "neg_dir_count: " + str(self.neg_dir_count))

		for i in range(len(self._dir_names)):
			print(prefix + str(i) + ": "\
				"dir_name: " + self._dir_names[i] + " " + \
				"dir_value: " + self._dir_values[i] + " " + \
				"scan_name: " + self._scan_names[i] + " " + \
				"scan_value: " + self._scan_values[i] + " " + \
				"echo_spacing: " + str(self._echo_spacing_values[i]))
	
	def show(self, prefix=""):
		print prefix + self.__class__.__name__
		self._show_attributes(prefix + "\t")
		
	def add_xnat_pipeline_parameters(self, parameters):
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: Start")
		
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: KEY: " + KEY_DIFFUSION_ECHO_SPACING)
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: VAL: " + str(self._echo_spacing_values[0]))
		parameters.addUniqueParameter(KEY_DIFFUSION_ECHO_SPACING, self._echo_spacing_values[0])
		
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: KEY: " + KEY_DIFFUSION_PHASE_ENCODING_DIR)
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: VAL: " + str(self._phase_encoding_dir))
		parameters.addUniqueParameter(KEY_DIFFUSION_PHASE_ENCODING_DIR, self._phase_encoding_dir)

		parameters.addListParameters(KEY_DIFFUSION_DIR_DICT_NAMES, self.dir_names)
		parameters.addListParameters(KEY_DIFFUSION_DIR_DICT_VALUES, self.dir_values)
		parameters.addListParameters(KEY_DIFFUSION_SCAN_DICT_NAMES, self.scan_names)
		parameters.addListParameters(KEY_DIFFUSION_SCAN_DICT_VALUES, self.scan_values)
		
		dir_names_dict = dict(zip(self.dir_names, self.dir_values))
		
		posData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_" + self._pos_suffix + ".nii.gz" for key in sorted(dir_names_dict) if key.startswith(self._pos_suffix)])
		negData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_" + self._neg_suffix + ".nii.gz" for key in sorted(dir_names_dict) if key.startswith(self._neg_suffix)])
		
		parameters.addUniqueParameter(KEY_DIFFUSION_NEG_DATA, negData)
		parameters.addUniqueParameter(KEY_DIFFUSION_POS_DATA, posData)
		
		parameters.addUniqueParameter(KEY_DIFFUSION_DETAILED_OUTLIER_STATS, self._detailed_outlier_stats)
		parameters.addUniqueParameter(KEY_DIFFUSION_REPLACE_OUTLIERS, self._replace_outliers)
		
		self._debug_DiffusionParameters.show("add_xnat_pipeline_parameters: End")

