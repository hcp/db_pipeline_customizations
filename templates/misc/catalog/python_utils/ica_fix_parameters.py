import debug_util

# Constants

KEY_ICAFIX_BANDPASS='icafix_bp'
KEY_ICAFIX_FUNCTSERIES='icafix_functseries'

class IcaFixParameters(object):
	"""
	Class for containing and managing parameters for the ICA FIX analysis processing part of the MPP pipeline
	"""
	
	def __init__(self, debugOn=False):
		super(IcaFixParameters, self).__init__()
		self._debug_IcaFixParameters = debug_util.DebugUtil("IcaFixParameters")
		self._debug_IcaFixParameters.show_debug_msgs = debugOn
		
		self._functseries_list = list()
		
		# HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
		self._bandpass = '2000'
		
	# property bandpass
	def _get_bandpass(self):
		return self._bandpass
	
	bandpass=property(_get_bandpass)
	
	# property functseries_list
	def _get_functseries_list(self):
		return self._functseries_list
	
	functseries_list=property(_get_functseries_list)
	
	def add_scan(self, scan_resource_name):
		self._functseries_list.append(scan_resource_name)
	
	# utility methods
	
	def valid(self):
		# No tests defined yet
		
		# All tests passed
		return True
	
	def _show_attributes(self, prefix):
		print(prefix + "bandpass: " + self.bandpass)
		print(prefix + "functseries: " + str(self.functseries_list))
		
	def show(self, prefix=""):
		print prefix + self.__class__.__name__
		self._show_attributes(prefix + "\t")
		
	def add_xnat_pipeline_parameters(self, parameters):
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: Start")
		
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: KEY: " + KEY_ICAFIX_BANDPASS)
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: VAL: " + str(self._bandpass))
		parameters.addUniqueParameter(KEY_ICAFIX_BANDPASS, self._bandpass)
		
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: KEY: " + KEY_ICAFIX_FUNCTSERIES)
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: VAL: " + str(self._functseries_list))
		parameters.addListParameters(KEY_ICAFIX_FUNCTSERIES, self._functseries_list)
		
		self._debug_IcaFixParameters.show("add_xnat_pipeline_parameters: End")
