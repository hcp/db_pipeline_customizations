import debug_util
from mpp_parameters import MppParameters

# Constants

KEY_STRUCTURAL_REFERENCE_PROJECT = 'structuralReferenceProject'
KEY_STRUCTURAL_REFERENCE_SESSION = 'structuralReferenceSession'

class MppParameters7T(MppParameters):

	def __init__(self):
		super(MppParameters7T, self).__init__()
		self._debug_MppParameters7T = debug_util.DebugUtil('MppParameters7T')
		self._debug_MppParameters7T.show_debug_msgs = True
		
		# new attributes of this class
		self._structural_reference_project = ''
		self._structural_reference_session = ''
		
	# property structural_reference_project
	
	def _get_structural_reference_project(self):
		return self._structural_reference_project
	
	def _set_structural_reference_project(self, new_structural_reference_project):
		self._structural_reference_project = new_structural_reference_project
		self._debug_MppParameters7T.show("structural_reference_project: " + str(self._structural_reference_project))
		
	structural_reference_project=property(_get_structural_reference_project, _set_structural_reference_project)
	
	# property structural_reference_session
	
	def _get_structural_reference_session(self):
		return self._structural_reference_session
	
	def _set_structural_reference_session(self, new_structural_reference_session):
		self._structural_reference_session = new_structural_reference_session
		self._debug_MppParameters7T.show("structural_reference_session: " + str(self._structural_reference_session))
		
	structural_reference_session=property(_get_structural_reference_session, _set_structural_reference_session)
	
	def add_xnat_pipeline_parameters(self, parameters):
		self._debug_MppParameters7T.show("add_xnat_pipeline_parameters: Start")
		super(MppParameters7T, self).add_xnat_pipeline_parameters(parameters)
		parameters.addUniqueParameter(KEY_STRUCTURAL_REFERENCE_PROJECT, self._structural_reference_project)
		parameters.addUniqueParameter(KEY_STRUCTURAL_REFERENCE_SESSION, self._structural_reference_session)
		self._debug_MppParameters7T.show("add_xnat_pipeline_parameters: End")
		
	def _show_attributes(self, prefix):
		super(MppParameters7T, self)._show_attributes(prefix)
		print(prefix + "structural_reference_project: " + str(self._structural_reference_project))
		print(prefix + "structural_reference_session: " + str(self._structural_reference_session))

