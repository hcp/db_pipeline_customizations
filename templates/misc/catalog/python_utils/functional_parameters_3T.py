from functional_parameters import FunctionalParameters

# Constants

KEY_FUNCTIONAL_LR_FIELDMAPSERIES='functional_lr_fieldmapseries'
KEY_FUNCTIONAL_RL_FIELDMAPSERIES='functional_rl_fieldmapseries'

class FunctionalParameters3T(FunctionalParameters):
	
	def __init__(self):
		super(FunctionalParameters3T, self).__init__()
		
		# attributes inherited from superclass, but set uniquely for this subclass
		self._TE = '2.46'
		self._distortion_correction_method = 'TOPUP'
		
		# new attributes of this class
		self._LR_fieldmapseries = 'SpinEchoFieldMap_LR'
		self._RL_fieldmapseries = 'SpinEchoFieldMap_RL'
		
	def add_xnat_pipeline_parameters(self, parameters):
		super(FunctionalParameters3T, self).add_xnat_pipeline_parameters(parameters)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_LR_FIELDMAPSERIES, self._LR_fieldmapseries)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_RL_FIELDMAPSERIES, self._RL_fieldmapseries)
