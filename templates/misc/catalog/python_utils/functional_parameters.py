
# Constants

KEY_FUNCTIONAL_TE='functional_TE'
KEY_FUNCTIONAL_DISTORTION_CORRECTION='functional_DistortionCorrection'

KEY_FUNCTIONAL_SCANID='functional_scanid'
KEY_FUNCTIONAL_FUNCTIONALSERIES='functional_functionalseries'
KEY_FUNCTIONAL_DWELLTIMES='functional_DwellTimes'
KEY_FUNCTIONAL_UNWARP_DIR='functional_UnwarpDir'

class FunctionalParameters(object):
	"""
	Class for containing and managing parameters for the functional preprocessing part of the MPP pipeline
	"""
	
	def __init__(self):
		super(FunctionalParameters, self).__init__()
		self._TE = None
		self._distortion_correction_method = None
		
		self._scan_info_list = list()

	def add_scan_info(self, scan_number, scan_type, dwell_time, phase_encoding_direction):
		scan_info_dict = dict()
		scan_info_dict["scan_number"] = scan_number
		scan_info_dict["scan_type"] = scan_type
		scan_info_dict["dwell_time"] = dwell_time
		scan_info_dict["phase_encoding_direction"] = phase_encoding_direction
		self._scan_info_list.append(scan_info_dict)
		
	def _show_attributes(self, prefix):
		for scan_info in self._scan_info_list:
			print(prefix + "scan_number: " + str(scan_info["scan_number"]))
			print(prefix + "\tscan_type: " + str(scan_info["scan_type"]))
			print(prefix + "\tdwell_time: " + str(scan_info["dwell_time"]))
			print(prefix + "\tphase_encoding_direction: " + str(scan_info["phase_encoding_direction"]))
			
	def show(self, prefix=""):
		print prefix + self.__class__.__name__
		self._show_attributes(prefix + "\t")
		
	def valid(self):
		# No validity tests defined yet
		
		# All tests passed
		return True
	
	def add_xnat_pipeline_parameters(self, parameters):
		parameters.addUniqueParameter(KEY_FUNCTIONAL_TE, self._TE)
		parameters.addUniqueParameter(KEY_FUNCTIONAL_DISTORTION_CORRECTION, self._distortion_correction_method)
		
		# strikes me that there is probably a more "Pythonic" way to build these lists from the list of dict objects
		scan_id_list = list()
		scan_type_list = list()
		scan_dwell_time_list = list()
		scan_phase_encoding_direction_list = list()
		
		for scan_info in self._scan_info_list:
			scan_id_list.append(scan_info["scan_number"])
			scan_type_list.append(scan_info["scan_type"])
			scan_dwell_time_list.append(scan_info["dwell_time"])
			scan_phase_encoding_direction_list.append(scan_info["phase_encoding_direction"])
			
		parameters.addListParameters(KEY_FUNCTIONAL_SCANID, scan_id_list)
		parameters.addListParameters(KEY_FUNCTIONAL_FUNCTIONALSERIES, scan_type_list)
		parameters.addListParameters(KEY_FUNCTIONAL_DWELLTIMES, scan_dwell_time_list)
		parameters.addListParameters(KEY_FUNCTIONAL_UNWARP_DIR, scan_phase_encoding_direction_list)

