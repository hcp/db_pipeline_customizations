
def check_equal(v1, v2, fail_message):
    if (v1 == v2):
        return True
    else:
        print fail_message 
        print str(v1) + " NOT == " + str(v2) 
        return False

def check_gte(v1, v2, fail_message):
    if (v1 >= v2):
        return True
    else:
        print fail_message
        print str(v1) + " NOT >= " + str(v2)
        return False

