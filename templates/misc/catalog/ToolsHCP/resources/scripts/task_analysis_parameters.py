

class SingleRunTaskAnalysisParameters(object):
    def __init__(self, init_lowresmesh, init_grayordinates, init_origsmoothingFWHM, init_finalsmoothingFWHM, init_temporalfilter, init_confound, init_vba):
        super(SingleRunTaskAnalysisParameters, self).__init__()
        self._lowresmesh = init_lowresmesh
        self._grayordinates = init_grayordinates
        self._origsmoothingFWHM = init_origsmoothingFWHM
        self._finalsmoothingFWHM = init_finalsmoothingFWHM
        self._temporalfilter = init_temporalfilter
        self._confound = init_confound
        self._vba = init_vba

    # property lowresmesh

    def _get_lowresmesh(self):
        return self._lowresmesh

    def _set_lowresmesh(self, new_lowresmesh):
        self._lowresmesh = new_lowresmesh

    lowresmesh=property(_get_lowresmesh, _set_lowresmesh)

    # property grayordinates

    def _get_grayordinates(self):
        return self._grayordinates

    def _set_grayordinates(self, new_grayordinates):
        self._grayordinates = new_grayordinates

    grayordinates=property(_get_grayordinates, _set_grayordinates)

    # property origsmoothingFWHM

    def _get_origsmoothingFWHM(self):
        return self._origsmoothingFWHM

    def _set_origsmoothingFWHM(self, new_origsmoothingFWHM):
        self._origsmoothingFWHM = new_origsmoothingFWHM

    origsmoothingFWHM=property(_get_origsmoothingFWHM, _set_origsmoothingFWHM)

    # property finalsmoothingFWHM

    def _get_finalsmoothingFWHM(self):
        return self._finalsmoothingFWHM

    def _set_finalsmoothingFWHM(self, new_finalsmoothingFWHM):
        self._finalsmoothingFWHM = new_finalsmoothingFWHM

    finalsmoothingFWHM=property(_get_finalsmoothingFWHM, _set_finalsmoothingFWHM)

    # property temporalfilter

    def _get_temporalfilter(self):
        return self._temporalfilter

    def _set_temporalfilter(self, new_temporalfilter):
        self._temporalfilter = new_temporalfilter

    temporalfilter=property(_get_temporalfilter, _set_temporalfilter)

    # property confound

    def _get_confound(self):
        return self._confound

    def _set_confound(self, new_confound):
        self._confound = new_confound

    confound=property(_get_confound, _set_confound)

    # property vba

    def _get_vba(self):
        return self._vba

    def _set_vba(self, new_vba):
        self._vba = new_vba

    vba=property(_get_vba, _set_vba)

    def show_all(self, prefix):
        print(prefix + "lowresmesh: " + self.lowresmesh)
        print(prefix + "grayordinates: " + self.grayordinates)
        print(prefix + "origsmoothingFWHM: " + self._origsmoothingFWHM)
        print(prefix + "finalsmoothingFWHM: " + self._finalsmoothingFWHM)
        print(prefix + "temporalfilter: " + self._temporalfilter)
        print(prefix + "confound: " + self._confound)
        print(prefix + "vba: " + self._vba)


class TaskAnalysisParameters(object):
    """
    Class for containing and managing parameters for the task analysis processing part of the MPP pipeline
    """

    def __init__(self):
        super(TaskAnalysisParameters, self).__init__()
        self._functional_roots = list()
        self._run_parameters_list = list()

        # HARDCODED PARAMETERS THAT SHOULD NOT BE HARDCODED
        self._run_parameters_list.append(SingleRunTaskAnalysisParameters('32', '2', '2', '2', '200', 'NONE', 'NO'))
        self._run_parameters_list.append(SingleRunTaskAnalysisParameters('32', '2', '2', '4', '200', 'NONE', 'YES'))
        self._run_parameters_list.append(SingleRunTaskAnalysisParameters('32', '2', '2', '8', '200', 'NONE', 'NO'))
        self._run_parameters_list.append(SingleRunTaskAnalysisParameters('32', '2', '2', '12', '200', 'NONE', 'NO'))

    # property run_parameters_list
    def _get_run_parameters_list(self):
        return self._run_parameters_list

    run_parameters_list=property(_get_run_parameters_list)

    # property functional_roots

    def _get_functional_roots(self):
        return self._functional_roots

    functional_roots=property(_get_functional_roots)

    def add_functional_root(self, new_functional_root):
        if new_functional_root not in self._functional_roots:
            self._functional_roots.append(new_functional_root)

    def _get_series_root(self, resource_name):
        parts = resource_name.split('_')
        series_root = parts[0] + '_' + parts[1]
        return series_root

    def add_scan(self, scan_resource_name):
        series_root = self._get_series_root(scan_resource_name)
        self.add_functional_root(series_root)

    def show_all(self, prefix):
        print(prefix + "functional_roots: " + str(self.functional_roots))

        run_count = 1
        for single_run_params in self._run_parameters_list:
            print(prefix + "Run " + str(run_count))
            single_run_params.show_all(prefix + "\t")
            run_count += 1

    def validate(self):
        # No tests defined yet

        # All tests passed
        return True
