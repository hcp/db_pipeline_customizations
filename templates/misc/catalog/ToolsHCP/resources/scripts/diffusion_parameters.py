
class DiffusionParameters(object):
    """
    Class for containing and managing parameters for the diffusion preprocessing part of the MPP pipeline
    """

    def __init__(self):
        super(DiffusionParameters, self).__init__()
        self._session = ''
        self._lr_count = 0
        self._rl_count = 0
        self._dir_names = list()
        self._dir_values = list()
        self._scan_names = list()
        self._scan_values = list()
        self._echo_spacing_values = list()

        # HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
        self._phase_encoding_dir = '1'  # '1' ==> RL/LR; '2' ==> AP/PA

    # property session
        
    def _get_session(self):
        return self._session

    def _set_session(self, new_session):
        self._session = new_session

    session=property(_get_session, _set_session)

    # property lr_count
        
    def _get_lr_count(self):
        return self._lr_count

    lr_count=property(_get_lr_count)

    # property rl_count
    
    def _get_rl_count(self):
        return self._rl_count
    
    rl_count=property(_get_rl_count)

    # property dir_names
    
    def _get_dir_names(self):
        return self._dir_names

    dir_names=property(_get_dir_names)

    # property dir_values

    def _get_dir_values(self):
        return self._dir_values

    dir_values=property(_get_dir_values)

    # property scan_names
    
    def _get_scan_names(self):
        return self._scan_names

    scan_names=property(_get_scan_names)

    # property scan_values

    def _get_scan_values(self):
        return self._scan_values

    scan_values=property(_get_scan_values)

    # property echo_spacing_values
    
    def _get_echo_spacing_values(self):
        return self._echo_spacing_values

    echo_spacing_values=property(_get_echo_spacing_values)

    # property phase_encoding_dir
    
    def _get_phase_encoding_dir(self):
        return self._phase_encoding_dir
    
    phase_encoding_dir=property(_get_phase_encoding_dir)

    # derived property echo_spacing

    def _get_echo_spacing(self):
        echo_spacing = '%s' % (sum(self.echo_spacing_values) / float(len(self.echo_spacing_values)))
        return echo_spacing

    echo_spacing=property(_get_echo_spacing)

    # utility methods

    def show_all(self, prefix):
        print(prefix + "lr_count: " + str(self.lr_count))
        print(prefix + "rl_count: " + str(self.rl_count))
        print(prefix + "dir_names: " + str(self.dir_names))
        print(prefix + "dir_values: " + str(self.dir_values))
        print(prefix + "scan_names: " + str(self.scan_names))
        print(prefix + "scan_values: " + str(self.scan_values))
        print(prefix + "echo_spacing_values: " + str(self.echo_spacing_values))
        print(prefix + "phase_encoding_dir: " + str(self.phase_encoding_dir))

    def add_scan(self, scan_number, direction, scan_dir_number, echo_spacing):
        if direction == "LR":
            self._lr_count = self._lr_count + 1
            self._dir_names.append("LR_Dir" + str(self._lr_count))
            self._dir_values.append(str(scan_dir_number))
            self._scan_names.append("LR_" + str(self._lr_count) + "ScanId")
            self._scan_values.append(str(scan_number))
            self._echo_spacing_values.append(echo_spacing)

        elif direction == "RL":
            self._rl_count = self._rl_count + 1
            self._dir_names.append("RL_Dir" + str(self._rl_count))
            self._dir_values.append(str(scan_dir_number))
            self._scan_names.append("RL_" + str(self._rl_count) + "ScanId")
            self._scan_values.append(str(scan_number))
            self._echo_spacing_values.append(echo_spacing)

        else:
            print("DiffusionParameters: ERROR: Unrecognized direction: " + direction)
            sys.exit()

    def validate(self):
        # No tests defined yet

        # All tests passed
        return True

    def add_parameters(self, Parameters):
        Parameters.addUniqueParameter("diffusion_EchoSpacing", self.echo_spacing)
        Parameters.addUniqueParameter("diffusion_PhaseEncodingDir", self.phase_encoding_dir)
        Parameters.addListParameters("diffusion_DiffusionDirDictNames", self.dir_names)
        Parameters.addListParameters("diffusion_DiffusionDirDictValues", self.dir_values)
        Parameters.addListParameters("diffusion_DiffusionScanDictNames", self.scan_names)
        Parameters.addListParameters("diffusion_DiffusionScanDictValues", self.scan_values)

        dir_names_dict = dict(zip(self.dir_names, self.dir_values))
        posData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_RL.nii.gz" for key in sorted(dir_names_dict) if key.startswith('RL')])
        negData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_LR.nii.gz" for key in sorted(dir_names_dict) if key.startswith('LR')])

        Parameters.addUniqueParameter('diffusion_negData', negData)
        Parameters.addUniqueParameter('diffusion_posData', posData)
