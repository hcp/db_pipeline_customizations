
class IcaFixParameters_UMINN_Prisma_3T(object):
    """
    Class for containing and managing parameters for the ICA FIX analysis processing part of the MPP pipeline
    """

    def __init__(self, phase_encoding_spec):
        super(IcaFixParameters_UMINN_Prisma_3T, self).__init__()
        self._functseries_list = list()

        # HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
        self._bandpass = '2000'

    # property bandpass
    def _get_bandpass(self):
        return self._bandpass

    bandpass=property(_get_bandpass)

    # property functseries_list
    def _get_functseries_list(self):
        return self._functseries_list

    functseries_list=property(_get_functseries_list)

    def show_all(self, prefix):
        print(prefix + "bandpass: " + self.bandpass)
        print(prefix + "functseries: " + str(self.functseries_list))

    def add_scan(self, scan_resource_name):
        self._functseries_list.append(scan_resource_name)

    def validate(self):
        # No tests defined yet

        # All tests passed
        return True
