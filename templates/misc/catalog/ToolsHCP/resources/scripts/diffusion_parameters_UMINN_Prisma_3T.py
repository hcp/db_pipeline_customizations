
class DiffusionParameters_UMINN_Prisma_3T(object):
	"""
	Class for containing and managing parameters for the diffusion preprocessing part of the MPP pipeline
	"""

	def __init__(self, phase_encoding_dir):
		super(DiffusionParameters_UMINN_Prisma_3T, self).__init__()
		self._session = ''
		self._negative_count = 0
		self._positive_count = 0
		self._dir_names = list()
		self._dir_values = list()
		self._scan_names = list()
		self._scan_values = list()
		self._echo_spacing_values = list()
		self._phase_encoding_dir = phase_encoding_dir # '1' ==> RL/LR; '2' ==> AP/PA
		self._gradient_distortion_coeffs_file = "UNSET"

		# HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
		# self._phase_encoding_dir = '2'  # '1' ==> RL/LR; '2' ==> AP/PA
		self._detailed_outlier_stats = True
		self._replace_outliers = False

	# property gradient_distortion_coeffs_file
	def _get_gradient_distortion_coeffs_file(self):
		return self._gradient_distortion_coeffs_file

	def _set_gradient_distortion_coeffs_file(self, new_gradient_distortion_coeffs_file):
		self._gradient_distortion_coeffs_file = new_gradient_distortion_coeffs_file
	
	gradient_distortion_coeffs_file=property(_get_gradient_distortion_coeffs_file, _set_gradient_distortion_coeffs_file)

	# property session
		
	def _get_session(self):
		return self._session

	def _set_session(self, new_session):
		self._session = new_session

	session=property(_get_session, _set_session)

	# property negative_count
		
	def _get_negative_count(self):
		return self._negative_count

	negative_count=property(_get_negative_count)

	# property positive_count
	
	def _get_positive_count(self):
		return self._positive_count
	
	positive_count=property(_get_positive_count)

	# property dir_names
	
	def _get_dir_names(self):
		return self._dir_names

	dir_names=property(_get_dir_names)

	# property dir_values

	def _get_dir_values(self):
		return self._dir_values

	dir_values=property(_get_dir_values)

	# property scan_names
	
	def _get_scan_names(self):
		return self._scan_names

	scan_names=property(_get_scan_names)

	# property scan_values

	def _get_scan_values(self):
		return self._scan_values

	scan_values=property(_get_scan_values)

	# property echo_spacing_values
	
	def _get_echo_spacing_values(self):
		return self._echo_spacing_values

	echo_spacing_values=property(_get_echo_spacing_values)

	# property phase_encoding_dir
	
	def _get_phase_encoding_dir(self):
		return self._phase_encoding_dir
	
	phase_encoding_dir=property(_get_phase_encoding_dir)

	# derived property echo_spacing

	def _get_echo_spacing(self):
		echo_spacing = '%s' % (sum(self.echo_spacing_values) / float(len(self.echo_spacing_values)))
		return echo_spacing

	echo_spacing=property(_get_echo_spacing)

	# property detailed_outlier_stats

	def _get_detailed_outlier_stats(self):
		return self._detailed_outlier_stats

	detailed_outlier_stats=property(_get_detailed_outlier_stats)
	
	# property replace_outliers
	
	def _get_replace_outliers(self):
		return self._replace_outliers
	
	replace_outliers=property(_get_replace_outliers)

	# utility methods

	def show_all(self, prefix):
		print(prefix + "negative_count: " + str(self.negative_count))
		print(prefix + "positive_count: " + str(self.positive_count))
		print(prefix + "dir_names: " + str(self.dir_names))
		print(prefix + "dir_values: " + str(self.dir_values))
		print(prefix + "scan_names: " + str(self.scan_names))
		print(prefix + "scan_values: " + str(self.scan_values))
		print(prefix + "echo_spacing_values: " + str(self.echo_spacing_values))
		print(prefix + "phase_encoding_dir: " + str(self.phase_encoding_dir))

	def add_scan(self, scan_number, direction, scan_dir_number, echo_spacing):
		if direction == "AP":
			self._negative_count = self._negative_count + 1
			self._dir_names.append("NEG_Dir" + str(self._negative_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("NEG_" + str(self._negative_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)

		elif direction == "PA":
			self._positive_count = self._positive_count + 1
			self._dir_names.append("POS_Dir" + str(self._positive_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("POS_" + str(self._positive_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)

		elif direction == "LR":
			self._negative_count = self._negative_count + 1
			self._dir_names.append("NEG_Dir" + str(self._negative_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("NEG_" + str(self._negative_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)

		elif direction == "RL":
			self._positive_count = self._positive_count + 1
			self._dir_names.append("POS_Dir" + str(self._positive_count))
			self._dir_values.append(str(scan_dir_number))
			self._scan_names.append("POS_" + str(self._positive_count) + "ScanId")
			self._scan_values.append(str(scan_number))
			self._echo_spacing_values.append(echo_spacing)

		else:
			print("DiffusionParameters: ERROR: Unrecognized direction: " + direction)
			sys.exit()

	def validate(self):
		# No tests defined yet

		# All tests passed
		return True

	def add_parameters(self, Parameters):
		Parameters.addUniqueParameter("diffusion_EchoSpacing", self.echo_spacing)
		Parameters.addUniqueParameter("diffusion_PhaseEncodingDir", self.phase_encoding_dir)
		Parameters.addListParameters("diffusion_DiffusionDirDictNames", self.dir_names)
		Parameters.addListParameters("diffusion_DiffusionDirDictValues", self.dir_values)
		Parameters.addListParameters("diffusion_DiffusionScanDictNames", self.scan_names)
		Parameters.addListParameters("diffusion_DiffusionScanDictValues", self.scan_values)
		
		dir_names_dict = dict(zip(self.dir_names, self.dir_values))
		
		if self.phase_encoding_dir == '1':
			posData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_RL.nii.gz" for key in sorted(dir_names_dict) if key.startswith('POS')])
			negData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_LR.nii.gz" for key in sorted(dir_names_dict) if key.startswith('NEG')])
		elif self.phase_encoding_dir == '2':
			posData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_PA.nii.gz" for key in sorted(dir_names_dict) if key.startswith('POS')])
			negData="@".join(["DiffData/" + self.session + "_DWI_dir" + dir_names_dict[key] + "_AP.nii.gz" for key in sorted(dir_names_dict) if key.startswith('NEG')])

		Parameters.addUniqueParameter('diffusion_negData', negData)
		Parameters.addUniqueParameter('diffusion_posData', posData)
		Parameters.addUniqueParameter("diffusion_detailed_outlier_stats", str(self.detailed_outlier_stats))
		Parameters.addUniqueParameter("diffusion_replace_outliers", str(self.replace_outliers))
		Parameters.addUniqueParameter("diffusion_gradient_distortion_coeffs_file", self.gradient_distortion_coeffs_file)

