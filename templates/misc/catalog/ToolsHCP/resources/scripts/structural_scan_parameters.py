class StructuralScanParameters(object):
    """
    Class for containing and managing parameters for a structural scan
    """
    def __init__(self):
        super(StructuralScanParameters, self).__init__()
        self.file_name = ''
        self.scan_number = ''
        self.series_desc = ''
        self.sample_spacing = ''

        self.magfieldmap_file_name = ''
        self.magfieldmap_scan_number = ''
        self.magfieldmap_delta_te = ''

        self.phasefieldmap_file_name = ''
        self.phasefieldmap_scan_number = ''

        # Positive SpinEcho Field Map
        self.pos_se_fieldmap_file_name = '' 
        self.pos_se_fieldmap_scan_number = ''
        self.pos_se_fieldmap_dwell_time = ''
        
        # Negative SpinEcho Field Map
        self.neg_se_fieldmap_file_name = ''
        self.neg_se_fieldmap_scan_number = ''
        self.neg_se_fieldmap_dwell_time = ''

    def show_all(self, prefix):
        print(prefix + "file_name: " + self.file_name)
        print(prefix + "scan_number: " + self.scan_number)
        print(prefix + "series_desc: " + self.series_desc)
        print(prefix + "sample_spacing: " + self.sample_spacing)
        print(prefix + "magfieldmap_file_name: " + self.magfieldmap_file_name)
        print(prefix + "magfieldmap_scan_number: " + self.magfieldmap_scan_number)
        print(prefix + "magfieldmap_delta_te: " + self.magfieldmap_delta_te)
        print(prefix + "phasefieldmap_file_name: " + self.phasefieldmap_file_name)
        print(prefix + "phasefieldmap_scan_number: " + self.phasefieldmap_scan_number)
        print(prefix + "pos_se_fieldmap_file_name: " + self.pos_se_fieldmap_file_name)
        print(prefix + "pos_se_fieldmap_scan_number: " + self.pos_se_fieldmap_scan_number)
        print(prefix + "pos_se_fieldmap_dwell_time: " + str(self.pos_se_fieldmap_dwell_time))
        print(prefix + "neg_se_fieldmap_file_name: " + self.neg_se_fieldmap_file_name)
        print(prefix + "neg_se_fieldmap_scan_number: " + self.neg_se_fieldmap_scan_number)
        print(prefix + "neg_se_fieldmap_dwell_time: " + str(self.neg_se_fieldmap_dwell_time))
        
