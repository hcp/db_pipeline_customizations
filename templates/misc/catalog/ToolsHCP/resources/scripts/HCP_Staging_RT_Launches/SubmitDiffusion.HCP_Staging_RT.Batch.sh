#!/bin/bash

if [ -z "${SUBJECT_FILES_DIR}" ] ; then
    echo "Environment variable SUBJECT_FILES_DIR must be set!"
    exit 1
fi

printf "Connectome DB Username: "
read userid

stty -echo
printf "Connectome DB Password: "
read password
echo ""
stty echo

project="HCP_Staging_RT"
subject_file_name="${SUBJECT_FILES_DIR}/${project}.diffusion.subjects"
echo "Retrieving subject list from: ${subject_file_name}"
subject_list_from_file=( $( cat ${subject_file_name} ) )
subjects="`echo "${subject_list_from_file[@]}"`"

for subject in ${subjects} ; do
    echo ""
    echo "--------------------------------------------------------------------------------"
    echo "Launching Diffusion Preprocessing for subject: ${subject} in project: ${project}"
    echo "--------------------------------------------------------------------------------"
    echo ""

    python ../launchHCP.py \
	-User "${userid}" \
	-Password "${password}" \
	-Server db.humanconnectome.org \
	-Project "${project}" \
	-Subjects "${subject}" \
	-LaunchDiffusion 1 \
	-LaunchFunctional 0 \
	-LaunchStructural 0 \
	-LaunchMPP 1 \
	-Launch 1 \
	-Compute CHPC \
	-LaunchTaskAnalysis 0 \
	-LaunchICAAnalysis 0 \
	-Shadow 1,2,3,4,5,7,8

done