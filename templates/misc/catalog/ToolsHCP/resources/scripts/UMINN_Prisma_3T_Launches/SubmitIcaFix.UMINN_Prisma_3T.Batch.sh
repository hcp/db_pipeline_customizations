#!/bin/bash

if [ -z "${SUBJECT_FILES_DIR}" ]
then
	echo "Environment variable SUBJECT_FILES_DIR must be set!"
	exit 1
fi

printf "Connectome DB Username: "
read userid

stty -echo
printf "Connectome DB Password: "
read password
echo ""
stty echo

project="WU_L37_Staging"
pipeline_project="UMINN_Prisma_3T"
subject_file_name="${SUBJECT_FILES_DIR}/${pipeline_project}.icafix.subjects"
echo "Retrieving subject list from: ${subject_file_name}"
subject_list_from_file=( $( cat ${subject_file_name} ) )
subjects="`echo "${subject_list_from_file[@]}"`"

start_shadow_number=1
max_shadow_number=8

shadow_number=${start_shadow_number}

for subject in ${subjects}
do
	echo ""
	echo "--------------------------------------------------------------------------------"
	echo " Launching ${pipeline_project} ICA FIX Processing for subject: ${subject}"
	echo " in project: ${project} on shadow number: ${shadow_number}"
	echo "--------------------------------------------------------------------------------"
	echo ""

	python ../launchMPP_UMINN_Prisma_3T.py \
		--debug \
		--server=db.humanconnectome.org \
		--username=${userid} \
		--password=${password} \
		--project=${project} \
		--pipeline-project=${pipeline_project} \
		--subject=${subject} \
		--sessionsuffix=_3T \
		--icafix \
		--shadow-number=${shadow_number} \
		--go

	shadow_number=$((shadow_number+1))

	if [ "$shadow_number" -gt "$max_shadow_number" ]
	then
		shadow_number=${start_shadow_number}
	fi

done
