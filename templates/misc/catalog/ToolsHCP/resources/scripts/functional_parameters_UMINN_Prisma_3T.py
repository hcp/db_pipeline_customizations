
class FunctionalParameters_UMINN_Prisma_3T(object):
	"""
	Class for containing and managing parameters for the functional preprocessing part of the MPP pipeline
	"""

	def __init__(self, phase_encoding_spec):
		super(FunctionalParameters_UMINN_Prisma_3T, self).__init__()
		self._scan_id_list = list()
		self._scan_type_list = list()
		self._scan_dwell_time_list = list()
		self._scan_phase_encoding_direction_list = list()

		if phase_encoding_spec == '1':
			self._NEG_fieldmapseries = 'SpinEchoFieldMap_LR'
			self._POS_fieldmapseries = 'SpinEchoFieldMap_RL'
		elif phase_encoding_spec == '2':
			self._NEG_fieldmapseries = 'SpinEchoFieldMap_AP'
			self._POS_fieldmapseries = 'SpinEchoFieldMap_PA'
		else:
			print("ERROR: Unrecognized phase_encoding_spec: " + phase_encoding_spec)
			sys.exit(1)

		self._gradient_distortion_coeffs_file = "UNSET"

		# HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
		self._TE = '2.46'
		self._distortion_correction_method = 'TOPUP'

	# property gradient_distortion_coeffs_file
	def _get_gradient_distortion_coeffs_file(self):
		return self._gradient_distortion_coeffs_file

	def _set_gradient_distortion_coeffs_file(self, new_gradient_distortion_coeffs_file):
		self._gradient_distortion_coeffs_file = new_gradient_distortion_coeffs_file
	
	gradient_distortion_coeffs_file=property(_get_gradient_distortion_coeffs_file, _set_gradient_distortion_coeffs_file)

	# property scan_id_list

	def _get_scan_id_list(self):
		return self._scan_id_list

	scan_id_list=property(_get_scan_id_list)

	# property scan_type_list
	
	def _get_scan_type_list(self):
		return self._scan_type_list

	scan_type_list=property(_get_scan_type_list)

	# property scan_dwell_time_list
	
	def _get_scan_dwell_time_list(self):
		return self._scan_dwell_time_list

	scan_dwell_time_list=property(_get_scan_dwell_time_list)

	# property scan_phase_encoding_direction_list

	def _get_scan_phase_encoding_direction_list(self):
		return self._scan_phase_encoding_direction_list

	scan_phase_encoding_direction_list=property(_get_scan_phase_encoding_direction_list)

	# property TE

	#def _set_TE(self, new_TE):
	#	self._TE = new_TE

	def _get_TE(self):
		return self._TE

	#TE=property(_get_TE, _set_TE)
	TE=property(_get_TE)

	# property distortion_correction_method

	#def _set_distortion_correction_method(self, new_distortion_correction_method):
	#	self._distortion_correction_method = new_distortion_correction_method

	def _get_distortion_correction_method(self):
		return self._distortion_correction_method

	#distortion_correction_method=property(_get_distortion_correction_method, _set_distortion_correction_method)
	distortion_correction_method=property(_get_distortion_correction_method)

	# property NEG_fieldmapseries

	def _get_NEG_fieldmapseries(self):
		return self._NEG_fieldmapseries

	NEG_fieldmapseries=property(_get_NEG_fieldmapseries)

	# property POS_fieldmapseries

	def _get_POS_fieldmapseries(self):
		return self._POS_fieldmapseries

	POS_fieldmapseries=property(_get_POS_fieldmapseries)

	def show_all(self, prefix):
		print(prefix + "TE: " + self.TE)
		print(prefix + "distortion_correction_method: " + self.distortion_correction_method)
		print(prefix + "NEG_fieldmapseries: " + self.NEG_fieldmapseries)
		print(prefix + "POS_fieldmapseries: " + self.POS_fieldmapseries)
		print(prefix + "scan_id_list: " + str(self.scan_id_list))
		print(prefix + "scan_type_list: " + str(self.scan_type_list))
		print(prefix + "scan_dwell_time_list: " + str(self.scan_dwell_time_list))
		print(prefix + "scan_phase_encoding_direction_list: " + str(self.scan_phase_encoding_direction_list))

	def add_scan(self, scan_number, scan_type, scan_dwell_time, scan_phase_encoding_direction):
		self._scan_id_list.append(scan_number)
		self._scan_type_list.append(scan_type)
		self._scan_dwell_time_list.append(scan_dwell_time)
		self._scan_phase_encoding_direction_list.append(scan_phase_encoding_direction)

	def validate(self):
		# No tests defined yet

		# All tests passed
		return True
