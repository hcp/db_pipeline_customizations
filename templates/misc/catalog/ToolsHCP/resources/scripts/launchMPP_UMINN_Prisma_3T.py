'''
@author: Timothy B. Brown (tbbrown@wustl.edu) 
'''

PROGRAM_VERSION = '1.0.0'

# XNAT session type for the session which should be used for preprocessing
PROCESSING_SESSION_TYPE = 'xnat:mrSessionData'

# Resource suffix indicating that it is an unprocessing resource
UNPROC_SUFFIX = '_unproc'

# Info about the first usable T1w scan
FIRST_T1W_FILE_NAME_BASE = 'T1w_MPR1'
FIRST_T1W_UNPROC_RESOURCE_NAME = FIRST_T1W_FILE_NAME_BASE + UNPROC_SUFFIX

# Info about the second usable T1w scan
SECOND_T1W_FILE_NAME_BASE = 'T1w_MPR2'
SECOND_T1W_UNPROC_RESOURCE_NAME = SECOND_T1W_FILE_NAME_BASE + UNPROC_SUFFIX

# Info about the first usable T2w scan
FIRST_T2W_FILE_NAME_BASE = 'T2w_SPC1'
FIRST_T2W_UNPROC_RESOURCE_NAME = FIRST_T2W_FILE_NAME_BASE + UNPROC_SUFFIX

# Info about the second usable T2w scan
SECOND_T2W_FILE_NAME_BASE = 'T2w_SPC2'
SECOND_T2W_UNPROC_RESOURCE_NAME = SECOND_T2W_FILE_NAME_BASE + UNPROC_SUFFIX

COMPRESSED_NIFTI_EXTENSION = '.nii.gz'

# This CSV file exists in each unprocessed resource to let us know what scan
# numbers correspond to each image file in the resource
SCAN_NUMBER_MAPPING_FILE_NAME = 'filescans.csv'

MAG_FIELDMAP_NAME = 'FieldMap_Magnitude'
PHASE_FIELDMAP_NAME = 'FieldMap_Phase'

SPIN_ECHO_FIELDMAP_NAME = 'SpinEchoFieldMap'
#POSITIVE_SPIN_ECHO_FIELDMAP_NAME = SPIN_ECHO_FIELDMAP_NAME + '_PA'
#NEGATIVE_SPIN_ECHO_FIELDMAP_NAME = SPIN_ECHO_FIELDMAP_NAME + '_AP'

import sys
import subprocess
import argparse # For parsing command line arguments
import StringIO # Read string buffers as if they were files (i.e. read in-memory files)
import csv	  # Comma Separated Value (csv) reading
import socket

from xnat_access import XnatAccess		# The XnatAccess class allows us to interact with an XNAT instance
from mpp_parameters_UMINN_Prisma_3T import MppParameters_UMINN_Prisma_3T  # The MppParameters class manages parameters the must be specified to invoke MPP pipelines

import numpy
import time

from workflow import Workflow

def show_retrieved_params( args ):
	"""
	Display (on stdout) user specified input parameters for this program.
	"""
	print("\nInput Parameters")
	print("\tdebug: " + str(args.debug))
	print("\tserver: " + args.server)
	print("\tusername: " + args.username)
	print("\tproject: " + args.project)
	print("\tsubject: " + args.subject)
	if args.session != "":
		print("\tsession: " + args.session)
	else:
		print("\tsession_suffix: " + args.session_suffix)
	print("\tstructural: " + str(args.structural))
	print("\tfieldmaps: " + args.fieldmaps)
	print("\tfunctional: " + str(args.functional))
	print("\tdiffusion: " + str(args.diffusion))
	print("\tdiffusion_direction_numbers: " + args.diffusion_direction_numbers)
	print("\ticafix: " + str(args.icafix))
	# print("\ttask: " + str(args.task))
	print("\tgo: " + str(args.go))
	print("\tshadow_number: " + str(args.shadow_number))
	print("\tpipeline_project: " + str(args.pipeline_project))
	print("\tphase_encoding: " + str(args.phase_encoding))
	print("\tgradient_distortion_coeffs_file: " + args.gradient_distortion_coeffs_file)

def retrieve_params( ):
	"""
	Parse the user specified command-line parameters to create an args object containing the user specified values
	"""
	parser = argparse.ArgumentParser(description="Script to generate proper command for XNAT Minimal Preprocessing (MPP) launching ...")

	parser.add_argument("-d",  "--debug",	dest="debug", action='store_true')
	parser.add_argument("-v",  "--version",  action='version', version="%(prog)s " + PROGRAM_VERSION)
	parser.add_argument("-s",  "--server",   dest="server", required=True, type=str,
					help="e.g. db.humanconnectome.org")
	parser.add_argument("-u",  "--username", dest="username", required=True, type=str)
	parser.add_argument("-pw", "--password", dest="password", required=True, type=str)
	parser.add_argument("-pr", "--project",  dest="project", required=True, type=str,
					help="e.g. PipelineTest, HCP_Staging, WU_L1A_Staging, etc.")
	parser.add_argument("-su", "--subject",  dest="subject", required=True, type=str)

	session_group = parser.add_mutually_exclusive_group(required=True)
	session_group.add_argument("-ss", "--sessionsuffix", dest="session_suffix", default="_3T", type=str,
							help="Build the session name by adding this suffix to the subject id specified. Defaults to _3T")
	session_group.add_argument("-se", "--session", dest="session", default="", type=str,
							help="Session id")

	parser.add_argument("-st", "--structural", dest="structural", action='store_true',
					help="specifying this argument means structural preprocessing is to be done")

	field_map_help_str = \
		"GE | SiemensGradientEcho = use Siemens Gradient Echo Field Maps; " + \
		"SE | SpinEcho = use Spin Echo Field Maps; " + \
		"NONE = do not use Field Maps"

	parser.add_argument("-fm", "--fieldmaps", dest="fieldmaps",
					choices=('GE', 'SiemensGradientEcho', 'SE', 'SpinEcho', 'NONE'),
					default="GE", type=str, help=field_map_help_str)

	parser.add_argument("-fu", "--functional", dest="functional", action='store_true',
					help="specifying this argument means functional preprocessing is to be done")

	parser.add_argument("-di", "--diffusion", dest="diffusion", action='store_true',
					help="specifying this argument means diffusion preprocessing is to be done")
	parser.add_argument("-dd", "--diffusion_direction_numbers", dest="diffusion_direction_numbers", type=str,
					default="80,81", help="comma separated list of diffusion scan direction numbers")

	parser.add_argument("-ic", "--icafix", dest="icafix", action='store_true',
					help="specifying this argument means ICA FIX processing is to be done")

	# parser.add_argument("-ta", "--task", dest="task", action='store_true',
	# 					help='specifying this argument means fMRI Task Analysis processing is to be done')

	parser.add_argument("-g", "--go", dest="go", action='store_true',
						help="specifying this argument means that the MPP will actually launch, otherwise it is just a test run")
	parser.add_argument("-sn", "--shadow-number", dest="shadow_number", choices=('1', '2', '3', '4', '5', '6', '7', '8'),
					default='5', type=str, help="Database shadow number")

	parser.add_argument("-pp", "--pipeline-project", dest="pipeline_project", default="", type=str,
					help="Pipeline project - determines which Pipeline Project to use -- TODO: needs better documentation")

	parser.add_argument("-pe", "--phase-encoding", dest="phase_encoding", default="PA", type=str,
					help="Phase encoding direction indicator (PA or AP ==> Positive=PA, Negative=AP) (LR or RL ==> Positive=RL, Negative=LR)")

	parser.add_argument("-gdcf", "--gradient-distortion-coeffs-file", dest="gradient_distortion_coeffs_file",
					default="CMRR_3TPrisma_coeff_AS82_trunc.grad", type=str)

	args = parser.parse_args()
	return args

def create_resource_file_name_to_scan_number_map(xnat_access):
	"""
	create a map/dict which when given a file name will return the scan number that corresponds 
	to that file name
	"""

	file_name_to_scan_number = dict()
	file_name_list = xnat_access.get_resource_file_name_list()
	if SCAN_NUMBER_MAPPING_FILE_NAME in file_name_list:
		scan_mapping_str = xnat_access.get_named_resource_file_content(SCAN_NUMBER_MAPPING_FILE_NAME)
		f = StringIO.StringIO(scan_mapping_str)
		reader = csv.reader(f, delimiter=',')
		for row in reader:
			key = row[1].replace("'", "")
			val = row[0].replace("'", "")
			if val != 'Scan':
				file_name_to_scan_number[key] = val

	return file_name_to_scan_number

def debug_print( str ):
	if args.debug: print "DEBUG: " + str

def is_unprocessed_resource( resource_label ):
	return resource_label.endswith(UNPROC_SUFFIX)

def get_unprocessed_resource_base_name( label ):
	return label[:len(label)-len(UNPROC_SUFFIX)]

def get_primary_scan_name_for_resource( xnat_access, resource_name ):
	return xnat_access.session + '_' + resource_name + COMPRESSED_NIFTI_EXTENSION


# Main functionality

# Get and show the command line parameters
global args
args = retrieve_params()
show_retrieved_params(args)

if args.phase_encoding == "PA" or args.phase_encoding == "AP":
	positive_phase_encoding_suffix = "_PA"
	negative_phase_encoding_suffix = "_AP"
	phase_encoding_spec = '2'
elif args.phase_encoding == "RL" or args.phase_encoding == "LR":
	positive_phase_encoding_suffix = "_RL"
	negative_phase_encoding_suffix = "_LR"
	phase_encoding_spec = '1'
else:
	print("ERROR: Unrecognized phase_encoding specification: " + args.phase_encoding)
	sys.exit(1)

# Initialize MPP processing parameters

mpp_parameters = MppParameters_UMINN_Prisma_3T(args.pipeline_project, phase_encoding_spec, args.gradient_distortion_coeffs_file)
mpp_parameters.debug = args.debug

# Use specified username and password to get token username and password
print("\nRequesting token username and password")
xnat_access = XnatAccess(args.server, args.username, args.password)
new_username, new_password = xnat_access.get_new_tokens()
args.username = new_username
args.password = new_password
debug_print("args.username: " + args.username)
debug_print("args.password: " + args.password)

# Establish connection to XNAT with token username and password
print("\nConnecting to: " + args.server)
xnat_access = XnatAccess(args.server, args.username, args.password)

# Establish project
xnat_access.project=args.project
print("\nEstablished current project: " + xnat_access.project)
mpp_parameters.project=args.project

# Establish subject
xnat_access.subject=args.subject
print("\nEstablished current subject: " + xnat_access.subject)
mpp_parameters.subject=xnat_access.subject

# Validate appropriate MR session exists and establish session
if (args.session != ""):
	session_label_for_processing = args.session
else:
	session_label_for_processing = xnat_access.subject + args.session_suffix

debug_print("session_label_for_processing: " + session_label_for_processing)

session_type = xnat_access.get_session_type(session_label_for_processing)
debug_print("session_type: " + session_type)

if session_type != PROCESSING_SESSION_TYPE:
	print("Session: " + session_label_for_processing + " is not of correct session type.")
	sys.exit(1)

xnat_access.session = session_label_for_processing
debug_print("xnat_access.session: " + xnat_access.session)

mpp_parameters.session = xnat_access.session
mpp_parameters.xnat_session_id = xnat_access.xnat_session_id

# Gather structural preprocessing parameters if required
if (args.structural):
	print("\nDetermining parameters for Structural Preprocessing")
	mpp_parameters.launch_structural = True
	mpp_parameters.fieldmap_type_str = args.fieldmaps

	# Determine parameters related to first T1w scan
	if (xnat_access.does_resource_exist(FIRST_T1W_UNPROC_RESOURCE_NAME)) :
		print("\nDetermining parameters for first T1w scan")
		xnat_access.resource = FIRST_T1W_UNPROC_RESOURCE_NAME
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)
		
		# Set parameter values for the first T1w scan
		file_name = xnat_access.session + '_' + FIRST_T1W_FILE_NAME_BASE + COMPRESSED_NIFTI_EXTENSION
		scan_number = file_name_to_scan_number[file_name]
		series_description = xnat_access.get_scan_data_field_value(scan_number, 'series_description')
		sample_spacing = xnat_access.get_scan_data_field_value(scan_number, 'parameters/readoutSampleSpacing')
		# sample_spacing value is supplied in nanoseconds from XNAT, but needs to be specified in
		# seconds to structural preprocessing script
		sample_spacing_str = '{:11.9f}'.format(float(sample_spacing)/1000000000.0)
		mpp_parameters.set_first_t1w(file_name, scan_number, series_description, sample_spacing_str)

		if mpp_parameters.fieldmap_type_str == "GE" or mpp_parameters.fieldmap_type_str == "SiemensGradientEcho":
			# Set parameter values for the magnitude field map for the first T1w scan
			file_name = xnat_access.session + '_' + MAG_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			deltaTE = xnat_access.get_scan_data_field_value(scan_number, 'parameters/deltaTE')
			mpp_parameters.set_first_t1w_magfieldmap(file_name, scan_number, deltaTE)

			# Set parameter values for the phase field map for the first T1w scan
			file_name = xnat_access.session + '_' + PHASE_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			mpp_parameters.set_first_t1w_phasefieldmap(file_name, scan_number)

		elif mpp_parameters.fieldmap_type_str == "SE" or mpp_parameters.fieldmap_type_str == "SpinEcho":
			# Set parameter values for the Positive Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + positive_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]			
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_first_t1w_positive_spin_echo_fieldmap(file_name, scan_number, dwell_time)

			# Set parameter values for the Negative Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + negative_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_first_t1w_negative_spin_echo_fieldmap(file_name, scan_number, dwell_time)

		elif mpp_parameters.fieldmap_type_str == "NONE":
			pass

		else:
			print("ERROR: Unrecognized fieldmap type specification: " + mpp_parameters.fieldmap_type_str)
			sys.exit(1)
			
	# Determine parameters related to second T1w scan
	if (xnat_access.does_resource_exist(SECOND_T1W_UNPROC_RESOURCE_NAME)):
		print("\nDetermining parameters for second T1w scan")
		xnat_access.resource = SECOND_T1W_UNPROC_RESOURCE_NAME
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)

		# Set parameter values for the second T1w scan
		file_name = xnat_access.session + '_' + SECOND_T1W_FILE_NAME_BASE + COMPRESSED_NIFTI_EXTENSION
		scan_number = file_name_to_scan_number[file_name]
		series_description = xnat_access.get_scan_data_field_value(scan_number, 'series_description')
		sample_spacing = xnat_access.get_scan_data_field_value(scan_number, 'parameters/readoutSampleSpacing')
		# sample_spacing value is supplied in nanoseconds from XNAT, but needs to be specified in
		# seconds to structural preprocessing script
		sample_spacing_str = '{:11.9f}'.format(float(sample_spacing)/1000000000.0)
		mpp_parameters.set_second_t1w(file_name, scan_number, series_description, sample_spacing_str)

		if mpp_parameters.fieldmap_type_str == "GE" or mpp_parameters.fieldmap_type_str == "SiemensGradientEcho":
			# Set parameter values for the magnitude field map for the second T1w scan
			file_name = xnat_access.session + '_' + MAG_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			deltaTE = xnat_access.get_scan_data_field_value(scan_number, 'parameters/deltaTE')
			mpp_parameters.set_second_t1w_magfieldmap(file_name, scan_number, deltaTE)

			# Set parameter values for the phase field map for the second T1w scan
			file_name = xnat_access.session + '_' + PHASE_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			mpp_parameters.set_second_t1w_phasefieldmap(file_name, scan_number)

		elif mpp_parameters.fieldmap_type_str == "SE" or mpp_parameters.fieldmap_type_str == "SpinEcho":
			# Set parameter values for the Positive Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + positive_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_second_t1w_positive_spin_echo_fieldmap(file_name, scan_number, dwell_time)
			
			# Set parameter values for the Negative Spin Echo Field Map
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + negative_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_second_t1w_negative_spin_echo_fieldmap(file_name, scan_number, dwell_time)

		elif mpp_parameters.fieldmap_type_str == "NONE":
			pass
		
		else:
			print("ERROR: Unrecognized fieldmap type specification: " + mpp_parameters.fieldmap_type_str)
			sys.exit(1)

	# Determine parameters related to first T2w scan
	if (xnat_access.does_resource_exist(FIRST_T2W_UNPROC_RESOURCE_NAME)):
		print("\nDetermining parameters for first T2w scan")
		xnat_access.resource = FIRST_T2W_UNPROC_RESOURCE_NAME
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)
		
		# Set parameter values for the first T2w scan
		file_name = xnat_access.session + '_' + FIRST_T2W_FILE_NAME_BASE + COMPRESSED_NIFTI_EXTENSION
		scan_number = file_name_to_scan_number[file_name]
		series_description = xnat_access.get_scan_data_field_value(scan_number, 'series_description')
		sample_spacing = xnat_access.get_scan_data_field_value(scan_number, 'parameters/readoutSampleSpacing')
		# sample_spacing value is supplied in nanoseconds from XNAT, but needs to be specified in
		# seconds to structural preprocessing script
		sample_spacing_str = '{:11.9f}'.format(float(sample_spacing)/1000000000.0)
		mpp_parameters.set_first_t2w(file_name, scan_number, series_description, sample_spacing_str)

		if mpp_parameters.fieldmap_type_str == "GE" or mpp_parameters.fieldmap_type_str == "SiemensGradientEcho":
			# Set parameter values for the magnitude field map for the first T2w scan
			file_name = xnat_access.session + '_' + MAG_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			deltaTE = xnat_access.get_scan_data_field_value(scan_number, 'parameters/deltaTE')
			mpp_parameters.set_first_t2w_magfieldmap(file_name, scan_number, deltaTE)

			# Set parameter values for the phase field map for the first T2w scan
			file_name = xnat_access.session + '_' + PHASE_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			mpp_parameters.set_first_t2w_phasefieldmap(file_name, scan_number)

		elif mpp_parameters.fieldmap_type_str == "SE" or mpp_parameters.fieldmap_type_str == "SpinEcho":
			# Set parameter values for the Positive Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + positive_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]			
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_first_t2w_positive_spin_echo_fieldmap(file_name, scan_number, dwell_time)

			# Set parameter values for the Negative Spin Echo Field Map
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + negative_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_first_t2w_negative_spin_echo_fieldmap(file_name, scan_number, dwell_time)

		else:
			print("ERROR: Unrecognized fieldmap type specification: " + mpp_parameters.fieldmap_type_str)
			sys.exit(1)

	# Determine parameters related to second T2w scan
	if (xnat_access.does_resource_exist(SECOND_T2W_UNPROC_RESOURCE_NAME)):
		print("\nDetermining parameters for second T2w scan")
		xnat_access.resource = SECOND_T2W_UNPROC_RESOURCE_NAME
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)

		# Set parameter values for the second T2w scan
		file_name = xnat_access.session + '_' + SECOND_T2W_FILE_NAME_BASE + COMPRESSED_NIFTI_EXTENSION
		scan_number = file_name_to_scan_number[file_name]
		series_description = xnat_access.get_scan_data_field_value(scan_number, 'series_description')
		sample_spacing = xnat_access.get_scan_data_field_value(scan_number, 'parameters/readoutSampleSpacing')
		# sample_spacing value is supplied in nanoseconds from XNAT, but needs to be specified in
		# seconds to structural preprocessing script
		sample_spacing_str = '{:11.9f}'.format(float(sample_spacing)/1000000000.0)
		mpp_parameters.set_second_t2w(file_name, scan_number, series_description, sample_spacing_str)

		if mpp_parameters.fieldmap_type_str == "GE" or mpp_parameters.fieldmap_type_str == "SiemensGradientEcho":
			# Set parameter values for the magnitude field map for the second T2w scan
			file_name = xnat_access.session + '_' + MAG_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			deltaTE = xnat_access.get_scan_data_field_value(scan_number, 'parameters/deltaTE')
			mpp_parameters.set_second_t2w_magfieldmap(file_name, scan_number, deltaTE)

			# Set parameter values for the phase field map for the second T2w scan
			file_name = xnat_access.session + '_' + PHASE_FIELDMAP_NAME + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			mpp_parameters.set_second_t2w_phasefieldmap(file_name, scan_number)

		elif mpp_parameters.fieldmap_type_str == "SE" or mpp_parameters.fieldmap_type_str == "SpinEcho":
			# Set parameter values for the Positive Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + positive_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]			
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_second_t2w_positive_spin_echo_fieldmap(file_name, scan_number, dwell_time)

			# Set parameter values for the Negative Spin Echo Field Map 
			file_name = xnat_access.session + '_' + SPIN_ECHO_FIELDMAP_NAME + negative_phase_encoding_suffix + COMPRESSED_NIFTI_EXTENSION
			scan_number = file_name_to_scan_number[file_name]
			dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
			mpp_parameters.set_second_t2w_negative_spin_echo_fieldmap(file_name, scan_number, dwell_time)

		elif mpp_parameters.fieldmap_type_str == "NONE":
			pass

		else:
			print("ERROR: Unrecognized fieldmap type specification: " + mpp_parameters.fieldmap_type_str)
			sys.exit(1)

# Gather functional preprocessing parameters if required
if (args.functional):
	functional_scan_types = list()
	functional_scan_types.append('tfMRI')
	functional_scan_types.append('rfMRI')

	print("\nDetermining parameters for Functional Preprocessing")
	mpp_parameters.launch_functional = True

	resource_label_list = xnat_access.get_resource_label_list()
	debug_print("resource_label_list: " + str(resource_label_list))

	# cycle through all the resources
	for label in resource_label_list:
		xnat_access.resource = label
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)
		
		# if the resource is an "unprocessed" resource
		if is_unprocessed_resource(label):
			resource_name = get_unprocessed_resource_base_name(label)
			debug_print("resource_name: " + resource_name)

			# determine the name of the "primary" scan that would be part of the resource
			full_scan_name = get_primary_scan_name_for_resource(xnat_access, resource_name)
			debug_print("full_scan_name: " + full_scan_name)

			# if that "primary" scan exists, get its scan number so we can find out some things about it
			if file_name_to_scan_number.has_key(full_scan_name):
				scan_number = file_name_to_scan_number[full_scan_name]
				debug_print("scan_number: " + scan_number)
				scan_type = xnat_access.get_scan_data_field_value(scan_number, 'type')
				debug_print("scan_type: " + scan_type)
				
				# if it is a functional scan (as determined by its scan type)
				if scan_type in functional_scan_types:
					# then this is a scan we want to put through functional preprocessing
					print("\nSetting parameters for resource: " + resource_name)
					dwell_time = xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
					pe_direction = xnat_access.get_scan_data_field_value(scan_number, 'parameters/peDirection')

					# The pe_direction is returned from the db as +x, -x, +y, -y, +z, or -z.
					# But the pipeline expects positive directions to _not_ be prefaced with +.
					# So we need to strip the plus sign off the front if it is there.
					if pe_direction.startswith('+'):
						pe_direction = pe_direction[1:]
					debug_print("pe_direction: " + pe_direction)

					mpp_parameters.add_functional_scan(scan_number, resource_name, dwell_time, str(pe_direction))


# Gather diffusion preprocessing parameters if required
if (args.diffusion):
	print("\nDetermining parameters for Diffusion Preprocessing")
	mpp_parameters.launch_diffusion = True

	resource_label_list = xnat_access.get_resource_label_list()
	debug_print("resouce_label_list: " + str(resource_label_list))

	# cycle through all resources
	for label in resource_label_list:
		xnat_access.resource = label
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)

		# if the resource is an "unprocessed" resource
		if is_unprocessed_resource(label):
			resource_name = get_unprocessed_resource_base_name(label)
			debug_print("resource_name: " + resource_name)

			# if this is a "Diffusion" scan
			if (resource_name == "Diffusion"):
				for scan_dir_number in ((args.diffusion_direction_numbers.split(','))):
					directions = list()
					if phase_encoding_spec == '1':
						directions.append('RL')
						directions.append('LR')
					elif phase_encoding_spec == '2':
						directions.append('PA')
						directions.append('AP')
					else:
						print("ERROR: unrecognized phase_encoding_spec: " + phase_encoding_spec)
						sys.exit(1)

					for direction in directions:
						full_scan_name = xnat_access.session + '_' + 'DWI_dir' + scan_dir_number + '_' + direction + COMPRESSED_NIFTI_EXTENSION
						debug_print("full_scan_name: " + full_scan_name)
						if file_name_to_scan_number.has_key(full_scan_name):
							scan_number = file_name_to_scan_number[full_scan_name]
							debug_print("scan_number: " + scan_number)
							echo_spacing = float(xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')) * 1.0e+3
							debug_print("echo_spacing: " + str(echo_spacing))
							mpp_parameters.add_diffusion_scan(scan_number, direction, scan_dir_number, echo_spacing)

# Gather ICA FIX processing parameters if required
if (args.icafix):
	resting_state_scan_types = list()
	resting_state_scan_types.append('rfMRI')

	print("\nDetermining parameters for ICA FIX processing")
	mpp_parameters.launch_ica_fix = True

	resource_label_list = xnat_access.get_resource_label_list()
	debug_print("resource_label_list: " + str(resource_label_list))

	# cycle through all the resources
	for label in resource_label_list:
		xnat_access.resource = label
		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)

		# if the resource is an "unprocessed" resource
		if is_unprocessed_resource(label):
			resource_name = get_unprocessed_resource_base_name(label)
			debug_print("resource_name: " + resource_name)

			# determine the name of the "primary" scan that would be part of the resource
			full_scan_name = get_primary_scan_name_for_resource(xnat_access, resource_name)
			debug_print("full_scan_name: " + full_scan_name)

			# if that "primary" scan exists, get its scan number so we can find out some things about it
			if file_name_to_scan_number.has_key(full_scan_name):
				scan_number = file_name_to_scan_number[full_scan_name]
				debug_print("scan_number: " + scan_number)
				scan_type = xnat_access.get_scan_data_field_value(scan_number, 'type')
				debug_print("scan_type: " + scan_type)

				# if it is a resting state scan (as determined by its scan type)
				if scan_type in resting_state_scan_types:
					# then this is a scan we want to put through ICA FIX processing
					print("\nSetting parameters for resource: " + resource_name)

					mpp_parameters.add_icafix_scan(resource_name)

# Gather Task Analysis processing parameters if required
# if (args.task):
# 	task_scan_types = list()
# 	task_scan_types.append('tfMRI')

# 	print("\nDetermining parameters for Task Analysis processing")
# 	mpp_parameters.launch_task_analysis = True

# 	resource_label_list = xnat_access.get_resource_label_list()
# 	debug_print("resource_label_list: " + str(resource_label_list))

# 	# cycle through all the resources
# 	for label in resource_label_list:
# 		xnat_access.resource = label
# 		file_name_to_scan_number = create_resource_file_name_to_scan_number_map(xnat_access)
		
# 		# if the resource is an "unprocessed" resource
# 		if is_unprocessed_resource(label):
# 			resource_name = get_unprocessed_resource_base_name(label)
# 			debug_print("resource_name: " + resource_name)
			
# 			# determine the name of the "primary" scan that would be part of the resource
# 			full_scan_name = get_primary_scan_name_for_resource(xnat_access, resource_name)
# 			debug_print("full_scan_name: " + full_scan_name)

# 			# if that "primary" scan exists, get its scan number so we can find out some things about it
# 			if file_name_to_scan_number.has_key(full_scan_name):
# 				scan_number = file_name_to_scan_number[full_scan_name]
# 				debug_print("scan_number: " + scan_number)
# 				scan_type = xnat_access.get_scan_data_field_value(scan_number, 'type')
# 				debug_print("scan_type: " + scan_type)

# 				# if it is a task scan (as determined by its scan type)
# 				if scan_type in task_scan_types:
# 					# then this is a scan type that we want to put through Task Analysis
# 					print("\nSetting parameters for resource: " + resource_name)

# 					mpp_parameters.add_task_scan(resource_name)




# Validate parameters for use with MPP
if not mpp_parameters.validate():
	print("ERROR: MPP parameters invalid")
	exit(1)

# Show MPP parameters
print("")
print("MPP Parameters")
print("")
mpp_parameters.show_all()

# Determine path to MPP parameter XML file
db_str = 'hcpdb'
build_dir_root = '/data/%s/build/%s/' % (db_str, mpp_parameters.project)

sTime = time.time()
curr_build_dir = build_dir_root + str(numpy.asarray(round(sTime), dtype=numpy.uint64)) + '_' + mpp_parameters.subject
parameter_file_path='%s/%s.xml' % (curr_build_dir, mpp_parameters.subject)
debug_print("Determined parameter_file_path: " + parameter_file_path)

# Store parameters in an XML file
print("About to save MPP Parameters in XML file at path: " + parameter_file_path)
mpp_parameters.save_params_to_xml_file(parameter_file_path)
	
# Build launching command

MPP_PIPELINE_NAME='MPP/MPP.xml'

if args.pipeline_project:
	MPP_PIPELINE_NAME=args.pipeline_project + '/' + MPP_PIPELINE_NAME

print("Initial pipeline to launch is: " + MPP_PIPELINE_NAME)

JOB_SUBMITTER = '/data/%s/pipeline/bin/schedule ' % (db_str)
PIPELINE_LAUNCHER = '/data/%s/pipeline/bin/XnatPipelineLauncher ' % (db_str)

DATA_TYPE_STR = 'xnat:mrSessionData'
DATA_TYPE = ' -dataType %s ' % (DATA_TYPE_STR)

XNAT_SERVER_STR = 'ConnectomeDB'
XNAT_SERVER = ' -parameter xnatserver=%s ' % (XNAT_SERVER_STR)
SUPPRESS_NOTIFY = ' -supressNotification '
NOTIFY_USER_STR = 'hilemanm@mir.wustl.edu'
NOTIFY_USER = ' -notify %s ' % (NOTIFY_USER_STR)
NOTIFY_ADMIN_STR = 'db-admin@humanconnectome.org'
NOTIFY_ADMIN = ' -notify %s ' % (NOTIFY_ADMIN_STR)
ADMIN_EMAIL = ' -parameter adminemail=%s ' % (NOTIFY_ADMIN_STR)
USER_EMAIL = ' -parameter useremail=%s ' % (NOTIFY_USER_STR)
MAIL_HOST_STR = 'mail.nrg.wustl.edu'
MAIL_HOST = ' -parameter mailhost=%s ' % MAIL_HOST_STR
USER_FULL_NAME_STR = 'MPPUser'
USER_FULL_NAME = ' -parameter userfullname=%s ' % (USER_FULL_NAME_STR)
BUILD_DIR = ' -parameter builddir=%s ' % (curr_build_dir)

if (args.go):
	launcher_pipeline = ' -pipeline %s ' %(MPP_PIPELINE_NAME)
	launcher_parameter_file = ' -parameter paramfile=%s ' % (parameter_file_path)
	launcher_compute_cluster = ' -parameter compute_cluster=CHPC '
	launcher_subject_parameter = ' -parameter subject=%s ' % mpp_parameters.subject
	current_jsession=xnat_access.get_jsession_id()

	workflow_obj = Workflow(xnat_access.user, xnat_access.password, xnat_access.server, current_jsession)
	workflow_id  = workflow_obj.createWorkflow(mpp_parameters.xnat_session_id, xnat_access.project, MPP_PIPELINE_NAME, 'Queued')
	launcher_workflow_primary_key = ' -workFlowPrimaryKey %s ' % workflow_id

	launcher_hcp_id = ' -id %s ' % mpp_parameters.xnat_session_id

	host = ' -host http://db-shadow' + args.shadow_number + '.nrg.mir:8080 '

	launcher_external_project = ' -project %s ' % (xnat_access.project)
	launcher_user = ' -u %s ' % xnat_access.user
	launcher_password = ' -pwd %s ' % xnat_access.password

	submit_str = JOB_SUBMITTER + PIPELINE_LAUNCHER + launcher_pipeline + launcher_hcp_id + DATA_TYPE + host + XNAT_SERVER + \
				 launcher_external_project + launcher_user + launcher_password + SUPPRESS_NOTIFY + NOTIFY_USER + NOTIFY_ADMIN + \
				 ADMIN_EMAIL + USER_EMAIL + MAIL_HOST + USER_FULL_NAME + BUILD_DIR + launcher_subject_parameter + launcher_parameter_file + \
				 launcher_compute_cluster + launcher_workflow_primary_key

	print("")
	print("Submission String: ")
	print(submit_str)
	print("")

	subprocess.call(submit_str, shell=True)
