from structural_parameters import StructuralParameters
from structural_parameters_UMINN_Prisma_3T import StructuralParameters_UMINN_Prisma_3T

from functional_parameters import FunctionalParameters
from functional_parameters_UMINN_Prisma_3T import FunctionalParameters_UMINN_Prisma_3T

from diffusion_parameters import DiffusionParameters
from diffusion_parameters_UMINN_Prisma_3T import DiffusionParameters_UMINN_Prisma_3T
from lifespan_diffusion_parameters import LifeSpanDiffusionParameters

from task_analysis_parameters import TaskAnalysisParameters

from ica_fix_parameters import IcaFixParameters
from ica_fix_parameters_UMINN_Prisma_3T import IcaFixParameters_UMINN_Prisma_3T

LIFESPAN_PHASE1A_PIPELINE_PROJECT = "WU_L1A"
LIFESPAN_PHASE1B_PIPELINE_PROJECT = "WU_L1B"
UMINN_PRISMA_3T_PROJECT = "UMINN_Prisma_3T"

class MppParametersFactory(object):
	"""
	This class is a factory for creating the necessary pipeline specific
	(Structural Preprocessing, Functional Preprocessing, Diffusion Preprocessing,
	ICA FIX processing, Task fMRI Analysis processing) parameter objects that are
	part of the overall MPP parameters.
	"""

	def __init__(self, pipeline_project):
		super(MppParametersFactory, self).__init__()
		self._pipeline_project = pipeline_project

	def __init__(self, pipeline_project, phase_encoding, gradient_distortion_coeffs_file):
		"""Create a factory to create pipeline specific parameter objects
		
		Args:
			pipeline_project (str): The pipeline project (e.g. WU_L1A or '')
		"""
		
		super(MppParametersFactory, self).__init__()
		self._pipeline_project = pipeline_project
		self._phase_encoding_spec = phase_encoding
		self._gradient_distortion_coeffs_file = gradient_distortion_coeffs_file

	def _get_pipeline_project(self):
		return self._pipeline_project

	pipeline_project=property(_get_pipeline_project)

	def get_structural_parameters(self):
		"""Get the appropriate StructuralParameters object for the pipeline project

		Returns:
		  An appropriate StructuralParameters object 

		"""
		if self.pipeline_project == UMINN_PRISMA_3T_PROJECT:
			structural_parameters = StructuralParameters_UMINN_Prisma_3T()
			structural_parameters.gradient_distortion_coeffs_file = self._gradient_distortion_coeffs_file
		else:
			structural_parameters = StructuralParameters()

		return structural_parameters

	def get_functional_parameters(self):
		"""Get the appropriate FunctionalParameters object for the pipeline project

		Returns:
		  An appropriate FunctionalParameters object

		"""
		if self.pipeline_project == UMINN_PRISMA_3T_PROJECT:
			functional_parameters = FunctionalParameters_UMINN_Prisma_3T(self._phase_encoding_spec)
			functional_parameters.gradient_distortion_coeffs_file = self._gradient_distortion_coeffs_file
		else:
			functional_parameters = FunctionalParameters()

		return functional_parameters

	def get_diffusion_parameters(self):
		"""Get the appropriate DiffusionParameters object for the pipeline project

		Returns:
		  An appropriate DiffusionParemeters object

		"""
		if self.pipeline_project == LIFESPAN_PHASE1A_PIPELINE_PROJECT:
			diffusion_parameters = LifeSpanDiffusionParameters()
		elif self.pipeline_project == LIFESPAN_PHASE1B_PIPELINE_PROJECT:
			diffusion_parameters = LifeSpanDiffusionParameters()
		elif self.pipeline_project == UMINN_PRISMA_3T_PROJECT:
			diffusion_parameters = DiffusionParameters_UMINN_Prisma_3T(self._phase_encoding_spec)
			diffusion_parameters.gradient_distortion_coeffs_file = self._gradient_distortion_coeffs_file
		else:
			diffusion_parameters = DiffusionParameters()
			
		return diffusion_parameters

	def get_task_analysis_parameters(self):
		"""Get the appropriate TaskAnalysisParameters object for the pipeline project

		Returns:
		  An appropriate TaskAnalysisParameters object

		"""
		task_analysis_parameters = TaskAnalysisParameters()
		return task_analysis_parameters
		
	def get_ica_fix_parameters(self):
		"""Get the appropriate IcaFixParameters object for the pipeline project

		Returns:
		  An appropriate IcaFixParameters object

		"""
		if self.pipeline_project == UMINN_PRISMA_3T_PROJECT:
			ica_fix_parameters = IcaFixParameters_UMINN_Prisma_3T(self._phase_encoding_spec)
		else:
			ica_fix_parameters = IcaFixParameters()

		return ica_fix_parameters

# end of module: mpp_parameters_factory.py
