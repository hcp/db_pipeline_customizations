#!/bin/bash

if [ -z "${SUBJECT_FILES_DIR}" ]
then
	echo "Environment variable SUBJECT_FILES_DIR must be set!"
	exit 1
fi

printf "Connectome DB Username: "
read userid

stty -echo
printf "Connectome DB Password: "
read password
echo ""
stty echo

project="WU_L1A_Staging"
subject_file_name="${SUBJECT_FILES_DIR}/${project}.structural.subjects"
echo "Retrieving subject list from: ${subject_file_name}"
subject_list_from_file=( $( cat ${subject_file_name} ) )
subjects="`echo "${subject_list_from_file[@]}"`"

start_shadow_number=1
max_shadow_number=8

shadow_number=${start_shadow_number}

for subject in ${subjects}
do
	echo ""
	echo "--------------------------------------------------------------------------------"
	echo " Launching Structural Preprocessing for subject: ${subject} in project: ${project}"
	echo " On shadow number: ${shadow_number}"
	echo "--------------------------------------------------------------------------------"
	echo ""

	python ../launchMPP.py \
		--debug \
		--server=db.humanconnectome.org \
		--username=${userid} \
		--password=${password} \
		--project=${project} \
		--pipeline-project=WU_L1A \
		--subject=${subject} \
		--sessionsuffix=_3T \
		--structural \
		--fieldmaps=SE \
		--shadow-number=${shadow_number} \
		--go

	shadow_number=$((shadow_number+1))

	if [ "$shadow_number" -gt "$max_shadow_number" ]
	then
		shadow_number=${start_shadow_number}
	fi

done
