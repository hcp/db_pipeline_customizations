import validate
from t1w_parameters import T1wParameters
from t2w_parameters import T2wParameters

class FieldMapType(object):
	NoFieldMaps = 1
	SiemensGradientEchoFieldMaps = 2
	SpinEchoFieldMaps = 3
	GeneralElectricGradientEchoFieldMaps = 4
	
class StructuralParameters_UMINN_Prisma_3T(object):
	"""
	Class for containing and managing parameters for the structural preprocessing part of the MPP pipeline
	"""

	def __init__(self):
		super(StructuralParameters_UMINN_Prisma_3T, self).__init__()

		self._fieldmap_type = FieldMapType.NoFieldMaps
		self._avgrdmethod = "NONE"

		self.first_t1w = T1wParameters()
		self.second_t1w = T1wParameters()
		self.first_t2w = T2wParameters()
		self.second_t2w = T2wParameters()

		self._gradient_distortion_coeffs_file = "UNSET"

		# HARDCODED VALUES THAT PROBABLY SHOULDN'T BE HARDCODED
		self._unwarp_dir = 'z'
		self._se_unwarp_dir = 'y'
		self._topup_config = 'b02b0.cnf'
		self._t1w_template = 'MNI152_T1_0.8mm.nii.gz'
		self._t2w_template = 'MNI152_T2_0.8mm.nii.gz'
		self._t1w_template_brain = 'MNI152_T1_0.8mm_brain.nii.gz'
		self._t2w_template_brain = 'MNI152_T2_0.8mm_brain.nii.gz'
		self._template_mask = 'MNI152_T1_0.8mm_brain_mask.nii.gz'
		self._final_template_space = 'MNI152_T1_0.8mm.nii.gz'
		self._structural_fs_assessor_ext = '3T'

	# property fieldmap_type

	def set_fieldmap_type(self, new_fieldmap_type_str):
		if new_fieldmap_type_str == "GE" or new_fieldmap_type_str == "SiemensGradientEcho" : # Gradient Echo
			self._fieldmap_type = FieldMapType.SiemensGradientEchoFieldMaps
			self._avgrdmethod = "FIELDMAP"
		elif new_fieldmap_type_str == "SE" or new_fieldmap_type_str == "SpinEcho" : # Spin Echo
			self._fieldmap_type = FieldMapType.SpinEchoFieldMaps
			self._avgrdmethod = "TOPUP"
		else:
			print("Unrecognized fieldmap type specification")
			sys.exit(1)

	def get_fieldmap_type(self):
		return self._fieldmap_type

	fieldmap_type=property(get_fieldmap_type, set_fieldmap_type)

	def get_fieldmap_type_str(self):
		if self._fieldmap_type == FieldMapType.NoFieldMaps:
			return "NONE"
		elif self._fieldmap_type == FieldMapType.SiemensGradientEchoFieldMaps:
			return "SiemensGradientEcho"
		elif self._fieldmap_type == FieldMapType.SpinEchoFieldMaps:
			return "SpinEcho"
		elif self._fieldmap_type == FieldMapType.GeneralElectricGradientEchoFieldMaps:
			return "GeneralElectricGradientEcho"
		else:
			print("Unrecognized fieldmap type: " + str(self._fieldmap_type))
			sys.exit(1)
			
	# property avgrdmethod
	def get_avgrdmethod(self):
		return self._avgrdmethod

	avgrdmethod=property(get_avgrdmethod)

	# derived property: t1w_sample_spacing

	def get_t1w_sample_spacing(self):
		if self.is_first_t1w_set and self.is_second_t1w_set:
			msg = "T1w scan sample spacings must be equal"
			if not validate.check_equal(self.first_t1w.sample_spacing, self.second_t1w.sample_spacing, msg):
				sys.exit(1)
			return self.first_t1w.sample_spacing
		elif self.is_first_t1w_set:
			return self.first_t1w.sample_spacing
		elif self.is_second_t1w_set:
			return self.second_t1w.sample_spacing
		else:
			print("Error: No T1w sample spacing set")
			sys.exit(1)

	t1w_sample_spacing=property(get_t1w_sample_spacing)

	# derived property: t2w_sample_spacing

	def get_t2w_sample_spacing(self):
		if self.is_first_t2w_set and self.is_second_t2w_set:
			msg = "T2w scan sample spacings must be equal"
			if not validate.check_equal(self.first_t2w.sample_spacing, self.second_t2w.sample_spacing, msg):
				sys.exit(1)
			return self.first_t2w.sample_spacing
		elif self.is_first_t2w_set:
			return self.first_t2w.sample_spacing
		elif self.is_second_t2w_set:
			return self.second_t2w.sample_spacing
		else:
			print("Error: No T2w sample spacing set")
			sys.exit(1)

	t2w_sample_spacing=property(get_t2w_sample_spacing)

	# derived property: is_first_t1w_set

	def _is_first_t1w_set(self):
		if (self.first_t1w.file_name == '') or (self.first_t1w.scan_number == '') or (self.first_t1w.series_desc == ''):
			return False
		else:
			return True
		
	is_first_t1w_set=property(_is_first_t1w_set)

	# derived property: is_second_t1w_set

	def _is_second_t1w_set(self):
		if (self.second_t1w.file_name == '') or (self.second_t1w.scan_number == '') or (self.second_t1w.series_desc == ''):
			return False
		else:
			return True

	is_second_t1w_set=property(_is_second_t1w_set)

	# derived property: T1w_count

	def _get_T1w_count(self):
		count = 0
		if self.is_first_t1w_set:
			count += 1
		if self.is_second_t1w_set:
			count += 1
		return count

	T1w_count=property(_get_T1w_count)

	# derived property: is_first_t2w_set

	def _is_first_t2w_set(self):
		if (self.first_t2w.file_name == '') or (self.first_t2w.scan_number == '') or (self.first_t2w.series_desc == ''):
			return False
		else:
			return True

	is_first_t2w_set=property(_is_first_t2w_set)

	# derived property: is_second_t2w_set

	def _is_second_t2w_set(self):
		if (self.second_t2w.file_name == '') or (self.second_t2w.scan_number == '') or (self.second_t2w.series_desc == ''):
			return False
		else:
			return True
		
	is_second_t2w_set=property(_is_second_t2w_set)

	# derived property: T2w_count

	def _get_T2w_count(self):
		count = 0
		if self.is_first_t2w_set:
			count += 1
		if self.is_second_t2w_set:
			count += 1
		return count

	T2w_count=property(_get_T2w_count)

	# property unwarp_dir

	def _get_unwarp_dir(self):
		return self._unwarp_dir

	unwarp_dir=property(_get_unwarp_dir)

	# property se_unwarp_dir

	def _get_se_unwarp_dir(self):
		return self._se_unwarp_dir

	se_unwarp_dir=property(_get_se_unwarp_dir)

	# property topup_config

	def _get_topup_config(self):
		return self._topup_config

	topup_config=property(_get_topup_config)

	# property t1w_template

	def _get_t1w_template(self):
		return self._t1w_template

	t1w_template=property(_get_t1w_template)
	
	# property t2w_template

	def _get_t2w_template(self):
		return self._t2w_template

	t2w_template=property(_get_t2w_template)

	# property t1w_template_brain

	def _get_t1w_template_brain(self):
		return self._t1w_template_brain

	t1w_template_brain=property(_get_t1w_template_brain)

	# property t2w_template_brain

	def _get_t2w_template_brain(self):
		return self._t2w_template_brain

	t2w_template_brain=property(_get_t2w_template_brain)

	# property template_mask
	
	def _get_template_mask(self):
		return self._template_mask

	template_mask=property(_get_template_mask)

	# property final_template_space

	def _get_final_template_space(self):
		return self._final_template_space

	final_template_space=property(_get_final_template_space)
	
	# property structural_fs_assessor_ext
	def _get_structural_fs_assessor_ext(self):
		return self._structural_fs_assessor_ext

	structural_fs_assessor_ext=property(_get_structural_fs_assessor_ext)

	# property gradient_distortion_coeffs_file
	def _get_gradient_distortion_coeffs_file(self):
		return self._gradient_distortion_coeffs_file

	def _set_gradient_distortion_coeffs_file(self, new_gradient_distortion_coeffs_file):
		self._gradient_distortion_coeffs_file = new_gradient_distortion_coeffs_file
	
	gradient_distortion_coeffs_file=property(_get_gradient_distortion_coeffs_file, _set_gradient_distortion_coeffs_file)

	# utility methods

	def show_all(self, prefix):
		print(prefix + "fieldmap_type: " + str(self.fieldmap_type))
		print(prefix + "avgrdmethod: " + self._avgrdmethod)
		print(prefix + "First T1w")
		self.first_t1w.show_all(prefix + "\t")
		print(prefix + "Second T1w")
		self.second_t1w.show_all(prefix + "\t")
		print(prefix + "First T2w")
		self.first_t2w.show_all(prefix + "\t")
		print(prefix + "Second T2w")
		self.second_t2w.show_all(prefix + "\t")
		print(prefix + "unwarp_dir: " + self.unwarp_dir)
		print(prefix + "se_unwarp_dir: " + self.se_unwarp_dir)
		print(prefix + "topup_config: " + self.topup_config)
		print(prefix + "t1w_template: " + self.t1w_template)
		print(prefix + "t2w_template: " + self.t2w_template)
		print(prefix + "t1w_template_brain: " + self.t1w_template_brain)
		print(prefix + "t2w_template_brain: " + self.t2w_template_brain)
		print(prefix + "template_mask: " + self.template_mask)
		print(prefix + "final_template_space: " + self.final_template_space)
		print(prefix + "structural_fs_assessor_ext: " + self.structural_fs_assessor_ext)
		print(prefix + "gradient_distortion_coeffs_file: " + self.gradient_distortion_coeffs_file)

	def validate(self):
		# Must have at least one T1w scan
		msg = "Must have at least one T1w scan"
		if (not validate.check_gte(self.T1w_count, 1, msg)): return False

		# If there are 2 T1w scans
		if self.T1w_count > 1:
			if self.fieldmap_type == FieldMapType.SiemensGradientEchoFieldMaps:
				# they must have the same magnitude fieldmap scan number
				msg = "Structural Parameter Validation: First and second T1w scans must have the same magnitude field map scan number value"
				if (not validate.check_equal(self.first_t1w.magfieldmap_scan_number, self.second_t1w.magfieldmap_scan_number, msg)): return False

				# they must have the same phase fieldmap scan number
				msg = "Structural Parameter Validation: First and second T1w scans must have the same phase field map scan number value"
				if (not validate.check_equal(self.first_t1w.phasefieldmap_scan_number, self.second_t1w.phasefieldmap_scan_number, msg)): return False
									  
			elif self.fieldmap_type == FieldMapType.SpinEchoFieldMaps:
				# they must have the same positive spin echo fieldmap number
				msg = "Structural Parameter Validation: First and second T1w scans must have the same positve spin echo field map scan number value"
				if (not validate.check_equal(self.first_t1w.pos_se_fieldmap_scan_number, self.second_t1w.pos_se_fieldmap_scan_number, msg)): return False

				# they must have the same negative spin echo fieldmap number
				msg = "Structural Parameter Validation: First and second T1w scans must have the same negative spin echo field map scan number value"
				if (not validate.check_equal(self.first_t1w.neg_se_fieldmap_scan_number, self.second_t1w.neg_se_fieldmap_scan_number, msg)): return False

			else:
				print("Structural Parameter Validation: Unrecognized field map type: " + self.fieldmap_type)
				return False

		# If there is only 1 T1w scan, duplicate it as the second T1w scan
		if self.T1w_count == 1:
			if self.is_first_t1w_set:
				self.second_t1w = self.first_t1w
			elif self.is_second_t1w_set:
				self.first_t1w = self.second_t1w
			else:
				print("Structural Parmeter Validation: T1w count = 1 but neither first or second indicates set")
				return False


		# Must have at least one T2w scan
		msg = "Must have at least one T2w scan"
		if (not validate.check_gte(self.T2w_count, 1, msg)): return False

		# If there are 2 T2w scans
		if self.T2w_count > 1:
			if self.fieldmap_type == FieldMapType.SiemensGradientEchoFieldMaps:
				# they must have the same magnitude fieldmap scan number
				msg = "Structural Parameter Validation: First and second T2w scans must have the same magnitude field map scan number value"
				if (not validate.check_equal(self.first_t2w.magfieldmap_scan_number, self.second_t2w.magfieldmap_scan_number, msg)): return False

				# they must have the same phase fieldmap scan number
				msg = "Structural Parameter Validation: First and second T2w scans must have the same phase field map scan number value"
				if (not validate.check_equal(self.first_t2w.phasefieldmap_scan_number, self.second_t2w.phasefieldmap_scan_number, msg)): return False

			elif self.fieldmap_type == FieldMapType.SpinEchoFieldMaps:
				# they must have the same positive spin echo fieldmap number
				msg = "Structural Parameter Validation: First and second T2w scans must have the same positive spin echo field map scan number value"
				if (not validate.check_equal(self.first_t2w.pos_se_fieldmap_scan_number, self.second_t2w.pos_se_fieldmap_scan_number, msg)): return False

				# they must have the same negative spin echo fieldmap number
				msg = "Structural Parameter Validation: First and second T2w scans must have the same negative spin echo field map scan number value"
				if (not validate.check_equal(self.first_t2w.neg_se_fieldmap_scan_number, self.second_t2w.neg_se_fieldmap_scan_number, msg)): return False

			else:
				print("Structural Parameter Validation: Unrecognized field map type: " + self.fieldmap_type)
				return False

		if self.fieldmap_type == FieldMapType.SiemensGradientEchoFieldMaps:

			# All the T1w and T2w scans that are set must have the same magnitude fieldmap
			msg = "Structural Parameter Validation: All T1w and T2w scans that are used must have the same magnitude field map scan number value"

			mag_fieldmap_scan_numbers = list()
			if self.is_first_t1w_set:
				mag_fieldmap_scan_numbers.append(self.first_t1w.magfieldmap_scan_number)
			if self.is_second_t1w_set:
				mag_fieldmap_scan_numbers.append(self.second_t1w.magfieldmap_scan_number)
			if self.is_first_t2w_set:
				mag_fieldmap_scan_numbers.append(self.first_t2w.magfieldmap_scan_number)
			if self.is_second_t2w_set:
				mag_fieldmap_scan_numbers.append(self.second_t2w.magfieldmap_scan_number)

			if (not validate.check_equal(len(set(mag_fieldmap_scan_numbers)), 1, msg)): return False
			
			# All the T1w and T2w scans that are set must have the same phase fieldmap
			msg = "Structural Parameter Validation: All T1w and T2w scans that are used must have the same phase field map scan number value"

			phase_fieldmap_scan_numbers = list()
			if self.is_first_t1w_set:
				phase_fieldmap_scan_numbers.append(self.first_t1w.phasefieldmap_scan_number)
			if self.is_second_t1w_set:
				phase_fieldmap_scan_numbers.append(self.second_t1w.phasefieldmap_scan_number)
			if self.is_first_t2w_set:
				phase_fieldmap_scan_numbers.append(self.first_t2w.phasefieldmap_scan_number)
			if self.is_second_t2w_set:
				phase_fieldmap_scan_numbers.append(self.second_t2w.phasefieldmap_scan_number)

			if (not validate.check_equal(len(set(phase_fieldmap_scan_numbers)), 1, msg)): return False

		elif self.fieldmap_type == FieldMapType.SpinEchoFieldMaps:

			# All the T1w and T2w scans that are set must have the same positive spin echo fieldmap
			msg = "Structural Parameter Validation: All T1w and T2w scans that are used must have the same positive spin echo field map scan number value"

			pos_se_fieldmap_scan_numbers = list()
			if self.is_first_t1w_set:
				pos_se_fieldmap_scan_numbers.append(self.first_t1w.pos_se_fieldmap_scan_number)
			if self.is_second_t1w_set:
				pos_se_fieldmap_scan_numbers.append(self.second_t1w.pos_se_fieldmap_scan_number)
			if self.is_first_t2w_set:
				pos_se_fieldmap_scan_numbers.append(self.first_t2w.pos_se_fieldmap_scan_number)
			if self.is_second_t2w_set:
				pos_se_fieldmap_scan_numbers.append(self.second_t2w.pos_se_fieldmap_scan_number)
			
			# Temporarily suspend this validation
			# if (not validate.check_equal(len(set(pos_se_fieldmap_scan_numbers)), 1, msg)): return False
		
			# All the T1w and T2w scans that are set must have the same negative spin echo fieldmap
			msg = "Structural Parameter Validation: All T1w and T2w scans that are used must have the same negative spin echo field map scan number value"

			neg_se_fieldmap_scan_numbers = list()
			if self.is_first_t1w_set:
				neg_se_fieldmap_scan_numbers.append(self.first_t1w.neg_se_fieldmap_scan_number)
			if self.is_second_t1w_set:
				neg_se_fieldmap_scan_numbers.append(self.second_t1w.neg_se_fieldmap_scan_number)
			if self.is_first_t2w_set:
				neg_se_fieldmap_scan_numbers.append(self.first_t2w.neg_se_fieldmap_scan_number)
			if self.is_second_t2w_set:
				neg_se_fieldmap_scan_numbers.append(self.second_t2w.neg_se_fieldmap_scan_number)
			
			# Temporarily suspend the validation
			#if (not validate.check_equal(len(set(neg_se_fieldmap_scan_numbers)), 1, msg)): return False
		
			# The positive and negative spin echo field maps must have the same dwell time
			msg = "Structural Parameter Validation: The positive and negative spin echo field maps must have the same dwell time"

			dwell_times = list()
			if self.is_first_t1w_set:
				dwell_times.append(self.first_t1w.pos_se_fieldmap_dwell_time)
				dwell_times.append(self.first_t1w.neg_se_fieldmap_dwell_time)
			if self.is_second_t1w_set:
				dwell_times.append(self.second_t1w.pos_se_fieldmap_dwell_time)
				dwell_times.append(self.second_t1w.neg_se_fieldmap_dwell_time) 
			if self.is_first_t2w_set:
				dwell_times.append(self.first_t2w.pos_se_fieldmap_dwell_time)
				dwell_times.append(self.first_t2w.neg_se_fieldmap_dwell_time)
			if self.is_second_t2w_set:
				dwell_times.append(self.second_t2w.pos_se_fieldmap_dwell_time)
				dwell_times.append(self.second_t2w.neg_se_fieldmap_dwell_time) 

			if (not validate.check_equal(len(set(dwell_times)), 1, msg)): return False

		else:
			print("Structural Parameter Validation: Unrecognized field map type: " + self.fieldmap_type)
			return False
		

		# If there is only 1 T2w scan, duplicate it as the second T2w scan
		if self.T2w_count == 1:
			if self.is_first_t2w_set:
				self.second_t2w = self.first_t2w
			elif self.is_second_t2w_set:
				self.first_t2w = self.second_t2w
			else:
				print("Structural Parmeter Validation: T2w count = 1 but neither first or second indicates set")
				return False

		# All the T1w and T2w scans that are set must have the same delta TE value
		msg = "Structural Parameter Validation: All T1w and T2w scans that are used must have the same delta TE value"

		delta_te_values = list()
		if self.is_first_t1w_set:
			delta_te_values.append(self.first_t1w.magfieldmap_delta_te)
		if self.is_second_t1w_set:
			delta_te_values.append(self.second_t1w.magfieldmap_delta_te)
		if self.is_first_t2w_set:
			delta_te_values.append(self.first_t2w.magfieldmap_delta_te)
		if self.is_second_t2w_set:
			delta_te_values.append(self.second_t2w.magfieldmap_delta_te)

		if (not validate.check_equal(len(set(delta_te_values)), 1, msg)): return False

		# All tests passed
		return True
