#!/bin/bash

           
source ${SCRIPTS_HOME}/groovy_setup.sh

export JAVA_OPTS="-Xmx1024m -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC"

export PackagerHome=/home/HCPpipeline/PACKAGING/download-packager
export ResourcePath=$PackagerHome/resources
export CLASSPATH=$PackagerHome:$PackagerHome/src/main/resources:$PackagerHome/src/main/groovy:$PackagerHome/target/dependency/fast-md5-2.7.1.jar:$PackagerHome/target/dependency/commons-cli-1.2.jar:$PackagerHome/target/dependency/hamcrest-core-1.1.jar:$CLASSPATH
export SCRIPTDIR=/home/HCPpipeline/PACKAGING/download-packager
           
PROJECT_CDB=$1 #PROJECT ID
SUBJECT=$2 #Subject ID
ARCHIVE=$3
PACKET=$4 #Structural or Functional or Diffusion
OUT_DIR=$5 #Location where the files would be copied to
TEMP_DIR=$6 #Temporary scratch space

MIGRATE_PARAM=""
MIGRATE_OPTION=""

ARGS=7

if [ $# == $ARGS ]; then
    MIGRATE_PARAM=$7
fi

if [ -n "$MIGRATE_PARAM" ] ; then
 MIGRATE_OPTION=" -m $MIGRATE_PARAM "
fi

echo groovy -cp $CLASSPATH $PackagerHome/download-packager.groovy -a $ARCHIVE -d $PACKET ${MIGRATE_OPTION} -w $TEMP_DIR -o $OUT_DIR -r $ResourcePath -s $SUBJECT --no-zip --copy

groovy -cp $CLASSPATH $PackagerHome/download-packager.groovy -a $ARCHIVE -d $PACKET -w $TEMP_DIR -o $OUT_DIR -r $ResourcePath -s $SUBJECT --no-zip --copy

exit 0

