import sys
import xml.etree.ElementTree as ET
from xnat_pipeline_parameters import XNATPipelineParameters

from structural_parameters import StructuralParameters
from functional_parameters import FunctionalParameters
from diffusion_parameters import DiffusionParameters
from task_analysis_parameters import TaskAnalysisParameters
from ica_fix_parameters import IcaFixParameters
from mpp_parameters_factory import MppParametersFactory

MailHostStr = 'mail.nrg.wustl.edu'
MailHost = '-parameter mailhost=%s ' % (MailHostStr)

DataTypeStr = 'xnat:mrSessionData'
DataType = '-dataType %s ' % (DataTypeStr)

NotifyUserStr = 'hilemanm@mir.wustl.edu'
NotifyUser = ' -notify %s ' % (NotifyUserStr)
UserEmail = ' -parameter useremail=%s ' % (NotifyUserStr)

NotifyAdminStr = 'db-admin@humanconnectome.org'
NotifyAdmin = ' -notify %s ' % (NotifyAdminStr)
AdminEmail = ' -parameter adminemail=%s' % (NotifyAdminStr)

UserFullNameStr = 'MPPUser'
UserFullName = ' -parameter userfullname=%s ' % (UserFullNameStr)

XnatServerStr = 'ConnectomeDB'
XnatServer = ' -parameter xnatserver=%s ' % (XnatServerStr)

class MppParameters(object):
    """
    Class for containing and managing parameters for the Minimal PreProcessing (MPP) family of pipelines.
    """

    def __init__(self, pipeline_project):
        """Create a ojbect for holding and managing MPP parameters

        Args:
          pipeline_project (str): The pipeline project for which we will be managing parameters

        """
        super(MppParameters, self).__init__()
        self._debug = False
        self._subject = ''
        self._session = ''
        self._xnat_session_id = ''
        self._project = ''

        
        parameters_factory = MppParametersFactory(pipeline_project, "", "")

        self._launch_structural = False
        self._structural_parameters = parameters_factory.get_structural_parameters()

        self._launch_functional = False
        self._functional_parameters = parameters_factory.get_functional_parameters()

        self._launch_diffusion = False
        self._diffusion_parameters = parameters_factory.get_diffusion_parameters()

        self._launch_task_analysis = False
        self._task_analysis_parameters = parameters_factory.get_task_analysis_parameters()

        self._launch_ica_fix = False
        self._ica_fix_parameters = IcaFixParameters()

        self._launch_mpp = False

    # property: debug 

    def get_debug(self):
        return self._debug

    def set_debug(self, new_value):
        if self._debug or new_value: print "DEBUG: MppParameters: Debug Status: " + str(new_value)
        self._debug = new_value

    debug=property(get_debug, set_debug, doc="Indication of whether or not debugging output is to be produced")

    # property: subject

    def get_subject(self):
        return self._subject

    def set_subject(self, new_subject):
        self._subject = new_subject
        self._debug_print("subject: " + self._subject)
        
    subject=property(get_subject, set_subject)

    # property: session

    def get_session(self):
        return self._session

    def set_session(self, new_session):
        self._session = new_session
        self._diffusion_parameters.session = new_session
        self._debug_print("session: " + self._session)

    session=property(get_session, set_session)

    # property: xnat_session_id

    def get_xnat_session_id(self):
        return self._xnat_session_id

    def set_xnat_session_id(self, new_xnat_session_id):
        self._xnat_session_id = new_xnat_session_id
        self._debug_print("xnat_session_id: " + self._xnat_session_id)

    xnat_session_id=property(get_xnat_session_id, set_xnat_session_id)

    # property: project

    def get_project(self):
        return self._project

    def set_project(self, new_project):
        self._project = new_project
        self._debug_print("project: " + self._project)

    project=property(get_project, set_project)

    # property: launch_structural

    def get_launch_structural(self):
        return self._launch_structural

    def set_launch_structural(self, new_tf_value):
        self._launch_structural = new_tf_value
        self._debug_print("launch_structural: " + str(self._launch_structural))

    launch_structural=property(get_launch_structural, set_launch_structural)

    # property: fieldmap_type_str (for structural)

    def get_fieldmap_type_str(self):
        return self._structural_parameters.get_fieldmap_type_str()

    def set_fieldmap_type_str(self, new_fieldmap_type_str):
        self._structural_parameters.fieldmap_type = new_fieldmap_type_str
        self._debug_print("fieldmap_type: " + str(self._structural_parameters.fieldmap_type))

    fieldmap_type_str=property(get_fieldmap_type_str, set_fieldmap_type_str)

    # First T1w (for structural)

    def set_first_t1w(self, new_file_name, new_scan_number, new_series_desc, new_sample_spacing):
        self._structural_parameters.first_t1w.file_name = new_file_name
        self._debug_print("first_t1w.filename: " + self._structural_parameters.first_t1w.file_name)

        self._structural_parameters.first_t1w.scan_number = new_scan_number
        self._debug_print("first_t1w.scan_number: " + self._structural_parameters.first_t1w.scan_number)

        self._structural_parameters.first_t1w.series_desc = new_series_desc
        self._debug_print("first_t1w.series_desc: " + self._structural_parameters.first_t1w.series_desc)

        self._structural_parameters.first_t1w.sample_spacing = new_sample_spacing
        self._debug_print("first_t1w.sample_spacing: " + str(self._structural_parameters.first_t1w.sample_spacing))

    def set_first_t1w_magfieldmap(self, new_file_name, new_scan_number, new_delta_te):
        self._structural_parameters.first_t1w.magfieldmap_file_name = new_file_name
        self._debug_print("first_t1w.magfieldmap_file_name: " + self._structural_parameters.first_t1w.magfieldmap_file_name)

        self._structural_parameters.first_t1w.magfieldmap_scan_number = new_scan_number
        self._debug_print("first_t1w.magfieldmap_scan_number: " + self._structural_parameters.first_t1w.magfieldmap_scan_number)

        self._structural_parameters.first_t1w.magfieldmap_delta_te = new_delta_te
        self._debug_print("first_t1w.magfieldmap_delta_te: " + str(self._structural_parameters.first_t1w.magfieldmap_delta_te))

    def set_first_t1w_phasefieldmap(self, new_file_name, new_scan_number):
        self._structural_parameters.first_t1w.phasefieldmap_file_name = new_file_name
        self._debug_print("first_t1w.phasefieldmap_file_name: " + self._structural_parameters.first_t1w.phasefieldmap_file_name)

        self._structural_parameters.first_t1w.phasefieldmap_scan_number = new_scan_number
        self._debug_print("first_t1w.phasefieldmap_scan_number: " + self._structural_parameters.first_t1w.phasefieldmap_scan_number)

    def set_first_t1w_positive_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.first_t1w.pos_se_fieldmap_file_name = new_file_name
        self._debug_print("first_t1w.pos_se_fieldmap_file_name: " + self._structural_parameters.first_t1w.pos_se_fieldmap_file_name)

        self._structural_parameters.first_t1w.pos_se_fieldmap_scan_number = new_scan_number
        self._debug_print("first_t1w.pos_se_fieldmap_scan_number: " + self._structural_parameters.first_t1w.pos_se_fieldmap_scan_number)

        self._structural_parameters.first_t1w.pos_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("first_t1w.pos_se_fieldmap_dwell_time: " + str(self._structural_parameters.first_t1w.pos_se_fieldmap_dwell_time))

    def set_first_t1w_negative_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.first_t1w.neg_se_fieldmap_file_name = new_file_name
        self._debug_print("first_t1w.neg_se_fieldmap_file_name: " + self._structural_parameters.first_t1w.neg_se_fieldmap_file_name)

        self._structural_parameters.first_t1w.neg_se_fieldmap_scan_number = new_scan_number
        self._debug_print("first_t1w.neg_se_fieldmap_scan_number: " + self._structural_parameters.first_t1w.neg_se_fieldmap_scan_number)

        self._structural_parameters.first_t1w.neg_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("first_t1w.neg_se_fieldmap_dwell_time: " + str(self._structural_parameters.first_t1w.neg_se_fieldmap_dwell_time))

    # Second T1w (for structural)

    def set_second_t1w(self, new_file_name, new_scan_number, new_series_desc, new_sample_spacing):
        self._structural_parameters.second_t1w.file_name = new_file_name
        self._debug_print("second_t1w.filename: " + self._structural_parameters.second_t1w.file_name)

        self._structural_parameters.second_t1w.scan_number = new_scan_number
        self._debug_print("second_t1w.scan_number: " + self._structural_parameters.second_t1w.scan_number)

        self._structural_parameters.second_t1w.series_desc = new_series_desc
        self._debug_print("second_t1w.series_desc: " + self._structural_parameters.second_t1w.series_desc)

        self._structural_parameters.second_t1w.sample_spacing = new_sample_spacing
        self._debug_print("second_t1w.sample_spacing: " + str(self._structural_parameters.second_t1w.sample_spacing))

    def set_second_t1w_magfieldmap(self, new_file_name, new_scan_number, new_delta_te):
        self._structural_parameters.second_t1w.magfieldmap_file_name = new_file_name
        self._debug_print("second_t1w.magfieldmap_file_name: " + self._structural_parameters.second_t1w.magfieldmap_file_name)

        self._structural_parameters.second_t1w.magfieldmap_scan_number = new_scan_number
        self._debug_print("second_t1w.magfieldmap_scan_number: " + self._structural_parameters.second_t1w.magfieldmap_scan_number)

        self._structural_parameters.second_t1w.magfieldmap_delta_te = new_delta_te
        self._debug_print("second_t1w.magfieldmap_delta_te: " + str(self._structural_parameters.second_t1w.magfieldmap_delta_te))

    def set_second_t1w_phasefieldmap(self, new_file_name, new_scan_number):
        self._structural_parameters.second_t1w.phasefieldmap_file_name = new_file_name
        self._debug_print("second_t1w.phasefieldmap_file_name: " + self._structural_parameters.second_t1w.phasefieldmap_file_name)

        self._structural_parameters.second_t1w.phasefieldmap_scan_number = new_scan_number
        self._debug_print("second_t1w.phasefieldmap_scan_number: " + self._structural_parameters.second_t1w.phasefieldmap_scan_number)

    def set_second_t1w_positive_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.second_t1w.pos_se_fieldmap_file_name = new_file_name
        self._debug_print("second_t1w.pos_se_fieldmap_file_name: " + self._structural_parameters.second_t1w.pos_se_fieldmap_file_name)

        self._structural_parameters.second_t1w.pos_se_fieldmap_scan_number = new_scan_number
        self._debug_print("second_t1w.pos_se_fieldmap_scan_number: " + self._structural_parameters.second_t1w.pos_se_fieldmap_scan_number)

        self._structural_parameters.second_t1w.pos_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("second_t1w.pos_se_fieldmap_dwell_time: " + str(self._structural_parameters.second_t1w.pos_se_fieldmap_dwell_time))

    def set_second_t1w_negative_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.second_t1w.neg_se_fieldmap_file_name = new_file_name
        self._debug_print("second_t1w.neg_se_fieldmap_file_name: " + self._structural_parameters.second_t1w.neg_se_fieldmap_file_name)

        self._structural_parameters.second_t1w.neg_se_fieldmap_scan_number = new_scan_number
        self._debug_print("second_t1w.neg_se_fieldmap_scan_number: " + self._structural_parameters.second_t1w.neg_se_fieldmap_scan_number)

        self._structural_parameters.second_t1w.neg_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("second_t1w.neg_se_fieldmap_dwell_time: " + str(self._structural_parameters.second_t1w.neg_se_fieldmap_dwell_time))

    # First T2w (for structural)

    def set_first_t2w(self, new_file_name, new_scan_number, new_series_desc, new_sample_spacing):
        self._structural_parameters.first_t2w.file_name = new_file_name
        self._debug_print("first_t2w.filename: " + self._structural_parameters.first_t2w.file_name)

        self._structural_parameters.first_t2w.scan_number = new_scan_number
        self._debug_print("first_t2w.scan_number: " + self._structural_parameters.first_t2w.scan_number)

        self._structural_parameters.first_t2w.series_desc = new_series_desc
        self._debug_print("first_t2w.series_desc: " + self._structural_parameters.first_t2w.series_desc)

        self._structural_parameters.first_t2w.sample_spacing = new_sample_spacing
        self._debug_print("first_t2w.sample_spacing: " + str(self._structural_parameters.first_t2w.sample_spacing))

    def set_first_t2w_magfieldmap(self, new_file_name, new_scan_number, new_delta_te):
        self._structural_parameters.first_t2w.magfieldmap_file_name = new_file_name
        self._debug_print("first_t2w.magfieldmap_file_name: " + self._structural_parameters.first_t2w.magfieldmap_file_name)

        self._structural_parameters.first_t2w.magfieldmap_scan_number = new_scan_number
        self._debug_print("first_t2w.magfieldmap_scan_number: " + self._structural_parameters.first_t2w.magfieldmap_scan_number)

        self._structural_parameters.first_t2w.magfieldmap_delta_te = new_delta_te
        self._debug_print("first_t2w.magfieldmap_delta_te: " + str(self._structural_parameters.first_t2w.magfieldmap_delta_te))

    def set_first_t2w_phasefieldmap(self, new_file_name, new_scan_number):
        self._structural_parameters.first_t2w.phasefieldmap_file_name = new_file_name
        self._debug_print("first_t2w.phasefieldmap_file_name: " + self._structural_parameters.first_t2w.phasefieldmap_file_name)

        self._structural_parameters.first_t2w.phasefieldmap_scan_number = new_scan_number
        self._debug_print("first_t2w.phasefieldmap_scan_number: " + self._structural_parameters.first_t2w.phasefieldmap_scan_number)

    def set_first_t2w_positive_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.first_t2w.pos_se_fieldmap_file_name = new_file_name
        self._debug_print("first_t2w.pos_se_fieldmap_file_name: " + self._structural_parameters.first_t2w.pos_se_fieldmap_file_name)

        self._structural_parameters.first_t2w.pos_se_fieldmap_scan_number = new_scan_number
        self._debug_print("first_t2w.pos_se_fieldmap_scan_number: " + self._structural_parameters.first_t2w.pos_se_fieldmap_scan_number)

        self._structural_parameters.first_t2w.pos_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("first_t2w.pos_se_fieldmap_dwell_time: " + str(self._structural_parameters.first_t2w.pos_se_fieldmap_dwell_time))

    def set_first_t2w_negative_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.first_t2w.neg_se_fieldmap_file_name = new_file_name
        self._debug_print("first_t2w.neg_se_fieldmap_file_name: " + self._structural_parameters.first_t2w.neg_se_fieldmap_file_name)

        self._structural_parameters.first_t2w.neg_se_fieldmap_scan_number = new_scan_number
        self._debug_print("first_t2w.neg_se_fieldmap_scan_number: " + self._structural_parameters.first_t2w.neg_se_fieldmap_scan_number)

        self._structural_parameters.first_t2w.neg_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("first_t2w.neg_se_fieldmap_dwell_time: " + str(self._structural_parameters.first_t2w.neg_se_fieldmap_dwell_time))

    # Second T2w (for structural)

    def set_second_t2w(self, new_file_name, new_scan_number, new_series_desc, new_sample_spacing):
        self._structural_parameters.second_t2w.file_name = new_file_name
        self._debug_print("second_t2w.filename: " + self._structural_parameters.second_t2w.file_name)

        self._structural_parameters.second_t2w.scan_number = new_scan_number
        self._debug_print("second_t2w.scan_number: " + self._structural_parameters.second_t2w.scan_number)

        self._structural_parameters.second_t2w.series_desc = new_series_desc
        self._debug_print("second_t2w.series_desc: " + self._structural_parameters.second_t2w.series_desc)

        self._structural_parameters.second_t2w.sample_spacing = new_sample_spacing
        self._debug_print("second_t2w.sample_spacing: " + str(self._structural_parameters.second_t2w.sample_spacing))

    def set_second_t2w_magfieldmap(self, new_file_name, new_scan_number, new_delta_te):
        self._structural_parameters.second_t2w.magfieldmap_file_name = new_file_name
        self._debug_print("second_t2w.magfieldmap_file_name: " + self._structural_parameters.second_t2w.magfieldmap_file_name)

        self._structural_parameters.second_t2w.magfieldmap_scan_number = new_scan_number
        self._debug_print("second_t2w.magfieldmap_scan_number: " + self._structural_parameters.second_t2w.magfieldmap_scan_number)

        self._structural_parameters.second_t2w.magfieldmap_delta_te = new_delta_te
        self._debug_print("second_t2w.magfieldmap_delta_te: " + str(self._structural_parameters.second_t2w.magfieldmap_delta_te))

    def set_second_t2w_phasefieldmap(self, new_file_name, new_scan_number):
        self._structural_parameters.second_t2w.phasefieldmap_file_name = new_file_name
        self._debug_print("second_t2w.phasefieldmap_file_name: " + self._structural_parameters.second_t2w.phasefieldmap_file_name)

        self._structural_parameters.second_t2w.phasefieldmap_scan_number = new_scan_number
        self._debug_print("second_t2w.phasefieldmap_scan_number: " + self._structural_parameters.second_t2w.phasefieldmap_scan_number)

    def set_second_t2w_positive_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.second_t2w.pos_se_fieldmap_file_name = new_file_name
        self._debug_print("second_t2w.pos_se_fieldmap_file_name: " + self._structural_parameters.second_t2w.pos_se_fieldmap_file_name)

        self._structural_parameters.second_t2w.pos_se_fieldmap_scan_number = new_scan_number
        self._debug_print("second_t2w.pos_se_fieldmap_scan_number: " + self._structural_parameters.second_t2w.pos_se_fieldmap_scan_number)

        self._structural_parameters.second_t2w.pos_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("second_t2w.pos_se_fieldmap_dwell_time: " + str(self._structural_parameters.second_t2w.pos_se_fieldmap_dwell_time))

    def set_second_t2w_negative_spin_echo_fieldmap(self, new_file_name, new_scan_number, new_dwell_time):
        self._structural_parameters.second_t2w.neg_se_fieldmap_file_name = new_file_name
        self._debug_print("second_t2w.neg_se_fieldmap_file_name: " + self._structural_parameters.second_t2w.neg_se_fieldmap_file_name)

        self._structural_parameters.second_t2w.neg_se_fieldmap_scan_number = new_scan_number
        self._debug_print("second_t2w.neg_se_fieldmap_scan_number: " + self._structural_parameters.second_t2w.neg_se_fieldmap_scan_number)

        self._structural_parameters.second_t2w.neg_se_fieldmap_dwell_time = new_dwell_time
        self._debug_print("second_t2w.neg_se_fieldmap_dwell_time: " + str(self._structural_parameters.second_t2w.neg_se_fieldmap_dwell_time))

    # derived property: magfieldmap_scan_number

    def get_magfieldmap_scan_number(self):
        if self._structural_parameters._is_first_t1w_set():
            return self._structural_parameters.first_t1w.magfieldmap_scan_number
        elif self._structural_parameters._is_second_t1w_set():
            return self._structural_parameters.second_t1w.magfieldmap_scan_number
        elif self._structural_parameters._is_first_t2w_set():
            return self._structural_parameters.first_t2w.magfieldmap_scan_number
        elif self._structural_parameters._is_second_t2w_set():
            return self._structural_parameters.second_t2w.magfieldmap_scan_number
        else:
            print("Cannot Derive Value: magnitude fieldmap scan number")
            sys.exit(1)

    magfieldmap_scan_number=property(get_magfieldmap_scan_number)

    # derived property: phasefieldmap_scan_number

    def get_phasefieldmap_scan_number(self):
        if self._structural_parameters._is_first_t1w_set():
            return self._structural_parameters.first_t1w.phasefieldmap_scan_number
        elif self._structural_parameters._is_second_t1w_set():
            return self._structural_parameters.second_t1w.phasefieldmap_scan_number
        if self._structural_parameters._is_first_t2w_set():
            return self._structural_parameters.first_t2w.phasefieldmap_scan_number
        elif self._structural_parameters._is_second_t2w_set():
            return self._structural_parameters.second_t2w.phasefieldmap_scan_number
        else:
            print("Cannot Derive Value: phase fieldmap scan number")
            sys.exit(1)

    phasefieldmap_scan_number=property(get_phasefieldmap_scan_number)

    # derived property: delta_te

    def get_delta_te(self):
        if self._structural_parameters._is_first_t1w_set():
            return self._structural_parameters.first_t1w.magfieldmap_delta_te
        elif self._structural_parameters._is_second_t1w_set():
            return self._structural_parameters.second_t1w.magfieldmap_delta_te
        if self._structural_parameters._is_first_t2w_set():
            return self._structural_parameters.first_t2w.magfieldmap_delta_te
        elif self._structural_parameters._is_second_t2w_set():
            return self._structural_parameters.second_t2w.magfieldmap_delta_te
        else:
            print("Cannot Derive Value: delta TE")
            sys.exit(1)

    delta_te=property(get_delta_te)

    # derived property: dwell_time

    def get_dwell_time(self):
        if self._structural_parameters._is_first_t1w_set():
            return self._structural_parameters.first_t1w.pos_se_fieldmap_dwell_time 
        elif self._structural_parameters._is_second_t1w_set():
            return self._structural_parameters.second_t1w.pos_se_fieldmap_dwell_time
        if self._structural_parameters._is_first_t2w_set():
            return self._structural_parameters.first_t2w.pos_se_fieldmap_dwell_time
        elif self._structural_parameters._is_second_t2w_set():
            return self._structural_parameters.second_t2w.pos_se_fieldmap_dwell_time
        else:
            print("Cannot Derive Value: dwell time")
            sys.exit(1)

    dwell_time=property(get_dwell_time)

    # property: launch_functional

    def get_launch_functional(self):
        return self._launch_functional

    def set_launch_functional(self, new_tf_value):
        self._launch_functional = new_tf_value
        self._debug_print("launch_functional: " + str(self._launch_functional))

    launch_functional=property(get_launch_functional, set_launch_functional)

    # add a functional scan

    def add_functional_scan(self, scan_number, scan_type, dwell_time, pe_direction):
        self._functional_parameters.add_scan(scan_number, scan_type, dwell_time, pe_direction)

    # property: launch_diffusion

    def get_launch_diffusion(self):
        return self._launch_diffusion

    def set_launch_diffusion(self, new_tf_value):
        self._launch_diffusion = new_tf_value
        self._debug_print("launch_diffusion: " + str(self._launch_diffusion))

    launch_diffusion=property(get_launch_diffusion, set_launch_diffusion)

    def add_diffusion_scan(self, scan_number, direction, scan_dir_number, echo_spacing):
        self._diffusion_parameters.add_scan(scan_number, direction, scan_dir_number, echo_spacing)

    # property: launch_task_analysis

    def get_launch_task_analysis(self):
        return self._launch_task_analysis

    def set_launch_task_analysis(self, new_tf_value):
        self._launch_task_analysis = new_tf_value
        self._debug_print("launch_task_analysis: " + str(self._launch_task_analysis))

    launch_task_analysis=property(get_launch_task_analysis, set_launch_task_analysis)

    def add_task_scan(self, scan_resource_name):
        self._task_analysis_parameters.add_scan(scan_resource_name)

    # property: launch_ica_fix
    def get_launch_ica_fix(self):
        return self._launch_ica_fix

    def set_launch_ica_fix(self, new_tf_value):
        self._launch_ica_fix = new_tf_value
        self._debug_print("launch_ica_fix: " + str(self._launch_ica_fix))

    launch_ica_fix=property(get_launch_ica_fix, set_launch_ica_fix)

    def add_icafix_scan(self, scan_resource_name):
        self._ica_fix_parameters.add_scan(scan_resource_name)

    # property: launch_mpp
    def get_launch_mpp(self):
        return self._launch_mpp

    def set_launch_mpp(self, new_tf_value):
        self._launch_mpp = new_tf_value
        self._debug_print("launch_mpp: " + str(self._launch_mpp))
        
    launch_mpp=property(get_launch_mpp, set_launch_mpp)

    # utility methods 

    def _debug_print(self, val ):
        if self.debug: print "DEBUG: MppParameters: " + str(val)

    def validate(self):
        if self.launch_structural and not self._structural_parameters.validate():
            print("MPP Parameter Validation: Structural Parameters must pass validation checks")
            return False

        if self.launch_functional and not self._functional_parameters.validate():
            print("MPP Parameter Validation: Functional Parameters must pass validation checks")
            return False

        if self.launch_diffusion and not self._diffusion_parameters.validate():
            print("MPP Parameter Validation: Diffusion Parameters must pass validation checks")
            return False

        if self.launch_task_analysis and not self._task_analysis_parameters.validate():
            print("MPP Parameter Validation: Task Analysis Parameters must pass validation checks")
            return False

        if self.launch_ica_fix and not self._ica_fix_parameters.validate():
            print("MPP Parameter Validation: ICA FIX Parameters must pass validation checks")
            return False

        # All tests passed
        return True

    def show_all(self):
        prefix = "\t"
        
        print("\nAll MPP Processing Parameters")
        print(prefix + "debug: " + str(self.debug))
        print(prefix + "subject: " + self.subject)
        print(prefix + "session: " + self.session)
        print(prefix + "xnat_session_id: " + self.xnat_session_id)
        print(prefix + "project: " + self.project)
        
        print(prefix + "launch_structural: " + str(self.launch_structural))
        print(prefix + "Structural Preprocessing Parameters")
        self._structural_parameters.show_all(prefix + "\t")

        print(prefix + "launch_functional: " + str(self.launch_functional))
        print(prefix + "Functional Preprocessing Parameters")
        self._functional_parameters.show_all(prefix + "\t")

        print(prefix + "launch_diffusion: " + str(self.launch_diffusion))
        print(prefix + "Diffusion Preprocessing Parameters")
        self._diffusion_parameters.show_all(prefix + "\t")
        
        print(prefix + "launch_task_analysis: " + str(self.launch_task_analysis))
        print(prefix + "Task fMRI Analysis Parameters")
        self._task_analysis_parameters.show_all(prefix + "\t")

        print(prefix + "launch_ica_fix: " + str(self.launch_ica_fix))
        print(prefix + "ICA FIX Parameters")
        self._ica_fix_parameters.show_all(prefix + "\t")
        
        print(prefix + "launch_mpp: " + str(self.launch_mpp))

    def save_params_to_xml_file(self, pathToParamsXmlFile):
        KEY_SUBJECT = 'subject'
        KEY_SESSION_ID = 'sessionId'
        KEY_XNAT_ID = 'xnat_id'

        KEY_LAUNCH_STRUCTURAL = 'launchStructural'
        KEY_FIELDMAP_TYPE = 'structural_fieldmap_type'
        KEY_AVGRDMETHOD = 'structural_Avgrdcmethod'

        # Keys for first T1w

        KEY_FIRST_T1W_FILE_NAME = 'structural_t1name_1'
        KEY_FIRST_T1W_SCAN_NUMBER = 'structural_t1scanid_1'
        KEY_FIRST_T1W_SERIES_DESC = 'structural_t1seriesdesc_1'
        
        KEY_FIRST_T1W_MAGFIELDMAP_FILE_NAME = 'structural_t1magscanname_1'
        KEY_FIRST_T1W_MAGFIELDMAP_SCAN_NUMBER = 'structural_t1magscanid_1'
        KEY_FIRST_T1W_MAGFIELDMAP_DELTA_TE = 'structural_t1magscan_te_1'
        
        KEY_FIRST_T1W_PHASEFIELDMAP_FILE_NAME = 'structural_t1phascanname_1'
        KEY_FIRST_T1W_PHASEFIELDMAP_SCAN_NUMBER = 'structural_t1phascanid_1'

        KEY_FIRST_T1W_POS_SE_FIELDMAP_FILE_NAME = 'structural_t1_pos_se_fieldmap_1'
        KEY_FIRST_T1W_POS_SE_FIELDMAP_SCAN_NUMBER = 'structural_t1_pos_se_scanid_1'
        KEY_FIRST_T1W_POS_SE_FIELDMAP_DWELL_TIME = 'structural_t1_pos_se_dwelltime_1'

        KEY_FIRST_T1W_NEG_SE_FIELDMAP_FILE_NAME = 'structural_t1_neg_se_fieldmap_1'
        KEY_FIRST_T1W_NEG_SE_FIELDMAP_SCAN_NUMBER = 'structural_t1_neg_se_scanid_1'
        KEY_FIRST_T1W_NEG_SE_FIELDMAP_DWELL_TIME = 'structural_t1_neg_se_dwelltime_1'

        # Keys for second T1w

        KEY_SECOND_T1W_FILE_NAME = 'structural_t1name_2'
        KEY_SECOND_T1W_SCAN_NUMBER = 'structural_t1scanid_2'
        KEY_SECOND_T1W_SERIES_DESC = 'structural_t1seriesdesc_2'
        
        KEY_SECOND_T1W_MAGFIELDMAP_FILE_NAME = 'structural_t1magscanname_2'
        KEY_SECOND_T1W_MAGFIELDMAP_SCAN_NUMBER = 'structural_t1magscanid_2'
        KEY_SECOND_T1W_MAGFIELDMAP_DELTA_TE = 'structural_t1magscan_te_2'
        
        KEY_SECOND_T1W_PHASEFIELDMAP_FILE_NAME = 'structural_t1phascanname_2'
        KEY_SECOND_T1W_PHASEFIELDMAP_SCAN_NUMBER = 'structural_t1phascanid_2'
        
        KEY_SECOND_T1W_POS_SE_FIELDMAP_FILE_NAME = 'structural_t1_pos_se_fieldmap_2'
        KEY_SECOND_T1W_POS_SE_FIELDMAP_SCAN_NUMBER = 'structural_t1_pos_se_scanid_2'
        KEY_SECOND_T1W_POS_SE_FIELDMAP_DWELL_TIME = 'structural_t1_pos_se_dwelltime_2'
        
        KEY_SECOND_T1W_NEG_SE_FIELDMAP_FILE_NAME = 'structural_t1_neg_se_fieldmap_2'
        KEY_SECOND_T1W_NEG_SE_FIELDMAP_SCAN_NUMBER = 'structural_t1_neg_se_scanid_2'
        KEY_SECOND_T1W_NEG_SE_FIELDMAP_DWELL_TIME = 'structural_t1_neg_se_dwelltime_2'

        # Keys for first T2w

        KEY_FIRST_T2W_FILE_NAME = 'structural_t2name_1'
        KEY_FIRST_T2W_SCAN_NUMBER = 'structural_t2scanid_1'
        KEY_FIRST_T2W_SERIES_DESC = 'structural_t2seriesdesc_1'
        
        KEY_FIRST_T2W_MAGFIELDMAP_FILE_NAME = 'structural_t2magscanname_1'
        KEY_FIRST_T2W_MAGFIELDMAP_SCAN_NUMBER = 'structural_t2magscanid_1'
        KEY_FIRST_T2W_MAGFIELDMAP_DELTA_TE = 'structural_t2magscan_te_1'
        
        KEY_FIRST_T2W_PHASEFIELDMAP_FILE_NAME = 'structural_t2phascanname_1'
        KEY_FIRST_T2W_PHASEFIELDMAP_SCAN_NUMBER = 'structural_t2phascanid_1'
        
        KEY_FIRST_T2W_POS_SE_FIELDMAP_FILE_NAME = 'structural_t2_pos_se_fieldmap_1'
        KEY_FIRST_T2W_POS_SE_FIELDMAP_SCAN_NUMBER = 'structural_t2_pos_se_scanid_1'
        KEY_FIRST_T2W_POS_SE_FIELDMAP_DWELL_TIME = 'structural_t2_pos_se_dwelltime_1'
        
        KEY_FIRST_T2W_NEG_SE_FIELDMAP_FILE_NAME = 'structural_t2_neg_se_fieldmap_1'
        KEY_FIRST_T2W_NEG_SE_FIELDMAP_SCAN_NUMBER = 'structural_t2_neg_se_scanid_1'
        KEY_FIRST_T2W_NEG_SE_FIELDMAP_DWELL_TIME = 'structural_t2_neg_se_dwelltime_1'
        
        # Keys for second T2w

        KEY_SECOND_T2W_FILE_NAME = 'structural_t2name_2'
        KEY_SECOND_T2W_SCAN_NUMBER = 'structural_t2scanid_2'
        KEY_SECOND_T2W_SERIES_DESC = 'structural_t2seriesdesc_2'
        
        KEY_SECOND_T2W_MAGFIELDMAP_FILE_NAME = 'structural_t2magscanname_2'
        KEY_SECOND_T2W_MAGFIELDMAP_SCAN_NUMBER = 'structural_t2magscanid_2'
        KEY_SECOND_T2W_MAGFIELDMAP_DELTA_TE = 'structural_t2magscan_te_2'
        
        KEY_SECOND_T2W_PHASEFIELDMAP_FILE_NAME = 'structural_t2phascanname_2'
        KEY_SECOND_T2W_PHASEFIELDMAP_SCAN_NUMBER = 'structural_t2phascanid_2'
        
        KEY_SECOND_T2W_POS_SE_FIELDMAP_FILE_NAME = 'structural_t2_pos_se_fieldmap_2'
        KEY_SECOND_T2W_POS_SE_FIELDMAP_SCAN_NUMBER = 'structural_t2_pos_se_scanid_2'
        KEY_SECOND_T2W_POS_SE_FIELDMAP_DWELL_TIME = 'structural_t2_pos_se_dwelltime_2'
        
        KEY_SECOND_T2W_NEG_SE_FIELDMAP_FILE_NAME = 'structural_t2_neg_se_fieldmap_2'
        KEY_SECOND_T2W_NEG_SE_FIELDMAP_SCAN_NUMBER = 'structural_t2_neg_se_scanid_2'
        KEY_SECOND_T2W_NEG_SE_FIELDMAP_DWELL_TIME = 'structural_t2_neg_se_dwelltime_2'
        
        KEY_LAUNCH_FUNCTIONAL = 'launchFunctional'
        KEY_LAUNCH_DIFFUSION = 'launchDiffusion'
        KEY_LAUNCH_TASK_ANALYSIS = 'launchTask'
        KEY_LAUNCH_ICA = 'launchICAFIX'

        KEY_MAGFIELDMAP_SCAN_NUMBER = 'structural_magscanid'
        KEY_PHASEFIELDMAP_SCAN_NUMBER = 'structural_phascanid'
        KEY_DELTA_TE = 'structural_TE'
        KEY_DWELL_TIME = 'structural_dwelltime'
        KEY_UNWARP_DIR = 'structural_UnwarpDir'
        KEY_SEUNWARP_DIR = 'structural_SeUnwarpDir'
        KEY_TOPUPCONFIG = 'structural_TopupConfig'
        KEY_T1W_SAMPLE_SPACING = 'structural_T1wSampleSpacing'
        KEY_T2W_SAMPLE_SPACING = 'structural_T2wSampleSpacing'
        KEY_T1W_TEMPLATE = 'structural_T1wTemplate'
        KEY_T2W_TEMPLATE = 'structural_T2wTemplate'
        KEY_T1W_TEMPLATE_BRAIN = 'structural_T1wTemplateBrain'
        KEY_T2W_TEMPLATE_BRAIN = 'structural_T2wTemplateBrain'
        KEY_TEMPLATE_MASK = 'structural_TemplateMask'
        KEY_FINAL_TEMPLATE_SPACE = 'structural_FinalTemplateSpace'
        KEY_STRUCTURAL_FS_ASSESSOR_EXT = 'structural_fs_assessor_ext'

        KEY_SE_FIELDMAP_POS = 'structural_se_fieldmap_pos'
        KEY_SE_FIELDMAP_NEG = 'structural_se_fieldmap_neg'

        # functional keys

        KEY_FUNCTIONAL_TE='functional_TE'
        KEY_FUNCTIONAL_DISTORTION_CORRECTION='functional_DistortionCorrection'
        KEY_FUNCTIONAL_LR_FIELDMAPSERIES='functional_lr_fieldmapseries'
        KEY_FUNCTIONAL_RL_FIELDMAPSERIES='functional_rl_fieldmapseries'
        KEY_FUNCTIONAL_SCANID='functional_scanid'
        KEY_FUNCTIONAL_FUNCTIONALSERIES='functional_functionalseries'
        KEY_FUNCTIONAL_DWELLTIMES='functional_DwellTimes'
        KEY_FUNCTIONAL_UNWARP_DIR='functional_UnwarpDir'

        # ica fix keys
        KEY_ICAFIX_BP = 'icafix_bp'
        KEY_ICAFIX_FUNCTSERIES = 'icafix_functseries'

        # Actual work begins here

        Parameters = XNATPipelineParameters()
        
        Parameters.addUniqueParameter(KEY_XNAT_ID, self._xnat_session_id)
        Parameters.addUniqueParameter(KEY_SUBJECT, self._subject)
        Parameters.addUniqueParameter(KEY_SESSION_ID, self._session)

        # structural

        Parameters.addUniqueParameter(KEY_LAUNCH_STRUCTURAL, int(self.launch_structural))

        if self.launch_structural:

            if self.fieldmap_type_str == "GE" or self.fieldmap_type_str == "SiemensGradientEcho":
                Parameters.addUniqueParameter(KEY_FIELDMAP_TYPE, "GE")
            elif self.fieldmap_type_str == "SE" or self.fieldmap_type_str == "SpinEcho":
                Parameters.addUniqueParameter(KEY_FIELDMAP_TYPE, "SE")
            else:
                print("ERROR: Unrecognized fieldmap type specification: " + mpp_parameters.fieldmap_type_str)
                sys.exit(1)

            Parameters.addUniqueParameter(KEY_AVGRDMETHOD, self._structural_parameters.avgrdmethod)
            Parameters.addUniqueParameter(KEY_MAGFIELDMAP_SCAN_NUMBER, self.magfieldmap_scan_number)
            Parameters.addUniqueParameter(KEY_PHASEFIELDMAP_SCAN_NUMBER, self.phasefieldmap_scan_number)
            Parameters.addUniqueParameter(KEY_FIRST_T1W_SCAN_NUMBER, self._structural_parameters.first_t1w.scan_number)
            Parameters.addUniqueParameter(KEY_FIRST_T1W_SERIES_DESC, self._structural_parameters.first_t1w.series_desc)
            Parameters.addUniqueParameter(KEY_SECOND_T1W_SCAN_NUMBER, self._structural_parameters.second_t1w.scan_number)
            Parameters.addUniqueParameter(KEY_SECOND_T1W_SERIES_DESC, self._structural_parameters.second_t1w.series_desc)
            Parameters.addUniqueParameter(KEY_FIRST_T2W_SCAN_NUMBER, self._structural_parameters.first_t2w.scan_number)
            Parameters.addUniqueParameter(KEY_FIRST_T2W_SERIES_DESC, self._structural_parameters.first_t2w.series_desc)            
            Parameters.addUniqueParameter(KEY_SECOND_T2W_SCAN_NUMBER, self._structural_parameters.second_t2w.scan_number)
            Parameters.addUniqueParameter(KEY_SECOND_T2W_SERIES_DESC, self._structural_parameters.second_t2w.series_desc)
            Parameters.addUniqueParameter(KEY_DELTA_TE, self.delta_te)
            Parameters.addUniqueParameter(KEY_DWELL_TIME, self.dwell_time)

            Parameters.addUniqueParameter(KEY_SE_FIELDMAP_POS, self._structural_parameters.first_t1w.pos_se_fieldmap_scan_number)
            Parameters.addUniqueParameter(KEY_SE_FIELDMAP_NEG, self._structural_parameters.first_t1w.neg_se_fieldmap_scan_number)
            
            Parameters.addUniqueParameter(KEY_T1W_SAMPLE_SPACING, self._structural_parameters.t1w_sample_spacing)
            Parameters.addUniqueParameter(KEY_T2W_SAMPLE_SPACING, self._structural_parameters.t2w_sample_spacing)
            
            Parameters.addUniqueParameter(KEY_UNWARP_DIR, self._structural_parameters.unwarp_dir)
            Parameters.addUniqueParameter(KEY_SEUNWARP_DIR, self._structural_parameters.se_unwarp_dir)
            Parameters.addUniqueParameter(KEY_TOPUPCONFIG, self._structural_parameters.topup_config)
            
            Parameters.addUniqueParameter(KEY_T1W_TEMPLATE, self._structural_parameters.t1w_template)
            Parameters.addUniqueParameter(KEY_T2W_TEMPLATE, self._structural_parameters.t2w_template)
            Parameters.addUniqueParameter(KEY_T1W_TEMPLATE_BRAIN, self._structural_parameters.t1w_template_brain)
            Parameters.addUniqueParameter(KEY_T2W_TEMPLATE_BRAIN, self._structural_parameters.t2w_template_brain)
            Parameters.addUniqueParameter(KEY_TEMPLATE_MASK, self._structural_parameters.template_mask)
            Parameters.addUniqueParameter(KEY_FINAL_TEMPLATE_SPACE, self._structural_parameters.final_template_space)
            Parameters.addUniqueParameter(KEY_STRUCTURAL_FS_ASSESSOR_EXT, self._structural_parameters.structural_fs_assessor_ext)

        # functional
        Parameters.addUniqueParameter(KEY_LAUNCH_FUNCTIONAL, int(self.launch_functional))
        if self.launch_functional:
            Parameters.addUniqueParameter(KEY_FUNCTIONAL_TE, self._functional_parameters.TE)
            Parameters.addUniqueParameter(KEY_FUNCTIONAL_DISTORTION_CORRECTION, self._functional_parameters.distortion_correction_method)
            Parameters.addUniqueParameter(KEY_FUNCTIONAL_LR_FIELDMAPSERIES, self._functional_parameters.LR_fieldmapseries)
            Parameters.addUniqueParameter(KEY_FUNCTIONAL_RL_FIELDMAPSERIES, self._functional_parameters.RL_fieldmapseries)
            Parameters.addListParameters(KEY_FUNCTIONAL_SCANID, self._functional_parameters.scan_id_list)
            Parameters.addListParameters(KEY_FUNCTIONAL_FUNCTIONALSERIES, self._functional_parameters.scan_type_list)
            Parameters.addListParameters(KEY_FUNCTIONAL_DWELLTIMES, self._functional_parameters.scan_dwell_time_list)
            Parameters.addListParameters(KEY_FUNCTIONAL_UNWARP_DIR, self._functional_parameters.scan_phase_encoding_direction_list)

        # diffusion
        Parameters.addUniqueParameter(KEY_LAUNCH_DIFFUSION, int(self.launch_diffusion))
        if self.launch_diffusion:
            self._diffusion_parameters.add_parameters(Parameters)


#        KEY_DIFFUSION_ECHO_SPACING = "diffusion_EchoSpacing"
#        KEY_DIFFUSION_PHASE_ENCODING_DIR = "diffusion_PhaseEncodingDir"
#        KEY_DIFFUSION_DIR_NAMES = "diffusion_DiffusionDirDictNames"
#        KEY_DIFFUSION_DIR_VALUES = "diffusion_DiffusionDirDictValues"
#        KEY_DIFFUSION_SCAN_NAMES = "diffusion_DiffusionScanDictNames"
#        KEY_DIFFUSION_SCAN_VALUES = "diffusion_DiffusionScanDictValues"
#        KEY_DIFFUSION_NEG_DATA = 'diffusion_negData'
#        KEY_DIFFUSION_POS_DATA = 'diffusion_posData'

#        if self.launch_diffusion:
#            Parameters.addUniqueParameter(KEY_DIFFUSION_ECHO_SPACING, self._diffusion_parameters.echo_spacing)
#            Parameters.addUniqueParameter(KEY_DIFFUSION_PHASE_ENCODING_DIR, self._diffusion_parameters.phase_encoding_dir)

#            Parameters.addListParameters(KEY_DIFFUSION_DIR_NAMES, self._diffusion_parameters.dir_names)
#            Parameters.addListParameters(KEY_DIFFUSION_DIR_VALUES, self._diffusion_parameters.dir_values)
#            Parameters.addListParameters(KEY_DIFFUSION_SCAN_NAMES, self._diffusion_parameters.scan_names)
#            Parameters.addListParameters(KEY_DIFFUSION_SCAN_VALUES, self._diffusion_parameters.scan_values)

            # dir_names_dict = dict()
            # for i in xrange(0, len(self._diffusion_parameters.dir_names)):
            #     dir_names_dict[self._diffusion_parameters.dir_names[i]] = self._diffusion_parameters.dir_values[i]

            # dir_names_dict = dict(*zip(self._diffusion_parameters.dir_names,self._diffusion_parameters.dir_values))
#            dir_names_dict = dict(zip(self._diffusion_parameters.dir_names,self._diffusion_parameters.dir_values))

#            posData="@".join(["DiffData/" + self._session + "_DWI_dir" + dir_names_dict[key] + "_RL.nii.gz" for key in sorted(dir_names_dict) if key.startswith('RL')])
#            negData="@".join(["DiffData/" + self._session + "_DWI_dir" + dir_names_dict[key] + "_LR.nii.gz" for key in sorted(dir_names_dict) if key.startswith('LR')])

            # posData=""
            # negData=""

            # for key in sorted(dir_names_dict):
            #     if key.startswith('RL'):
            #         # positive
            #         if posData != "":
            #             posData = posData + "@"
                        
            #         posData = posData + "DiffData/" + self._session + "_DWI_dir" + dir_names_dict[key] + "_RL.nii.gz"

            #     elif key.startswith('LR'):
            #         # negative
            #         if negData != "":
            #             negData = negData + "@"
                        
            #         negData = negData + "DiffData/" + self._session + "_DWI_dir" + dir_names_dict[key] + "_LR.nii.gz"

            #     else:
            #         print("ERROR unrecognized direction key: " + key)
            #         sys.exit(1)

#            self._debug_print("negData: " + negData)
#            self._debug_print("posData: " + posData)

#            Parameters.addUniqueParameter(KEY_DIFFUSION_NEG_DATA, negData)
#            Parameters.addUniqueParameter(KEY_DIFFUSION_POS_DATA, posData)

        # task analysis

        Parameters.addUniqueParameter(KEY_LAUNCH_TASK_ANALYSIS, int(self.launch_task_analysis))
        if self.launch_task_analysis:

            print("\nwriting parameters for task analysis\n")

            print("\nfunctional_roots: " + self._task_analysis_parameters.functional_roots);

            Parameters.addListParameters('taskfMRI_functroot', self._task_analysis_parameters.functional_roots)

            run_parameters_list = self._task_analysis_parameters.run_parameters_list
            index = 1
            for run_parameters in run_parameters_list:
                Parameters.addUniqueParameter('taskfMRI_lowresmesh'+str(index), run_parameters.lowresmesh)
                Parameters.addUniqueParameter('taskfMRI_grayordinates'+str(index), run_parameters.grayordinates)
                Parameters.addUniqueParameter('taskfMRI_origsmoothingFWHM'+str(index), run_parameters.origsmoothingFWHM)
                Parameters.addUniqueParameter('taskfMRI_finalsmoothingFWHM'+str(index), run_parameters.finalsmoothingFWHM)
                Parameters.addUniqueParameter('taskfMRI_temporalfilter'+str(index), run_parameters.temporalfilter)
                Parameters.addUniqueParameter('taskfMRI_confound'+str(index), run_parameters.confound)
                Parameters.addUniqueParameter('taskfMRI_vba'+str(index), run_parameters.vba)
                index+=1

        # ica fix
        Parameters.addUniqueParameter(KEY_LAUNCH_ICA, int(self.launch_ica_fix))
        if self.launch_ica_fix:
            Parameters.addUniqueParameter(KEY_ICAFIX_BP, self._ica_fix_parameters.bandpass)
            Parameters.addListParameters(KEY_ICAFIX_FUNCTSERIES, self._ica_fix_parameters.functseries_list)

        # Miscellaneous project and email related parameters
        Parameters.addUniqueParameter('mailhost', MailHostStr)
        Parameters.addUniqueParameter('useremail', NotifyUserStr)
        Parameters.addUniqueParameter('userfullname', UserFullNameStr)
        Parameters.addUniqueParameter('xnatserver', XnatServerStr)
        Parameters.addUniqueParameter('adminemail', NotifyAdminStr)
        Parameters.addUniqueParameter('dataType', 'xnat:mrSessionData')
        Parameters.addUniqueParameter('project', self._project)
        Parameters.addUniqueParameter('label', self._session)

        # Save the parameters in the XML file
        Parameters.saveParameters(pathToParamsXmlFile)

# end of module: mpp_parameters.py
