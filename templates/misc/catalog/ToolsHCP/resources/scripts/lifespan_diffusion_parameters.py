from diffusion_parameters import DiffusionParameters

class LifeSpanDiffusionParameters(DiffusionParameters):
	"""
	Class for containing and managing parameters for the diffusion preprocessing part of the MPP pipeline
	"""

	def __init__(self):
		super(LifeSpanDiffusionParameters, self).__init__()

		self._detailed_outlier_stats = True
		self._replace_outliers = False


	# property detailed_outlier_stats

	def _get_detailed_outlier_stats(self):
		return self._detailed_outlier_stats

	detailed_outlier_stats=property(_get_detailed_outlier_stats)

	# property replace_outliers
	
	def _get_replace_outliers(self):
		return self._replace_outliers

	replace_outliers=property(_get_replace_outliers)


	def add_parameters(self, Parameters):
		super(LifeSpanDiffusionParameters, self).add_parameters(Parameters)
		Parameters.addUniqueParameter("diffusion_detailed_outlier_stats", str(self.detailed_outlier_stats))
		Parameters.addUniqueParameter("diffusion_replace_outliers", str(self.replace_outliers))
