from structural_scan_parameters import StructuralScanParameters

class T2wParameters(StructuralScanParameters):
    """
    Class for containing and managing parameters associated with a T2w structural scan
    """

    def __init__(self):
        super(T2wParameters, self).__init__()
