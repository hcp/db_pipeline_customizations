#!/bin/bash


#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Script to find all files created and modified using a time-stamp contained in a
# file. Found files are moved to the given folder

#ARG1: Working directory - Script expects the working directory string to end in a /

#ARG2: Time-stamp file

#ARG3: Folder to move the found files

#ARG4: List of files which have to be copied


#############################
#
# ARGUMENTS
#
#############################


program=$0

working_directory=$1

time_stamped_file=$2

folder_to_move_to=$3

list_file=$4

#############################
#
# BEGIN
#
#############################
touch $list_file

find $working_directory -type f -newer $time_stamped_file > $list_file


if [ ! -e $folder_to_move_to ]; then
 mkdir $folder_to_move_to
fi


replace_char=""

pushd $folder_to_move_to

while read file
do

 file_parent=`echo "${file%/*}"`

 if [ "${file_parent}/" != "$working_directory" ] ; then
    file_relative_path="${file_parent/$working_directory/$replace_char}"
 else
    #File is at the root of the folder so dont create a sub-folder
    file_relative_path=""
 fi


 file_name=$(basename "$file")

 if [ ! -e $folder_to_move_to/$file_relative_path ]; then
    mkdir -p $folder_to_move_to/$file_relative_path
 fi
 cp $file $folder_to_move_to/$file_relative_path/$file_name
 echo "Copied file $file_name to $folder_to_move_to/$file_relative_path"

done < $list_file

popd

exit 0;
