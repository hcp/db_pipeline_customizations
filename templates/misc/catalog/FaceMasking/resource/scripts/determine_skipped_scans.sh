#!/bin/bash

#################################################################################################
## NOTE:  This version names files according to the latest file naming convention (2011-12-15) ##
#################################################################################################

if [ $# -lt 8 ] ; then
   echo "1.0"
   exit 0
fi

export USER=$1
export PASSWORD=$2
export HOST=$3
export OUT_DIR=$4
export PROJECT=$5
export SUBJECT=$6
export SESSION=$7
export SCAN=$8
export EXISTING=$9

if [ ! -d "$OUT_DIR" ] ; then
	echo "ERROR:  Specified directory does not exist"
	exit 9
fi

cd $OUT_DIR

## MODIFICATIONS ACCORDING TO NAMING CONVENTION ##
export SCANTYPE=`curl -u $USER:$PASSWORD -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN} | grep -a " ID=" | grep -a " type=\"" | sed "s/^.* type=\"//" | sed "s/\".*$//"`
export SERIESDESC=`curl -u $USER:$PASSWORD -X GET ${HOST}/REST/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN} | grep -a "series_description" | sed "s/^[^>]*>//" | sed "s/<.*$//"`
## MRH (2012/04/26) - remove spaces in scantype/seriesdesc - bugfix for transfer failures
SCANTYPE=`echo $SCANTYPE | sed "s/ //g"`
SERIESDESC=`echo $SERIESDESC | sed "s/ //g"`

shopt -s nocasematch

if [[ $SERIESDESC =~ ^BIAS ]] || [[ $SERIESDESC =~ ^AFI ]] || [[ $SERIESDESC =~ ^T[12]W*.* ]] ;  then

	echo "SeriesDescription ($SERIESDESC) indicates eligibility for facemasking.  Continue processing..."

else 

	echo "SeriesDescription ($SERIESDESC) will be skipped..."
	echo "Scan excluded based on SeriesDescription ($SERIESDESC) -- " >  $OUT_DIR/README_scan_excluded

fi

## SEE IF ALREADY PROCESSED.  ACT ACCORDING TO "EXISTING" PARAMETER (SKIP/OVERWRITE/FAIL) ##

export DEFACE_RESP=`curl -s -L -o /dev/null -w '%{http_code}' -u $USER:$PASSWORD -X GET "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED"`
export DEFACEQC_RESP=`curl -s -L -o /dev/null -w '%{http_code}' -u $USER:$PASSWORD -X GET "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DEFACE_QC"`

if [[ ! $EXISTING =~ ^[FOS] ]] ; then

	EXISTING=FAIL

fi

if [[ $EXISTING =~ ^F  && ( $DEFACE_RESP -eq 200 || $DEFACEQC_RESP -eq 200 ) ]]; then

	echo "ERROR:  Defaced output already exists [SCAN=$SCAN].  Pipeline set to fail on existing.  Exiting...."
	exit 9

fi

if [[ $EXISTING =~ ^O ]]; then 

	if [ $DEFACE_RESP -eq 200 ]; then

		echo "NOTE:  Defaced output already exists [SCAN=$SCAN] and [EXISTING=OVERWRITE].  Removing current output"
		if [[ ${#PROJECT} -gt 0 &&  ${#SUBJECT} -gt 0 &&  ${#SESSION} -gt 0 &&  ${#SCAN} -gt 0 ]] ; then
			curl -u $USER:$PASSWORD -X DELETE "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DICOM_DEFACED"
		fi

	fi
	if [ $DEFACEQC_RESP -eq 200 ]; then

		echo "NOTE:  Defaced QC output already exists [SCAN=$SCAN] and [EXISTING=OVERWRITE].  Removing current output"
		if [[ ${#PROJECT} -gt 0 &&  ${#SUBJECT} -gt 0 &&  ${#SESSION} -gt 0 &&  ${#SCAN} -gt 0 ]] ; then
			echo "HELLO"
			curl -u $USER:$PASSWORD -X DELETE "$HOST/REST/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN/resources/DEFACE_QC"
		fi

	fi

fi

if [[ $EXISTING =~ ^S && ( $DEFACE_RESP -eq 200 || $DEFACEQC_RESP -eq 200 ) ]]; then

	echo "NOTE:  Defaced output already exists [SCAN=$SCAN] and [EXISTING=SKIP].  Skipping scan..."
	echo "Scan excluded because defacing output already exists and parameter EXISTING=SKIP" >  $OUT_DIR/README_scan_excluded

fi

shopt -u nocasematch


