#!/bin/bash 


#This script will generate a job file for a StructuralHCP pipeline processing

#DB Structural Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013
#Author: Timothy B. Brown (tbbrown@wustl.edu)
#Version 0.2 Date: 19 Sep 2014

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster


isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}

ARGS=7
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile Path_to_mppPipelineParamsFile XNAT_Password Path_to_outFile Path_to_outPutJobFile MPP_PARAM_FILE_PATH"
  exit 1
fi

paramsFile=$1
mpp_pipelineparamsFile=$2
passwd=$3
outFile=$4
outPutJobFile=$5
mppParamsFile=$6
jsession=$7

source $paramsFile
source $mpp_pipelineparamsFile

source $mppParamsFile

PIPELINE_NAME="WU_L1B/Structural/StructuralHCP.xml"

dirname=`dirname "$program"`
configdir="${dirname}/config"

#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi

config_root_name=structuralHCP

if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/${config_root_name}.pbs.config  
   configurationForPutJobFile=$configdir/CHPC/${config_root_name}_put.pbs.config  
   logDirectiveFile=$configdir/CHPC/log.pbs.config	

   processingParamsFile=$configdir/CHPC/structuralHCP.pbs.param   	
elif [ $compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/NRG/${config_root_name}.sge.config
   configurationForPutJobFile=$configdir/NRG/${config_root_name}_put.sge.config  
   logDirectiveFile=$configdir/NRG/log.sge.config	

   processingParamsFile=$configdir/NRG/structuralHCP.sge.param   	
fi

if [ ! -f $configurationForJobFile ] ; then
  echo "File at $configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $configurationForPutJobFile ] ; then
  echo "File at $configurationForPutJobFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $logDirectiveFile ] ; then
  echo "File at $logDirectiveFile doesnt exist. Aborting!"
  exit 1;
fi


#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet structural_t1scanid_1
isSet structural_t1scanid_2
isSet structural_t2scanid_1
isSet structural_t2scanid_2
isSet structural_t1seriesdesc_1
isSet structural_t1seriesdesc_2
isSet structural_t2seriesdesc_1
isSet structural_t2seriesdesc_2

if [ "${structural_fieldmap_type}" == "SE" ] ; then
    isSet structural_se_fieldmap_pos
    isSet structural_se_fieldmap_neg
    isSet structural_dwelltime
    isSet structural_SeUnwarpDir
    isSet structural_TopupConfig
else
    isSet structural_magscanid
    isSet structural_phascanid
fi

isSet structural_T1wSampleSpacing
isSet structural_T2wSampleSpacing
isSet structural_Avgrdcmethod


###########################################################
# Continue - looks good
#
###########################################################


touch $outFile
touch $outPutJobFile


if [ ! -f $outFile ] ; then
  echo "File at $outFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $outPutJobFile ] ; then
  echo "File at $outPutJobFile doesnt exist. Aborting!"
  exit 1;
fi


cat $configurationForJobFile > $outFile
cat $logDirectiveFile >> $outFile	

cat $configurationForPutJobFile > $outPutJobFile
cat $logDirectiveFile >> $outPutJobFile	


workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
if [ $? -ne 0 ] ; then
	echo "Fetching workflow for structural failed. Aborting!"
	exit 1
fi 


commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline $PIPELINE_NAME"
commandStr="${commandStr} -project $project"
commandStr="${commandStr} -id $xnat_id"
commandStr="${commandStr} -dataType $dataType"
commandStr="${commandStr} -host $xnat_host"
commandStr="${commandStr} -parameter xnatserver=$xnatserver"
commandStr="${commandStr} -parameter project=$project"
commandStr="${commandStr} -parameter xnat_id=$xnat_id"
commandStr="${commandStr} -label $label"
commandStr="${commandStr} -u $user"
commandStr="${commandStr} -pwd $passwd"
commandStr="${commandStr} -supressNotification"
commandStr="${commandStr} -notify $useremail"
commandStr="${commandStr} -notify $adminemail"
commandStr="${commandStr} -parameter adminemail=$adminemail"
commandStr="${commandStr} -parameter useremail=$useremail"
commandStr="${commandStr} -parameter mailhost=$mailhost"
commandStr="${commandStr} -parameter userfullname=$userfullname"
commandStr="${commandStr} -parameter builddir=$builddir"
commandStr="${commandStr} -parameter sessionid=$sessionId"
commandStr="${commandStr} -parameter subjects=$subject"

if [ "${structural_fieldmap_type}" == "SE" ] ; then

    commandStr="${commandStr} -parameter magscanid=NONE"
    commandStr="${commandStr} -parameter phascanid=NONE"
    commandStr="${commandStr} -parameter TE=NONE"

    commandStr="${commandStr} -parameter sefieldmappos=$structural_se_fieldmap_pos"
    commandStr="${commandStr} -parameter sefieldmapneg=$structural_se_fieldmap_neg"
    commandStr="${commandStr} -parameter dwelltime=$structural_dwelltime"
    commandStr="${commandStr} -parameter seunwarpdir=$structural_SeUnwarpDir"
    commandStr="${commandStr} -parameter topupconfig=$structural_TopupConfig"

else
    commandStr="${commandStr} -parameter magscanid=$structural_magscanid"
    commandStr="${commandStr} -parameter phascanid=$structural_phascanid"
    commandStr="${commandStr} -parameter TE=$structural_TE"

    commandStr="${commandStr} -parameter sefieldmappos=NONE"
    commandStr="${commandStr} -parameter sefieldmapneg=NONE"
    commandStr="${commandStr} -parameter dwelltime=NONE"
    commandStr="${commandStr} -parameter seunwarpdir=NONE"
    commandStr="${commandStr} -parameter topupconfig=NONE"

fi

commandStr="${commandStr} -parameter t1scanid_1=$structural_t1scanid_1"
commandStr="${commandStr} -parameter t1scanid_2=$structural_t1scanid_2"
commandStr="${commandStr} -parameter t2scanid_1=$structural_t2scanid_1"
commandStr="${commandStr} -parameter t2scanid_2=$structural_t2scanid_2"
commandStr="${commandStr} -parameter t1seriesdesc_1=$structural_t1seriesdesc_1"
commandStr="${commandStr} -parameter t1seriesdesc_2=$structural_t1seriesdesc_2"
commandStr="${commandStr} -parameter t2seriesdesc_1=$structural_t2seriesdesc_1"
commandStr="${commandStr} -parameter t2seriesdesc_2=$structural_t2seriesdesc_2"
commandStr="${commandStr} -parameter T1wSampleSpacing=$structural_T1wSampleSpacing"
commandStr="${commandStr} -parameter T2wSampleSpacing=$structural_T2wSampleSpacing"
commandStr="${commandStr} -parameter Avgrdcmethod=$structural_Avgrdcmethod"
commandStr="${commandStr} -parameter T1wTemplate=$structural_T1wTemplate"
commandStr="${commandStr} -parameter T1wTemplateBrain=$structural_T1wTemplateBrain"
commandStr="${commandStr} -parameter T2wTemplate=$structural_T2wTemplate"
commandStr="${commandStr} -parameter T2wTemplateBrain=$structural_T2wTemplateBrain"
commandStr="${commandStr} -parameter TemplateMask=$structural_TemplateMask"
commandStr="${commandStr} -parameter FinalTemplateSpace=$structural_FinalTemplateSpace"
commandStr="${commandStr} -parameter templatesdir=$templatesdir"
commandStr="${commandStr} -parameter configdir=$configdir"
commandStr="${commandStr} -parameter CaretAtlasDir=$CaretAtlasDir"
commandStr="${commandStr} -parameter compute_cluster=$compute_cluster"
commandStr="${commandStr} -parameter packaging_outdir=$packaging_outdir"
commandStr="${commandStr} -parameter structural_fs_assessor_ext=$structural_fs_assessor_ext"
commandStr="${commandStr} -parameter cluster_builddir_prefix=$cluster_builddir_prefix"
commandStr="${commandStr} -parameter db_builddir_prefix=$db_builddir_prefix"
commandStr="${commandStr} -workFlowPrimaryKey $workflowID"

putCommandStr="$commandStr -startAt 15"


echo "Creating $outFile" 

echo "echo \" \"" >> $outFile
echo "$commandStr" >> $outFile
echo "rc_command=\$?" >> $outFile

echo "echo \$rc_command \" \"" >> $outFile
echo "echo \"Job finished  at \`date\`\"" >> $outFile

echo "exit \$rc_command" >> $outFile

chmod +x $outFile

###########################################################
# Create PUT JOB
#
###########################################################


echo "Creating $outPutJobFile" 

echo "echo \" \"" >> $outPutJobFile
echo "$putCommandStr" >> $outPutJobFile
echo "rc_command=\$?" >> $outPutJobFile

echo "echo \$rc_command \" \"" >> $outPutJobFile
echo "echo \"Job finished  at \`date\`\"" >> $outPutJobFile

echo "exit \$rc_command" >> $outPutJobFile

chmod +x $outPutJobFile

exit 0;
