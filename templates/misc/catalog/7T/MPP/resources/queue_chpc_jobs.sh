#!/bin/bash -x

#This script will submit jobs to the PBS managed CHPC cluster for a subject taking care of the job dependencies

# Data Processing pipeline Job submittor
# Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
# Version 0.1 Date: November 15, 2013
#
# Modified for 7T by: Timothy B. Brown (tbbrown@wustl.edu)
#
# Inputs: 
#  path of param file

############################################################################
#
# Convenience Methods
#
############################################################################
program="${0}"

log()
{
	msg=$@
	now=`date`
	echo "${now} - ${program} - ${msg}"
}

get_jobid()
{
	# $1: Series Description of the scan
	index=0
	found=-1
	for fseries in ${functional_functionalseries[@]}
	do
		if [ $fseries = $1 ]
		then
			found=1
			break
		fi
		index=$((index+1))
	done

	# Did we find the series description
	if [ "$found" == "-1" ]
	then
		echo $found
	else
		echo ${functionalJobIds[$index]}
	fi
}

isQueuedFSF()
{
	# $1: Series Description of the scan
	isQueued=0
	for fseries in "${queued[@]}"
	do
		if [ $fseries = $1 ]
		then
			isQueued=1
			break
		fi
	done

	echo $isQueued
}

log "STARTING"

paramsFile=$1
log "paramsFile: ${paramsFile}"

passwd=$2

path_to_scripts=$3
log "path_to_scripts: ${path_to_scripts}"

declare -a functionalJobIds

declare -a queued

functionalJobIds=()
queued=()

source $paramsFile

queueLogFile=$path_to_scripts/queue.log

if [ -e $queueLogFile ]
then
	\rm $queueLogFile
fi

touch $queueLogFile

############################################################################
#
# Submit the functional jobs
#
############################################################################

if [ $launchFunctional -eq 1 ]
then
	index=0

	for fscan in "${functional_scanid[@]}"
	do
		fseries=${functional_functionalseries[$index]}
		fseriesRoot=`echo $fseries | awk '{gsub(/_PA/,""); gsub(/_AP/,""); print}'`

		dependencyControl=" "
		FUNCTIONAL_START_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseries}_functional.sh`
		if [ $? -ne 0 ] ; then
			log "Submission of  $path_to_scripts/${subject}_${fseries}_functional.sh failed. Aborting!"
			exit 1
		else
			log "Functional job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_START_JOB_ID"
			echo "Functional job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_START_JOB_ID" >> $queueLogFile
		fi

		dependencyControl=" -W depend=afterok:${FUNCTIONAL_START_JOB_ID} "

		FUNCTIONAL_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseries}_functional_put.sh`
		if [ $? -ne 0 ]
		then
			log "Submission of  $path_to_scripts/${subject}_${fseries}_functional_put.sh failed. Aborting!"
			exit 1
		else 
			log "Functional job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID"
			echo "Functional PUT job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID" >> $queueLogFile
		fi

		functionalJobIds[$index]=$FUNCTIONAL_JOB_ID

		let index=index+1;
	done
fi

############################################################################
#
# Now submit the FSF creation job - these cannot be run unless the Functionals are done
#
############################################################################

if [ $launchFunctional -eq 1 ]
then

	index=0
	j=0

	for fscan in "${functional_scanid[@]}"
	do
		fseries=${functional_functionalseries[$index]}
		fseriesRoot=`echo $fseries | awk '{gsub(/_PA/,""); gsub(/_AP/,""); print}'`

		isQueuedFSF=`isQueuedFSF $fseriesRoot` # We see fseriesRoot twice hence we need to keep track
		if [ $isQueuedFSF -eq 0 ]
		then
			pa_task_series=${fseriesRoot}_PA
			ap_task_series=${fseriesRoot}_AP
			pa_job_id=`get_jobid $pa_task_series`
			ap_job_id=`get_jobid $ap_task_series`
			dependencyControl=" "

			if [ $pa_job_id != "-1" ] || [ $ap_job_id != "-1" ]
			then
				dependencyControl=" -W depend=afterok"
			fi

			if [  $pa_job_id != "-1"  ]
			then
				dependencyControl="${dependencyControl}:${pa_job_id}"
			fi

			if [  $ap_job_id != "-1"  ]
			then
				dependencyControl="${dependencyControl}:${ap_job_id}"
			fi

			dependencyControl="${dependencyControl} "

			echo "FSF DEPENDENCY $fseriesRoot $dependencyControl" >> $queueLogFile

			FUNCTIONAL_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseriesRoot}_end.sh`
			if [ $? -ne 0 ] ; then
				log "Submission of  $path_to_scripts/${subject}_${fseriesRoot}_end.sh failed. Aborting!"
				exit 1
			else
				log "FSF job for $subject scan $fseriesRoot has been queued. JOB ID = $FUNCTIONAL_JOB_ID"
				echo "FSF job for $subject scan $fseriesRoot has been queued. JOB ID = $FUNCTIONAL_JOB_ID" >> $queueLogFile
			fi
			
			queued[$j]=$fseriesRoot
			j=$((j+1))
			echo ${queued[@]}
		fi
		index=$((index+1))
	done
fi

############################################################################
#
# Now submit the diffusion job 
#
############################################################################

if [ $launchDiffusion -eq 1 ]
then
	dependencyControl=" "

	PRE_EDDY_DIFFUSION=`qsub -V $dependencyControl $path_to_scripts/${subject}_pre_eddy.sh`
	if [ $? -ne 0 ]
	then
		log "Submission of  $path_to_scripts/${subject}_pre_eddy.sh failed. Aborting!"
		exit 1
	else
		log "Diffusion job for $subject has been queued. JOB ID = $PRE_EDDY_DIFFUSION"
		echo "Diffusion PRE-EDDY job for $subject has been queued. JOB ID = $PRE_EDDY_DIFFUSION" >> $queueLogFile
	fi

	dependencyControl=" -W depend=afterok:$PRE_EDDY_DIFFUSION "
	EDDY_DIFFUSION=`qsub -V $dependencyControl $path_to_scripts/${subject}_eddy.sh`
	if [ $? -ne 0 ]
	then
		log "Submission of  $path_to_scripts/${subject}_eddy.sh failed. Aborting!"
		exit 1
	else
		log "Diffusion job for $subject has been queued. JOB ID = $EDDY_DIFFUSION"
		echo "Diffusion EDDY job for $subject has been queued. JOB ID = $EDDY_DIFFUSION" >> $queueLogFile
	fi

	dependencyControl=" -W depend=afterok:$EDDY_DIFFUSION "
	POST_EDDY_DIFFUSION=`qsub -V $dependencyControl $path_to_scripts/${subject}_post_eddy.sh`
	if [ $? -ne 0 ]
	then
		log "Submission of  $path_to_scripts/${subject}_post_eddy.sh failed. Aborting!"
		exit 1
	else 
		log "Diffusion job for $subject has been queued. JOB ID = $POST_EDDY_DIFFUSION"
		echo "Diffusion POST-DIFFUSION job for $subject has been queued. JOB ID = $POST_EDDY_DIFFUSION" >> $queueLogFile
	fi

	dependencyControl=" -W depend=afterok:$POST_EDDY_DIFFUSION "
	DIFFUSION=`qsub -V $dependencyControl $path_to_scripts/${subject}_diffusion_put.sh`
	if [ $? -ne 0 ] ; then
		log "Submission of  $path_to_scripts/${subject}_diffusion.sh failed. Aborting!"
		exit 1
	else 
		log "Diffusion PUT job for $subject has been queued. JOB ID = $DIFFUSION"
		echo "Diffusion PUT job for $subject has been queued. JOB ID = $DIFFUSION" >> $queueLogFile
	fi

fi


######################################################################################################################
#
# Now submit the ICA+FIX Analysis pipeline 
#
######################################################################################################################

if [ $launchICAFIX -eq 1 ]
then
	index=0
	fix_packaging_dependency_control=" -W depend=afterok:"

	for fscan in "${icafix_functseries[@]}"
	do
		fseries=${icafix_functseries[$index]}
		fseriesRoot=`echo $fseries | awk '{gsub(/_PA/,""); gsub(/_AP/,""); print}'`

		dependencyControl=" "
		if [ $launchFunctional -eq 1 ]
		then
			functional_put_job_id=`get_jobid $fseries`
			dependencyControl=" -W depend=afterok:${functional_put_job_id} "
		fi

		FIX_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseries}_icafix.sh`
		if [ $? -ne 0 ]
		then
			log "Submission of  $path_to_scripts/${subject}_${fseries}_icafix.sh failed. Aborting!"
			exit 1
		else
			log "ICAFIX job for $subject scan $fseries has been queued. JOB ID = $FIX_JOB_ID"
			echo "ICAFIX job for $subject scan $fseries has been queued. JOB ID = $FIX_JOB_ID" >> $queueLogFile
		fi
		
		dependencyControl=" -W depend=afterok:$FIX_JOB_ID "

		FIX_PUT=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseries}_icafix_put.sh`
		if [ $? -ne 0 ]
		then
			log "Submission of  $path_to_scripts/${subject}_${fseries}_icafix_put.sh failed. Aborting!"
			exit 1
		else
			log "ICAFIX PUT job for $subject has been queued. JOB ID = $FIX_PUT"
			echo "ICAFIX PUT job for $subject has been queued. JOB ID = $FIX_PUT" >> $queueLogFile
			fix_packaging_dependency_control="${fix_packaging_dependency_control}${FIX_PUT}:"
		fi

		index=$((index+1))
	done

    # Now queue the FIX packaging script
    # Remove the last colon

	fix_packaging_dependency_control=`echo $fix_packaging_dependency_control | sed 's/:$//'`
	fix_packaging_dependency_control="${fix_packaging_dependency_control=} "
	echo "PACKGING DEPENDS ON $fix_packaging_dependency_control " >> $queueLogFile
	FIX_PACKAGING_JOB_ID=`qsub -V $fix_packaging_dependency_control $path_to_scripts/${subject}_icafix_package.sh`
	if [ $? -ne 0 ]
	then
		log "Submission of  $path_to_scripts/${subject}_icafix_package.sh failed. Aborting!"
		exit 1
	else
		log "ICAFIX PACKAGE job for $subject has been queued. JOB ID = $FIX_PACKAGING_JOB_ID"
		echo "ICAFIX PACKAGE job for $subject has been queued. JOB ID = $FIX_PACKAGING_JOB_ID" >> $queueLogFile
	fi

fi

log "ENDING"
exit 0