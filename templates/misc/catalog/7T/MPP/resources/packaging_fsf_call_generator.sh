#!/bin/bash -x

declare -a seriesRoot


seriesRoot=(  "rfMRI_REST1"  "tfMRI_WM"  "tfMRI_GAMBLING" "tfMRI_MOTOR"  "rfMRI_REST2"   "tfMRI_LANGUAGE"  "tfMRI_SOCIAL"  "tfMRI_RELATIONAL" "tfMRI_EMOTION")

xnat_host="http://db.humanconnectome.org"

program="$0"

subject=$1
user=$2
passwd=$3
project=$4



builddir=$BUILD_DIR/PACKAGING/$subject


#Default to CHPC
if [ X$compute_cluster = X ] ; then
	compute_cluster=CHPC
fi


path_to_scripts=`dirname "$program"`
configdir=$path_to_scripts/config



logDirectiveFile=$configdir/CHPC/log.pbs.config	
processingParamsFile=$configdir/CHPC/mpp.pbs.param   	


fsf_configurationForJobFileName="functionalHCP_fsf"
fsf_configurationForJobFile=$configdir/CHPC/${fsf_configurationForJobFileName}.pbs.config  


source $processingParamsFile


mkdir -p $builddir

queueLogFile=$builddir/queue.log

if [ -e $queueLogFile ] ; then
	\rm $queueLogFile
fi

touch $queueLogFile


for fscan in "${seriesRoot[@]}"
do

###########################################################
# Create FSF and Package Creation JOB
#
###########################################################

	fsf_packaging_outFile=$builddir/${subject}_${fscan}_end.sh


	if [ ! -f $fsf_packaging_outFile ] ; then
		touch $fsf_packaging_outFile
		cat $fsf_configurationForJobFile > $fsf_packaging_outFile
		cat $logDirectiveFile >> $fsf_packaging_outFile	

		xnat_hostRoot=`echo $xnat_host | awk '{gsub(/http:\/\//,""); gsub(/:8080\//,""); gsub(/https:\/\//,""); gsub(/\//,""); print}'`
		fsf_builddir=$builddir/FSF/$fscan

		mkdir -p $fsf_builddir
		mkdir -p $builddir/$fscan
		echo "Creating $fsf_packaging_outFile" 

		if [[ $fscan == tfMRI* ]] ; then
			fsf_commandStr="$NRG_PACKAGES/tools/HCP/FSF/callCreateFSFs.sh --host $xnat_hostRoot --user $user --pw $passwd --buildDir $fsf_builddir --project $project --subject $subject --series $fscan"

			echo "echo \" \"" >> $fsf_packaging_outFile
			echo "$fsf_commandStr" >> $fsf_packaging_outFile
			echo "echo \" \"" >> $fsf_packaging_outFile
			
		fi

		packaging_commandStr="source $SCRIPTS_HOME/epd-python_setup.sh; source $SCRIPTS_HOME/groovy_setup.sh;   $NRG_PACKAGES/tools/packaging/callPackager.sh --host $xnat_hostRoot --user $user --pw $passwd --outDir $packaging_outdir --buildDir $builddir/$fscan/ --project $project --subject $subject --outputFormat PACKAGE --outputType PREPROC --packet FUNCTIONAL --series $fscan"

		echo "echo \" \"" >> $fsf_packaging_outFile
		echo "$packaging_commandStr" >> $fsf_packaging_outFile

		echo "echo \" \"" >> $fsf_packaging_outFile
		echo "echo \"Job finished  at \`date\`\"" >> $fsf_packaging_outFile

		echo "exit 0;" >> $fsf_packaging_outFile
		chmod +x $fsf_packaging_outFile

		FUNCTIONAL_JOB_ID=`qsub -V $fsf_packaging_outFile`
		if [ $? -ne 0 ] ; then
			echo "Submission of  $fsf_packaging_outFile failed. Aborting!"
			exit 1
		else
			echo "FSF job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID"
			echo "FSF job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID" >> $queueLogFile
		fi

	fi


done
