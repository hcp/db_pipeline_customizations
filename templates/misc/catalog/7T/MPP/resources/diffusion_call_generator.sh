#!/bin/bash -x
set -e

# This script will generate a job file for the 7T version of DiffusionHCP processing

# DB Diffusion Processing pipeline Job File Generator
# Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
# Version 0.1 Date: November 15, 2013
# 
# Modified for 7T:
# Author: Timothy B. Brown (tbbrown@wustl.edu)
#
# Inputs: 
#  path of param file
#  path to location where the job file would be generated

# The PBS/SGE statements are picked from a config file for the specific cluster

ARGS=6
program="$0"

log()
{
	msg=$@
	now=`date`
	echo "${now} - ${program} - ${msg}"
}

isSet() {
	if [[ ! ${!1} && ${!1-_} ]]
	then
		log "$1 is not set, aborting."
		exit 1
	elif [ -z ${!1} ]
	then
		log "$1 has no value, aborting."
		exit 1
	fi
}

log "STARTING"

if [ $# -ne "$ARGS" ]
then
	echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outFile Path_to_outPutJobFile MPP_PARAM_FILE_PATH"
	exit 1
fi

paramsFile=$1
log "paramsFile: ${paramsFile}"

mpp_pipelineparamsFile=$2
log "mpp_pipelineparamsFile: ${mpp_pipelineparamsFile}"

passwd=$3

outDir=$4
log "outDir: ${outDir}"

mppParamsFile=$5
log "mppParamsFile: ${mppParamsFile}"

jsession=$6
log "jsession: ${jsession}"

dirname=`dirname "$program"`
configdir="${dirname}/config"

source $paramsFile
source $mpp_pipelineparamsFile
source $mppParamsFile

PIPELINE_NAME="7T/Diffusion/DiffusionHCP.xml"
log "PIPELINE_NAME: ${PIPELINE_NAME}"

#Default to CHPC
if [ X$compute_cluster = X ]
then
	compute_cluster=CHPC
fi

if [ $compute_cluster = CHPC ]
then
	configurationForPreEddyJobFile=$configdir/CHPC/diffusionHCP_preeddy.pbs.config
	configurationForEddyJobFile=$configdir/CHPC/diffusionHCP_eddy.pbs.config
	configurationForPostEddyJobFile=$configdir/CHPC/diffusionHCP_posteddy.pbs.config
	configurationForPutJobFile=$configdir/CHPC/diffusionHCP_put.pbs.config
	processingParamsFile=$configdir/CHPC/diffusionHCP.pbs.param
	logDirectiveFile=$configdir/CHPC/log.pbs.config
elif [ $compute_cluster = NRG ]
then
	configurationForPreEddyJobFile=$configdir/NRG/diffusionHCP_preeddy.sge.config
	configurationForEddyJobFile=$configdir/NRG/diffusionHCP_eddy.sge.config
	configurationForPostEddyJobFile=$configdir/NRG/diffusionHCP_posteddy.sge.config
	configurationForPutJobFile=$configdir/NRG/diffusionHCP_put.sge.config
	processingParamsFile=$configdir/NRG/diffusionHCP.sge.param
	logDirectiveFile=$configdir/NRG/log.sge.config
fi

log "configurationForPreEddyJobFile: ${configurationForPreEddyJobFile}"
if [ ! -f $configurationForPreEddyJobFile ]
then
	log "File at $configurationForPreEddyJobFile does not exist. Aborting!"
	exit 1;
fi

log "configurationForEddyJobFile: ${configurationForEddyJobFile}"
if [ ! -f $configurationForEddyJobFile ]
then
	log "File at $configurationForEddyJobFile does not exist. Aborting!"
	exit 1;
fi

log "configurationForPostEddyJobFile: ${configurationForPostEddyJobFile}"
if [ ! -f $configurationForPostEddyJobFile ]
then
	log "File at $configurationForPostEddyJobFile does not exist. Aborting!"
	exit 1;
fi

log "configurationForPutJobFile: ${configurationForPutJobFile}"
if [ ! -f $configurationForPutJobFile ]
then
	log "File at $configurationForPutJobFile does not exist. Aborting!"
	exit 1;
fi

log "processingParamsFile: ${processingParamsFile}"
if [ ! -f $processingParamsFile ]
then
	log "File at $processingParamsFile does not exist. Aborting!"
	exit 1;
fi

log "logDirectiveFile: ${logDirectiveFile}"
if [ ! -f $logDirectiveFile ]
then
	log "File at $logDirectiveFile doesnt exist. Aborting!"
	exit 1;
fi

#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet diffusion_EchoSpacing
isSet diffusion_PhaseEncodingDir
isSet diffusion_DiffusionDirDictNames[0]
isSet diffusion_DiffusionDirDictValues[0]
isSet diffusion_DiffusionScanDictValues[0]
isSet diffusion_DiffusionScanDictNames[0]

if [[ ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionDirDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictNames[@]}  ]]
then
	log "The array lengths of variables diffusion_DiffusionDirDictNames, diffusion_DiffusionDirDictValues, diffusion_DiffusionScanDictValues, diffusion_DiffusionScanDictNames don't match. Aborting! "
	exit 1
fi 

###########################################################
# Continue - looks good
#
###########################################################

outPreEddyFile=${outDir}${subject}_pre_eddy.sh
outEddyFile=${outDir}${subject}_eddy.sh
outPostEddyFile=${outDir}${subject}_post_eddy.sh
outPutJobFile=${outDir}${subject}_diffusion_put.sh

touch $outPreEddyFile
touch $outEddyFile
touch $outPostEddyFile

touch $outPutJobFile

if [ ! -f $outPreEddyFile ]
then
	log "File at $outPreEddyFile doesn't exist. Aborting!"
	exit 1;
fi

if [ ! -f $outEddyFile ]
then
	log "File at $outEddyFile doesn't exist. Aborting!"
	exit 1;
fi

if [ ! -f $outPostEddyFile ]
then
	log "File at $outPostEddyFile doesn't exist. Aborting!"
	exit 1;
fi

if [ ! -f $outPutJobFile ]
then
	log "File at $outPutJobFile doesnt exist. Aborting!"
	exit 1;
fi

cat $configurationForPreEddyJobFile > $outPreEddyFile
cat $logDirectiveFile >> $outPreEddyFile
 
cat $configurationForEddyJobFile > $outEddyFile
cat $logDirectiveFile >> $outEddyFile

cat $configurationForPostEddyJobFile > $outPostEddyFile
cat $logDirectiveFile >> $outPostEddyFile


cat $configurationForPutJobFile > $outPutJobFile
cat $logDirectiveFile >> $outPutJobFile


workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
if [ $? -ne 0 ]
then
	log "Fetching workflow failed. Aborting!"
	exit 1
fi 

commandStr=""
commandStr+=" $PIPELINE_HOME/bin/XnatPipelineLauncher"
commandStr+=" -pipeline $PIPELINE_NAME"
commandStr+=" -project $project"
commandStr+=" -id $xnat_id"
commandStr+=" -dataType $dataType"
commandStr+=" -host $xnat_host"
commandStr+=" -parameter xnatserver=$xnatserver"
commandStr+=" -parameter project=$project"
commandStr+=" -parameter xnat_id=$xnat_id"
commandStr+=" -label $label"
commandStr+=" -u $user"
commandStr+=" -supressNotification"
commandStr+=" -notify $useremail"
commandStr+=" -notify $adminemail"
commandStr+=" -parameter adminemail=$adminemail"
commandStr+=" -parameter useremail=$useremail"
commandStr+=" -parameter mailhost=$mailhost"
commandStr+=" -parameter userfullname=$userfullname"
commandStr+=" -parameter builddir=$builddir"
commandStr+=" -parameter sessionid=$sessionId"
commandStr+=" -parameter subjects=$subject"
commandStr+=" -parameter templatesdir=$templatesdir"
commandStr+=" -parameter configdir=$configdir"
commandStr+=" -parameter CaretAtlasDir=$CaretAtlasDir"
commandStr+=" -parameter EchoSpacing=$diffusion_EchoSpacing"
commandStr+=" -parameter PhaseEncodingDir=$diffusion_PhaseEncodingDir"
commandStr+=" -parameter posData=\"$diffusion_posData\""
commandStr+=" -parameter negData=\"$diffusion_negData\""
commandStr+=" -parameter compute_cluster=$compute_cluster"
commandStr+=" -parameter packaging_outdir=$packaging_outdir"
commandStr+=" -parameter cluster_builddir_prefix=$cluster_builddir_prefix"
commandStr+=" -parameter db_builddir_prefix=$db_builddir_prefix"
commandStr+=" -parameter structuralReferenceProject=$structuralReferenceProject"
commandStr+=" -parameter structuralReferenceSession=$structuralReferenceSession"
commandStr+=" -parameter dwiname=$diffusion_dwiname"
commandStr+=" -workFlowPrimaryKey $workflowID"

# Now for the Direction scans - append the parameters to the command

index=0
for diffParameter in "${diffusion_DiffusionDirDictNames[@]}"
do
	diffDirParameterValue=${diffusion_DiffusionDirDictValues[$index]}
	diffParameterValue=${diffusion_DiffusionScanDictValues[$index]}
	diffParameterName=${diffusion_DiffusionScanDictNames[$index]}
	
	commandStr+=" -parameter ${diffParameter}=${diffDirParameterValue} -parameter ${diffParameterName}=${diffParameterValue}"
	index=$((index+1))
done

eddyCommandStr="${commandStr} -startAt 7b"
postEddyCommandStr="${commandStr} -startAt 7c"
putCommandStr="${commandStr} -startAt 12"

# all command strings logged without password to avoid having password in logs
log "commandStr: ${commandStr}"
log "eddyCommandStr: ${eddyCommandStr}"
log "postEddyCommandStr: ${postEddyCommandStr}"
log "putCommandStr: ${putCommandStr}"

# password added to all command strings after logging
commandStr+=" -pwd $passwd"
eddyCommandStr+=" -pwd $passwd"
postEddyCommandStr+=" -pwd $passwd"
putCommandStr+=" -pwd $passwd"

###########################################################
# Create PRE-EDDY JOB
#
###########################################################

log "Creating $outPreEddyFile"

echo "echo \" \"" >> $outPreEddyFile
echo "$commandStr" >> $outPreEddyFile
echo "rc_command=\$?" >> $outPreEddyFile

echo "echo \$rc_command \" \"" >> $outPreEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outPreEddyFile

echo "exit \$rc_command" >> $outPreEddyFile

chmod +x $outPreEddyFile


###########################################################
# Create EDDY JOB
#
###########################################################

log "Creating $outEddyFile"

echo "echo \" \"" >> $outEddyFile
echo "$eddyCommandStr" >> $outEddyFile
echo "rc_command=\$?" >> $outEddyFile

echo "echo \$rc_command \" \"" >> $outEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outEddyFile

echo "exit \$rc_command" >> $outEddyFile

chmod +x $outEddyFile


###########################################################
# Create POST-EDDY JOB
#
###########################################################

log "Creating $outPostEddyFile"

echo "echo \" \"" >> $outPostEddyFile
echo "$postEddyCommandStr" >> $outPostEddyFile

echo "rc_command=\$?" >> $outPostEddyFile

echo "echo \$rc_command \" \"" >> $outPostEddyFile
echo "echo \"Job finished  at \`date\`\"" >> $outPostEddyFile

echo "exit \$rc_command" >> $outPostEddyFile

chmod +x $outEddyFile

###########################################################
# Create PUT JOB
#
###########################################################

log "Creating $outPutJobFile"

echo "echo \" \"" >> $outPutJobFile
echo "$putCommandStr" >> $outPutJobFile
echo "rc_command=\$?" >> $outPutJobFile

echo "echo \$rc_command \" \"" >> $outPutJobFile
echo "echo \"Job finished  at \`date\`\"" >> $outPutJobFile

echo "exit \$rc_command" >> $outPutJobFile

chmod +x $outPutJobFile

log "ENDING"
exit 0

