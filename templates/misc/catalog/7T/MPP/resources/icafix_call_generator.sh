#!/bin/bash
set -e

# This script will generate a job file for ICA+FIX analysis 

# Inputs: 
#  path of param file
#  path to location where the job file would be generated

# The PBS/SGE statements are picked from a config file for the specific cluster

ARGS=6
program="$0"

log()
{
	msg=$@
	now=`date`
	echo "${now} - ${program} - ${msg}"
}

isSet()
{
	if [[ ! ${!1} && ${!1-_} ]]
	then
		log "$1 is not set, aborting."
		exit 1
	elif [ -z ${!1} ]
	then
		log "$1 has no value, aborting."
		exit 1
	fi
}

log "STARTING"

if [ $# -ne "$ARGS" ]
then
	echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outDir MPP_PARAM_FILE_PATH"
	exit 1
fi

paramsFile=$1
log "paramsFile: ${paramsFile}"

mpp_pipelineparamsFile=$2
log "mpp_pipelineparamsFile: ${mpp_pipelineparamsFile}"

passwd=$3

outDir=$4
log "outDir: ${outDir}"

mppParamsFile=$5
log "mppParamsFile: ${mppParamsFile}"

jsession=$6
log "jsession: ${jsession}"

source $paramsFile
source $mpp_pipelineparamsFile
source $mppParamsFile

PIPELINE_NAME="7T/rfMRIFIXHCP/rfMRIFIXHCP.xml"
log "PIPELINE_NAME: ${PIPELINE_NAME}"

#Default to CHPC
if [ X$compute_cluster = X ]
then
	compute_cluster=CHPC
fi

dirname=`dirname "$program"`
configdir="${dirname}/config"

if [ $compute_cluster = CHPC ]
then
	configurationForJobFile=$configdir/CHPC/icafix.pbs.config
	putConfigurationForJobFile=$configdir/CHPC/icafix_put.pbs.config
	fix_configurationForJobFile=$configdir/CHPC/icafix_packaging.pbs.config
	logDirectiveFile=$configdir/CHPC/log.pbs.config
elif [ $compute_cluster = NRG ]
then
	configurationForJobFile=$configdir/NRG/icafix.sge.config
	putConfigurationForJobFile=$configdir/NRG/icafix_put.sge.config
	fix_configurationForJobFile=$configdir/NRG/icafix_packaging.sge.config
	logDirectiveFile=$configdir/NRG/log.sge.config
fi

if [ ! -f $configurationForJobFile ]
then
	log "File at $configurationForJobFile does not exist. Aborting!"
	exit 1;
fi

if [ ! -f $putConfigurationForJobFile ]
then
	log "File at $puConfigurationForJobFile does not exist. Aborting!"
	exit 1;
fi

if [ ! -f $fix_configurationForJobFile ]
then
	log "File at $fix_configurationForJobFile does not exist. Aborting!"
	exit 1;
fi

if [ ! -f $logDirectiveFile ]
then
	log "File at $logDirectiveFile does not exist. Aborting!"
	exit 1;
fi

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet icafix_bp
isSet icafix_functseries

###########################################################
# Continue - looks good
#
###########################################################

for fscan in "${icafix_functseries[@]}"
do
	#For each scan create the outfile of command to launch the ICA FIX processing pipeline
	outFile=$outDir/${subject}_${fscan}_icafix.sh
	putFile=$outDir/${subject}_${fscan}_icafix_put.sh

	touch $outFile
	touch $putFile

	if [ ! -f $outFile ]
	then
		log "File at $outFile doesnt exist. Aborting!"
		exit 1;
	fi
	
	if [ ! -f $putFile ]
	then
		log "File at $putFile doesnt exist. Aborting!"
		exit 1;
	fi

	cat $configurationForJobFile > $outFile
	cat $putConfigurationForJobFile > $putFile

	cat $logDirectiveFile >> $outFile	
	cat $logDirectiveFile >> $putFile	

	workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
	if [ $? -ne 0 ]
	then
		echo "Fetching workflow failed. Aborting!"
		exit 1
	fi 

	commandStr=""
	commandStr+=" $PIPELINE_HOME/bin/XnatPipelineLauncher"
	commandStr+=" -pipeline $PIPELINE_NAME"
	commandStr+=" -project $project"
	commandStr+=" -id $xnat_id"
	commandStr+=" -dataType $dataType"
	commandStr+=" -host $xnat_host"
	commandStr+=" -parameter xnatserver=$xnatserver"
	commandStr+=" -parameter project=$project"
	commandStr+=" -parameter xnat_id=$xnat_id"
	commandStr+=" -label $label"
	commandStr+=" -u $user"
	commandStr+=" -supressNotification"
	commandStr+=" -notify $useremail"
	commandStr+=" -notify $adminemail"
	commandStr+=" -parameter adminemail=$adminemail"
	commandStr+=" -parameter useremail=$useremail"
	commandStr+=" -parameter mailhost=$mailhost"
	commandStr+=" -parameter userfullname=$userfullname"
	commandStr+=" -parameter builddir=$builddir"
	commandStr+=" -parameter sessionid=$sessionId"
	commandStr+=" -parameter subjects=$subject"
	commandStr+=" -parameter BP=$icafix_bp"
	commandStr+=" -parameter functseries=$fscan"
	commandStr+=" -parameter compute_cluster=$compute_cluster"
	commandStr+=" -parameter packaging_outdir=$packaging_outdir"
	commandStr+=" -parameter cluster_builddir_prefix=$cluster_builddir_prefix"
	commandStr+=" -parameter db_builddir_prefix=$db_builddir_prefix"
	commandStr+=" -parameter structuralReferenceProject=$structuralReferenceProject"
	commandStr+=" -parameter structuralReferenceSession=$structuralReferenceSession"
	commandStr+=" -workFlowPrimaryKey $workflowID"
	# commandStr and putCommandStr logged without password to avoid having password in logs
	log "commandStr: ${commandStr}"

	putCommandStr="${commandStr} -startAt 6"
	log "putCommandStr: ${putCommandStr}"
	
	# password added to both commandStr and putCommandStr after logging
	commandStr+=" -pwd $passwd"
	putCommandStr+=" -pwd $passwd"

	log "Creating $outFile" 

	echo "echo \" \"" >> $outFile
	echo "$commandStr" >> $outFile
	echo "rc_command=\$?" >> $outFile

	echo "echo \$rc_command \" \"" >> $outFile
	echo "echo \"Job finished  at \`date\`\"" >> $outFile

	echo "exit \$rc_command" >> $outFile

	chmod +x $outFile

	###########################################################
	# Create PUT JOB
	#
	###########################################################

	log "Creating $putFile"

	echo "echo \" \"" >> $putFile
	echo "$putCommandStr" >> $putFile
	echo "rc_command=\$?" >> $putFile

	echo "echo \$rc_command \" \"" >> $putFile
	echo "echo \"Job finished  at \`date\`\"" >> $putFile

	echo "exit \$rc_command" >> $putFile

	chmod +x $putFile
	
done

################################################################################################
# Create Package Creation JOB - this is done once AFTER all the FIX are run
#
################################################################################################
fix_packaging_outFile=$outDir/${subject}_icafix_package.sh
log "fix_packaging_outFile: ${fix_packaging_outFile}"

if [ ! -f $fix_packaging_outFile ]
then
	touch $fix_packaging_outFile
	cat $fix_configurationForJobFile > $fix_packaging_outFile
	cat $logDirectiveFile >> $fix_packaging_outFile	

	xnat_hostRoot=`echo $xnat_host | awk '{gsub(/http:\/\//,""); gsub(/:8080\//,""); gsub(/https:\/\//,""); gsub(/\//,""); print}'`
	packaging_builddir=$builddir/packaging
	mkdir -p $packaging_builddir
	log "Creating ${fix_packaging_outFile}"

	packaging_commandStr=""
	packaging_commandStr+=" source $SCRIPTS_HOME/epd-python_setup.sh;"
	packaging_commandStr+=" source $SCRIPTS_HOME/groovy_setup.sh;"
	packaging_commandStr+=" $NRG_PACKAGES/tools/packaging/callPackager.sh"
	packaging_commandStr+=" --host $xnat_hostRoot"
	packaging_commandStr+=" --user $user"
	packaging_commandStr+=" --outDir $packaging_outdir"
	packaging_commandStr+=" --buildDir $builddir"
	packaging_commandStr+=" --project $project"
	packaging_commandStr+=" --subject $subject"
	packaging_commandStr+=" --outputFormat PACKAGE"
	packaging_commandStr+=" --outputType FIX"
	log "packaging_commandStr: ${packaging_commandStr}"
	packaging_commandStr+=" --pw $passwd"

	echo "echo \" \"" >> $fix_packaging_outFile
	echo "$packaging_commandStr" >> $fix_packaging_outFile
	echo "echo \" \"" >> $fix_packaging_outFile
	echo "echo \"Job finished  at \`date\`\"" >> $fix_packaging_outFile

	echo "exit 0;" >> $fix_packaging_outFile
	chmod +x $fix_packaging_outFile
fi

log "ENDING"
exit 0

