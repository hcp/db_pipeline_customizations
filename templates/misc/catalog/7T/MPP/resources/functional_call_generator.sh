#!/bin/bash -x
set -e

# This script will generate a job file for the 7T version of FunctionalHCP pipeline processing

# Inputs: 
#  path of param file
#  path to location where the job file would be generated

# The PBS/SGE statements are picked from a config file for the specific cluster

ARGS=6
program="$0"

log()
{
	msg=$@
	now=`date`
	echo "${now} - ${program} - ${msg}"
}

isSet()
{
	if [[ ! ${!1} && ${!1-_} ]]
	then
		log "$1 is not set, aborting."
		exit 1
	elif [ -z ${!1} ]
	then
		log "$1 has no value, aborting."
		exit 1
	fi
}

log "STARTING"

if [ $# -ne "$ARGS" ]
then
	echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outDir MPP_PARAM_FILE_PATH"
	exit 1
fi

paramsFile=$1
log "paramsFile: ${paramsFile}"

mpp_pipelineparamsFile=$2
log "mpp_pipelineparamsFile: ${mpp_pipelineparamsFile}"

passwd=$3

outDir=$4
log "outDir: ${outDir}"

mppParamsFile=$5
log "mppParamsFile: ${mppParamsFile}"

jsession=$6
log "jsession: ${jsession}"

source $paramsFile
source $mpp_pipelineparamsFile
source $mppParamsFile

PIPELINE_NAME="7T/Functional/FunctionalHCP.xml"
log "PIPELINE_NAME: ${PIPELINE_NAME}"

#Default to CHPC
if [ X$compute_cluster = X ]
then
	compute_cluster=CHPC
fi

dirname=`dirname "$program"`
pipeline_configdir="${dirname}/config"

if [ $compute_cluster = CHPC ]
then
	processingParamsFile=$pipeline_configdir/CHPC/functionalHCP.pbs.param
	logDirectiveFile=$pipeline_configdir/CHPC/log.pbs.config
elif [ $compute_cluster = NRG ]
then
	processingParamsFile=$pipeline_configdir/NRG/functionalHCP.sge.param
	logDirectiveFile=$pipeline_configdir/NRG/log.sge.config
fi

log "processingParamsFile: ${processingParamsFile}"
if [ ! -f $processingParamsFile ] ; then
	log "File at $processingParamsFile does not exist. Aborting!"
	exit 1;
fi

log "logDirectiveFile: ${logDirectiveFile}"
if [ ! -f $logDirectiveFile ] ; then
	log "File at $logDirectiveFile does not exist. Aborting!"
	exit 1;
fi

#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet functional_pa_fieldmapseries
isSet functional_ap_fieldmapseries
isSet functional_TE
isSet functional_DistortionCorrection
isSet functional_scanid[0]
isSet functional_functionalseries[0]
isSet functional_DwellTimes[0]
isSet functional_UnwarpDir[0]
isSet functional_fmrinames[0]

if [[ ${#functional_scanid[@]} -ne ${#functional_functionalseries[@]} || ${#functional_scanid[@]} -ne ${#functional_DwellTimes[@]} || ${#functional_scanid[@]} -ne ${#functional_UnwarpDir[@]} || ${#functional_scanid[@]} -ne ${#functional_fmrinames[@]} ]]
then
	log "The array lengths of variables functional_scanid, functional_functionalseries, functional_DwellTimes, functional_UnwarpDir, functional_fmrinames do not match. Aborting!"
	exit 1
fi

###########################################################
# Continue - looks good
#
###########################################################


configurationForPutJobFileName="functionalHCP_put"

index=0
for fscan in "${functional_scanid[@]}"
do
	# For each scan create the outfile of command to launch the Functional processing pipeline
	
	seriesDescription=${functional_functionalseries[$index]}
	configurationForJobFileName="functionalHCP_"${seriesDescription}
	fmriname=${functional_fmrinames[$index]}
	
	fsf_configurationForJobFileName="functionalHCP_fsf"
	
	seriesRoot=`echo $seriesDescription | awk '{gsub(/_PA/,""); gsub(/_AP/,""); print}'`
	
	if [ $compute_cluster = CHPC ]
	then
		configurationForJobFile=$pipeline_configdir/CHPC/${configurationForJobFileName}.pbs.config
		configurationForPutJobFile=$pipeline_configdir/CHPC/${configurationForPutJobFileName}.pbs.config
		fsf_configurationForJobFile=$pipeline_configdir/CHPC/${fsf_configurationForJobFileName}.pbs.config
		#processingParamsFile=$pipeline_configdir/CHPC/functionalHCP.pbs.param
	elif [ X$compute_cluster = NRG ]
	then
		configurationForJobFile=$pipeline_configdir/NRG/${configurationForJobFileName}.sge.config
		configurationForPutJobFile=$pipeline_configdir/NRG/${configurationForPutJobFileName}.sge.config
		fsf_configurationForJobFile=$pipeline_configdir/NRG/${fsf_configurationForJobFileName}.sge.config
		#processingParamsFile=$pipeline_configdir/NRG/functionalHCP.sge.param
	fi
	
	if [ ! -f $configurationForJobFile ]
	then
		log "File at $configurationForJobFile does not exist. Aborting!"
		exit 1;
	fi
	
	if [ ! -f $configurationForPutJobFile ]
	then
		log "File at $configurationForPutJobFile does not exist. Aborting!"
		exit 1;
	fi
	
	outFile=$outDir/${subject}_${seriesDescription}_functional.sh
	outPutJobFile=$outDir/${subject}_${seriesDescription}_functional_put.sh
	
	packaging_outFile=$outDir/${subject}_${seriesRoot}_end.sh
	
	touch $outFile
	touch $outPutJobFile
	
	if [ ! -f $outFile ]
	then
		log "File at $outFile does not exist. Aborting!"
		exit 1;
	fi
	
	if [ ! -f $outPutJobFile ]
	then
		log "File at $outPutJobFile does not exist. Aborting!"
		exit 1;
	fi
	
	
	cat $configurationForJobFile > $outFile
	cat $logDirectiveFile >> $outFile	
	
	cat $configurationForPutJobFile > $outPutJobFile
	cat $logDirectiveFile >> $outPutJobFile	
	
	
	ap_fieldmap=$functional_ap_fieldmapseries
	pa_fieldmap=$functional_pa_fieldmapseries
	dwellTime=${functional_DwellTimes[$index]}
	TE=$functional_TE
	unwarpDir=${functional_UnwarpDir[$index]}
	distortionCorrection=$functional_DistortionCorrection

	workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
	if [ $? -ne 0 ]
	then
		log "Fetching workflow failed. Aborting!"
		exit 1
	fi 

	commandStr=""
	commandStr+=" $PIPELINE_HOME/bin/XnatPipelineLauncher"
	commandStr+=" -pipeline $PIPELINE_NAME"
	commandStr+=" -project $project"
	commandStr+=" -id $xnat_id"
	commandStr+=" -dataType $dataType"
	commandStr+=" -host $xnat_host"
	commandStr+=" -parameter xnatserver=$xnatserver"
	commandStr+=" -parameter project=$project"
	commandStr+=" -parameter xnat_id=$xnat_id"
	commandStr+=" -label $label"
	commandStr+=" -u $user"
	commandStr+=" -supressNotification"
	commandStr+=" -notify $useremail"
	commandStr+=" -notify $adminemail"
	commandStr+=" -parameter adminemail=$adminemail"
	commandStr+=" -parameter useremail=$useremail"
	commandStr+=" -parameter mailhost=$mailhost"
	commandStr+=" -parameter userfullname=$userfullname"
	commandStr+=" -parameter builddir=$builddir"
	commandStr+=" -parameter sessionid=$sessionId"
	commandStr+=" -parameter subjects=$subject"
	commandStr+=" -parameter functionalscanid=$fscan"
	commandStr+=" -parameter TE=$TE"
	commandStr+=" -parameter functionalseries=$seriesDescription"
	commandStr+=" -parameter functionalfmriname=$fmriname"
	commandStr+=" -parameter pa_fieldmapseries=$pa_fieldmap"
	commandStr+=" -parameter ap_fieldmapseries=$ap_fieldmap"
	commandStr+=" -parameter DwellTime=$dwellTime"
	commandStr+=" -parameter UnwarpDir=$unwarpDir"
	commandStr+=" -parameter DistortionCorrection=$distortionCorrection"
	commandStr+=" -parameter templatesdir=$templatesdir"
	commandStr+=" -parameter configdir=$configdir"
	commandStr+=" -parameter CaretAtlasDir=$CaretAtlasDir"
	commandStr+=" -parameter compute_cluster=$compute_cluster"
	commandStr+=" -parameter packaging_outdir=$packaging_outdir"
	commandStr+=" -parameter cluster_builddir_prefix=$cluster_builddir_prefix"
	commandStr+=" -parameter db_builddir_prefix=$db_builddir_prefix"
	commandStr+=" -parameter structuralReferenceProject=$structuralReferenceProject"
	commandStr+=" -parameter structuralReferenceSession=$structuralReferenceSession"
	commandStr+=" -workFlowPrimaryKey $workflowID"
	# commandStr and putCommandStr logged without password to avoid having password in logs
	log "commandStr: ${commandStr}"

	putCommandStr="${commandStr} -startAt 26"
	log "putCommandStr: ${putCommandStr}"

	# password added to both commandStr and putCommandStr after logging
	commandStr+=" -pwd $passwd"
	putCommandStr+=" -pwd $passwd"

	log "Creating $outFile"

	echo "echo \" \"" >> $outFile
	echo "$commandStr" >> $outFile
	echo "rc_command=\$?" >> $outFile

	echo "echo \$rc_command \" \"" >> $outFile
	echo "echo \"Job finished  at \`date\`\"" >> $outFile

	echo "exit \$rc_command" >> $outFile

	chmod +x $outFile

	###########################################################
	# Create PUT JOB
	#
	###########################################################

	log "Creating $outPutJobFile"

	echo "echo \" \"" >> $outPutJobFile
	echo "$putCommandStr" >> $outPutJobFile
	echo "rc_command=\$?" >> $outPutJobFile

	echo "echo \$rc_command \" \"" >> $outPutJobFile
	echo "echo \"Job finished  at \`date\`\"" >> $outPutJobFile

	echo "exit \$rc_command" >> $outPutJobFile

	chmod +x $outPutJobFile

	###########################################################
	# Create Package Creation JOB
	#
	###########################################################

	if [ ! -f $packaging_outFile ]
	then
		touch $packaging_outFile
		cat $fsf_configurationForJobFile > $packaging_outFile
		cat $logDirectiveFile >> $packaging_outFile

		xnat_hostRoot=`echo $xnat_host | awk '{gsub(/http:\/\//,""); gsub(/:8080\//,""); gsub(/https:\/\//,""); gsub(/\//,""); print}'`
		log "Creating $packaging_outFile"

		source /home/HCPpipeline/SCRIPTS/SetUpHCPPipeline_7T.sh
		pipeline_version_number=`cat ${HCPPIPEDIR}/version.txt`

		packaging_commandStr="$NRG_PACKAGES/tools/packaging/CreatePackage.sh"
		packaging_commandStr+=" --server=${xnat_hostRoot}"
		packaging_commandStr+=" --username=${user}"
		packaging_commandStr+=" --pipeline-project=7T"
		packaging_commandStr+=" --project=${project}"
		packaging_commandStr+=" --subject=${subject}"
		packaging_commandStr+=" --session=${subject}_7T"
		packaging_commandStr+=" --processing-type=preproc"
		packaging_commandStr+=" --scan-type=${seriesRoot}"
		packaging_commandStr+=" --destination-dir=${packaging_outdir}/${project}/${subject}/preproc"
		packaging_commandStr+=" --package-type=cinab"
		packaging_commandStr+=" --pipeline-version-number=${pipeline_version_number}"

		# packaging_commandStr logged without password to avoid having password in logs
		echo "packaging_commandStr: ${packaging_commandStr}"

		# password added after logging
		packaging_commandStr+=" --password=${passwd}"

		unproc_packaging_commandStr="$NRG_PACKAGES/tools/packaging/CreatePackage.sh"
		unproc_packaging_commandStr+=" --server=${xnat_hostRoot}"
		unproc_packaging_commandStr+=" --username=${user}"
		unproc_packaging_commandStr+=" --pipeline-project=7T"
		unproc_packaging_commandStr+=" --project=${project}"
		unproc_packaging_commandStr+=" --subject=${subject}"
		unproc_packaging_commandStr+=" --session=${subject}_7T"
		unproc_packaging_commandStr+=" --processing-type=unproc"
		unproc_packaging_commandStr+=" --scan-type=${seriesRoot}"
		unproc_packaging_commandStr+=" --destination-dir=${packaging_outdir}/${project}/${subject}/unproc"
		unproc_packaging_commandStr+=" --package-type=cinab"

		# packaging_commandStr logged without password to avoid having password in logs
		echo "unproc_packaging_commandStr: ${unproc_packaging_commandStr}"
		
		# password added after logging
		unproc_packaging_commandStr+=" --password=${passwd}"

		echo "echo \" \"" >> $packaging_outFile
		echo "$packaging_commandStr" >> $packaging_outFile
		
		echo "echo \" \"" >> $packaging_outFile
		echo "$unproc_packaging_commandStr" >> $packaging_outFile
		
		echo "echo \" \"" >> $packaging_outFile
		echo "echo \"Job finished  at \`date\`\"" >> $packaging_outFile
		
		echo "exit 0;" >> $packaging_outFile
		chmod +x $packaging_outFile
	fi

	index=$((index+1))

done

log "ENDING"
exit 0
