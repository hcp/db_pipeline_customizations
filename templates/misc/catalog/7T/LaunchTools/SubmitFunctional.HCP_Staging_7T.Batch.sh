#!/bin/bash

set_structural_reference_project()
{
	if [ -d /data/hcpdb/archive/HCP_500/arc001/${subject}_3T/RESOURCES/Structural_preproc ]
	then
		structural_reference_project="HCP_500"
	elif [ -d /data/hcpdb/archive/HCP_Staging/arc001/${subject}_3T/RESOURCES/Structural_preproc ]
	then
		structural_reference_project="HCP_Staging"
	else
		echo "NO STRUCTURAL REFERENCE PROJECT FOR SUBJECT ${subject} FOUND"
		exit 1
	fi
}

launch_subject()
{
	echo ""
	echo "-----------------------------------------------------------------------------------"
	echo " Launching Functional Preprocessing for subject: ${subject} in project: ${project}"
	echo " With Structural Reference Project: ${structural_reference_project}"
	echo " On shadow number: ${shadow_number}"
	echo "-----------------------------------------------------------------------------------"
	echo ""
	
	python launch7TMpp.py \
		--username="${userid}" \
		--password="${password}" \
		--project="${project}" \
		--subject="${subject}" \
		--session="${subject}_7T" \
		--functional \
		--structural-reference-project="${structural_reference_project}" \
		--structural-reference-session="${subject}_3T" \
		--debug \
		--shadow-number="${shadow_number}" \
		--go
}

if [ -z "${SUBJECT_FILES_DIR}" ]
then
	echo "Environment variable SUBJECT_FILES_DIR must be set!"
	exit 1
fi

printf "Connectome DB Username: "
read userid

stty -echo
printf "Connectome DB Password: "
read password
echo ""
stty echo

project="HCP_Staging_7T"
structural_reference_project="HCP_500"

subject_file_name="${SUBJECT_FILES_DIR}/${project}.functional.subjects"
echo "Retrieving subject list from: ${subject_file_name}"
subject_list_from_file=( $( cat ${subject_file_name} ) )
subjects="`echo "${subject_list_from_file[@]}"`"

start_shadow_number=1
max_shadow_number=8

shadow_number=${start_shadow_number}

export PYTHONPATH=../../python_utils

for subject in ${subjects}
do
	set_structural_reference_project
	
	case ${subject} in
		StructRef=*)
			structural_reference_project=${subject/*=/""}
			;;
		StopProcessing*)
			exit;
			;;
		*)
			launch_subject
			;;
	esac

	shadow_number=$((shadow_number+1))
	
	if [ "$shadow_number" -gt "$max_shadow_number" ]
	then
		shadow_number=${start_shadow_number}
	fi
	
done
