#!/bin/bash

printf "Connectome DB Username: "
read userid

stty -echo
printf "Connectome DB Password: "
read password
echo ""
stty echo

export PYTHONPATH=../../python_utils

python launch7TMpp.py \
	--username="${userid}" \
	--password="${password}" \
	--project=PipelineTest \
	--subject=132118 \
	--session=132118_7T \
	--functional \
	--structural-reference-project=HCP_500 \
	--structural-reference-session=132118_3T \
	--debug \
	--shadow-number 1 \
	--go
