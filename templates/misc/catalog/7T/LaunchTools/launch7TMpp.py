'''
@author: Timothy B. Brown (tbbrown@wustl.edu)
'''

import sys
import subprocess
import argparse 
import validate
import database_credentials
import xnat_access
import debug_util
import hcp_scan_utils
import hcp_mpp_utils

from logger import Logger
from mpp_parameters_7T import MppParameters7T
from functional_parameters_7T import FunctionalParameters7T
from diffusion_parameters_7T import DiffusionParameters7T
from ica_fix_parameters_7T import IcaFixParameters7T

# Constants

# Current version number for this program
PROGRAM_VERSION = '1.0.0' 

# XNAT Session type string for valid sessions to use
PROCESSING_SESSION_TYPE = 'xnat:mrSessionData'

# Scan types for functional preprocessing
FUNCTIONAL_SCAN_TYPES = ['tfMRI', 'rfMRI']

# File name extension for compressed NIFTI format files
COMPRESSED_NIFTI_EXTENSION = '.nii.gz'

def retrieve_params( ):
	"""
	Parse the user specified command-line parameters
	"""
	
	parser = argparse.ArgumentParser(
		description="Script to generate proper parameters and command for XNAT MPP for 7T launching ...")
	
	parser.add_argument("-d", "--debug", dest="debug", action='store_true')
	parser.add_argument("-v", "--version", action='version', version="%(prog)s " + PROGRAM_VERSION)
	parser.add_argument("-s", "--server", dest="server", default="db.humanconnectome.org", type=str,
					help="database server system, defaults to db.humanconnectome.org")
	parser.add_argument("-u", "--username", dest="username", required=True, type=str)
	parser.add_argument("-pw", "--password", dest="password", required=True, type=str)
	parser.add_argument("-pr", "--project", dest="project", required=True, type=str,
					help="project in which to run pipeline, e.g. PipelineTest, HCP_Staging, WU_L1A_Staging, etc.")
	parser.add_argument("-su", "--subject", dest="subject", required=True, type=str)
	parser.add_argument("-se", "--session", dest="session", required=True, type=str, help="Session ID")
	parser.add_argument("-fu", "--functional", dest="functional", action='store_true',
					help="specifying this argument means that functional preprocessing is to be done")
	parser.add_argument("-di", "--diffusion", dest="diffusion", action='store_true',
					help="specifying this argument means the diffusion preprocessing is to be done")
	parser.add_argument("-dd", "--diffusion-direction-numbers", dest="diffusion_direction_numbers", type=str,
					default="80,81", help="comma separated list of diffusion scan direction numbers")
	parser.add_argument("-ic", "--icafix", dest="icafix", action='store_true',
					help="specifying this argument means that ICA FIX processing is to be done")
	parser.add_argument("-srpr", "--structural-reference-project", dest="structural_reference_project", 
					required=True, type=str)
	parser.add_argument("-srse", "--structural-reference-session", dest="structural_reference_session",
					required=True, type=str)
	parser.add_argument("-sn", "--shadow-number", dest="shadow_number", 
					choices=('1', '2', '3', '4', '5', '6', '7', '8'),
					default='5', type=str, help="Database shadow number")
	parser.add_argument("-pp", "--pipeline-project", dest="pipeline_project", default="7T", type=str,
					help="Pipeline project - determines which Pipeline Project to use")
	parser.add_argument("-g", "--go", dest="go", action='store_true')
	
	args = parser.parse_args()
	return args

def show_retrieved_params( parameters ):
	"""
	Display (on stdout) the user specified program input parameters
	"""
	logger.log("Input Parameters")
	logger.log("\tdebug: " + str(parameters.debug))
	logger.log("\tserver: " + str(parameters.server))
	logger.log("\tusername: " + str(parameters.username))
	logger.log("\tproject: " + str(parameters.project))
	logger.log("\tsubject: " + str(parameters.subject))
	logger.log("\tsession: " + str(parameters.session))
	logger.log("\tfunctional: " + str(parameters.functional))
	logger.log("\tdiffusion: " + str(parameters.diffusion))
	logger.log("\tdiffusion_direction_numbers: " + str(parameters.diffusion_direction_numbers))
	logger.log("\ticafix: " + str(parameters.icafix))
	logger.log("\tstructural_reference_project: " + str(parameters.structural_reference_project))
	logger.log("\tstructural_reference_session: " + str(parameters.structural_reference_session))
	logger.log("\tshadow-number: " + str(parameters.shadow_number))
	logger.log("\tgo: " + str(parameters.go))

# main functionality

def main():
	
	# setup logging
	global logger
	logger = Logger()
	
	# get and show the command line parameters
	args = retrieve_params()
	show_retrieved_params(parameters=args)
	
	# setup debugging and logging
	debug = debug_util.DebugUtil("launch7TMpp")
	debug.show_debug_msgs = args.debug
	
	# create token credentials
	logger.log("Requesting token database credentials")
	db_credentials = database_credentials.DatabaseCredentials(args.server, args.username, args.password)
	db_credentials = database_credentials.get_token_credentials(db_credentials)
	
	debug.show("db_credentials.server: " + str(db_credentials.server))
	debug.show("db_credentials.username: " + str(db_credentials.username))
	debug.show("db_credentials.password: " + str(db_credentials.password))
	
	# connect to db server for project access
	logger.log("Connecting to: " + db_credentials.server + " for project access")
	project_xnat_access = xnat_access.XnatAccess(db_credentials.server, db_credentials.username, db_credentials.password)
	project_xnat_access.project = args.project
	project_xnat_access.subject = args.subject
	session_type = project_xnat_access.get_session_type(args.session)
	
	msg = "Session: " + args.session + " in project " + args.project + " is not of correct type"
	if (not validate.check_equal(actual=session_type, expected=PROCESSING_SESSION_TYPE, fail_message=msg)):
		sys.exit(1)
	
	project_xnat_access.session = args.session
	
	# connect to db server for reference project access
	logger.log("Connecting to: " + db_credentials.server + " for structural reference project access")
	reference_project_xnat_access = xnat_access.XnatAccess(db_credentials.server, db_credentials.username, db_credentials.password)
	reference_project_xnat_access.project = args.structural_reference_project
	reference_project_xnat_access.subject = args.subject
	reference_session_type = reference_project_xnat_access.get_session_type(args.structural_reference_session)
	
	msg = "Session: " + args.structural_reference_session + " in project " + args.structural_reference_project + \
		" is not of correct type"
	if (not validate.check_equal(actual=reference_session_type, expected=PROCESSING_SESSION_TYPE, fail_message=msg)):
		sys.exit(1)
	
	reference_project_xnat_access.session = args.structural_reference_session
	
	# create and configure appropriate mpp parameters object
	mpp_parameters = MppParameters7T()
	
	mpp_parameters.debug = args.debug
	mpp_parameters.subject = args.subject
	mpp_parameters.session = args.session
	mpp_parameters.xnat_session_id = project_xnat_access.xnat_session_id
	mpp_parameters.structural_reference_project = args.structural_reference_project
	mpp_parameters.structural_reference_session = args.structural_reference_session
	mpp_parameters.project = args.project
	mpp_parameters.launch_structural = False 
	mpp_parameters.launch_functional = args.functional
	mpp_parameters.launch_diffusion = args.diffusion
	mpp_parameters.launch_ica_fix = args.icafix
	mpp_parameters.launch_task_analysis = False
	
	# gather functional preprocessing parameters if required
	if (args.functional):
		functional_parameters = FunctionalParameters7T()
		logger.log("Determining parameters for Functional Preprocessing")
		
		resource_label_list = project_xnat_access.get_resource_label_list()
		
		# cycle through all the resource labels
		for label in resource_label_list:
			debug.show("checking resource labeled: " + str(label))
			project_xnat_access.resource = label
			file_name_to_scan_number = hcp_scan_utils.create_resource_file_name_to_scan_number_map(project_xnat_access)
			
			# if the resource is an "unprocessed" resource
			if hcp_scan_utils.is_unprocessed_resource(label):
				resource_base_name = hcp_scan_utils.get_unprocessed_resource_base_name(label)
				debug.show("unprocessed resource base name: " + str(resource_base_name))
				
				# determine the name of the "primary" scan that should be part of the resource
				primary_scan_name = hcp_scan_utils.get_primary_scan_name_for_resource(project_xnat_access, resource_base_name)
				debug.show("primary_scan_name: " + str(primary_scan_name))
				
				# if that "primary" scan exists, get its scan number so we can find out some things about it
				if file_name_to_scan_number.has_key(primary_scan_name):
					scan_number = file_name_to_scan_number[primary_scan_name]
					debug.show("scan_number: " + scan_number)
					scan_type = project_xnat_access.get_scan_data_field_value(scan_number, 'type')
					debug.show("scan_type: " + scan_type)
					
					# if the "primary" scan is a functional scan (as determined by its scan type),
					# then it is a scan we want to put through functional preprocessing
					if scan_type in FUNCTIONAL_SCAN_TYPES:
						logger.log("Setting parameters for resource: " + resource_base_name)
						dwell_time = project_xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')
						pe_direction = project_xnat_access.get_scan_data_field_value(scan_number, 'parameters/peDirection')
						
						# The pe_direction is returned from the DB as +x, -x, +y, -y, +z, or -z,
						# but the pipeline expects positive directions to NOT be prefaced with +.
						# So we need to strip the plus sign off the front if it is there.
						if pe_direction.startswith('+'):
							pe_direction = pe_direction[1:]
						
						debug.show("dwell_time: " + str(dwell_time))
						debug.show("pe_direction: " + str(pe_direction))
						
						functional_parameters.add_scan_info(scan_number, resource_base_name, dwell_time, str(pe_direction))
		
		# finished configuring the functional parameters, so assign them to be the functional parameters for this mpp run
		mpp_parameters.functional_parameters = functional_parameters
	
	# gather diffusion preprocessing parameters if required
	if (args.diffusion):
		diffusion_parameters = DiffusionParameters7T()
		diffusion_parameters.session = args.session
		logger.log("Determining parameters for Diffusion Preprocessing")
		
		resource_label_list = project_xnat_access.get_resource_label_list()
		
		# cycle through all the resource labels
		for label in resource_label_list:
			debug.show("checking resource labeled: " + str(label))
			project_xnat_access.resource = label
			file_name_to_scan_number = hcp_scan_utils.create_resource_file_name_to_scan_number_map(project_xnat_access)
			
			# if the resource is an "unprocessed" resource
			if hcp_scan_utils.is_unprocessed_resource(label):
				resource_base_name = hcp_scan_utils.get_unprocessed_resource_base_name(label)
				debug.show("unprocessed resource base name: " + str(resource_base_name))
				
				# if this is a "Diffusion" resource
				if (resource_base_name == "Diffusion"):
					for scan_dir_number in ((args.diffusion_direction_numbers.split(','))):
						for direction in (("AP,PA".split(','))):
							full_scan_name = project_xnat_access.session + '_' + 'DWI_dir' + scan_dir_number + '_' + direction + COMPRESSED_NIFTI_EXTENSION
							debug.show("full scan name: " + str(full_scan_name))
							if file_name_to_scan_number.has_key(full_scan_name):
								scan_number = file_name_to_scan_number[full_scan_name]
								debug.show("scan_number: " + scan_number)
								echo_spacing = float(project_xnat_access.get_scan_data_field_value(scan_number, 'parameters/echoSpacing')) * 1.0e+3
								debug.show("echo_spacing: " + str(echo_spacing))
								diffusion_parameters.add_scan(scan_number, direction, scan_dir_number, echo_spacing)
								
			
			
		# finished configuring diffusion parameters, so assign them to be the diffusion parameters for this mpp run
		debug.show("finished configuring diffusion parameters, so assign them to be the diffusion parameters for this mpp run")
		mpp_parameters.diffusion_parameters = diffusion_parameters
	
	# gather ICA FIX processing parameters if required
	if (args.icafix):
		ica_fix_parameters = IcaFixParameters7T()
		logger.log("Determining parameters for ICA FIX processing")

		resource_label_list = project_xnat_access.get_resource_label_list()

		# cycle through all the resource labels
		for label in resource_label_list:
			debug.show("checking resource labeled: " + str(label))
			project_xnat_access.resource = label
			file_name_to_scan_number = hcp_scan_utils.create_resource_file_name_to_scan_number_map(project_xnat_access)
			
			# if the resource is and "unprocessed" resourse
			if hcp_scan_utils.is_unprocessed_resource(label):
				resource_base_name = hcp_scan_utils.get_unprocessed_resource_base_name(label)
				debug.show("unprocessed resource base name: " + str(resource_base_name))
				
				# determine the name of the "primary" scan that should be part of the resource
				primary_scan_name = hcp_scan_utils.get_primary_scan_name_for_resource(project_xnat_access, resource_base_name)
				debug.show("primary_scan_name: " + str(primary_scan_name))
				
				if file_name_to_scan_number.has_key(primary_scan_name):
					scan_number = file_name_to_scan_number[primary_scan_name]
					debug.show("scan_number: " + scan_number)
					scan_type = project_xnat_access.get_scan_data_field_value(scan_number, 'type')
					debug.show("scan_type: " + scan_type)
					
					# if the "primary" scan is a functional scan (as determined by its scan type),
					# then it is a scan we want to put through ICA FIX processing
					if scan_type in FUNCTIONAL_SCAN_TYPES:
						logger.log("Setting parameters for resource: " + resource_base_name)
						ica_fix_parameters.add_scan(resource_base_name)

		# finished configuring ICA FIX parameters, so assign them to be the ICA FIX parameters for this mpp run
		debug.show("finished configuring ICA FIX parameters, so assign them to be the ICA FIX parameters for this mpp run")
		mpp_parameters.ica_fix_parameters = ica_fix_parameters
	
	# show gathered MPP Parameters
	mpp_parameters.show()
	
	# validate parameters for use with MPP
	msg = "Invalid MPP parameters"
	if not validate.check_true(test_value=mpp_parameters.valid(), fail_message=msg):
		sys.exit(1)
	
	# Save parameters to XML file
	timestamped_subject_dir = hcp_mpp_utils.get_timestamped_subject_dir(mpp_parameters.subject)
	parameter_file_path = hcp_mpp_utils.get_parameter_file_path(mpp_parameters.project, timestamped_subject_dir, mpp_parameters.subject)
	logger.log("Saving MPP parameters to XML file: " + parameter_file_path)
	mpp_parameters.save_to_xml_file(parameter_file_path)
	
	current_jsession=project_xnat_access.get_jsession_id()
	
	# build launching command
	submit_str = hcp_mpp_utils.build_pipeline_submission_str(
															user=db_credentials.username,
															password=db_credentials.password,
															server=db_credentials.server,
															project=project_xnat_access.project,
															xnat_session_id=project_xnat_access.xnat_session_id,
															subject=project_xnat_access.subject,
															shadow_number=args.shadow_number,
															pipeline_project=args.pipeline_project,
															timestamped_subject_dir=timestamped_subject_dir,
															parameter_file_path=parameter_file_path,
															current_jsession=current_jsession)
	
	logger.log("Submission String: ")
	logger.log(submit_str)
	
	if (args.go):
		subprocess.call(submit_str, shell=True)

# start as an application

if __name__ == "__main__":
	main()

