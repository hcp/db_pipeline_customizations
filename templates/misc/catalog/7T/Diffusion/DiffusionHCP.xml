<?xml version="1.0" encoding="UTF-8"?>
<Pipeline
	xmlns="http://nrg.wustl.edu/pipeline"
	xmlns:xi="http://www.w3.org/2001/XInclude"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd"
	xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
	
	<name>DiffusionHCP</name>
	<location>DiffusionHCP</location>
	<description>7T version of Diffusion preprocessing pipeline: minimally preprocessed diffusion data.</description>
	
	<xnatInfo appliesTo="xnat:mrSessionData"/>
	<outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(), 'DiffusionHCP')^</outputFileNamePrefix>

	<parameters>
		<parameter>
			<name>xnat_host</name>
			<values>
				<unique>^if (count(/Pipeline/parameters/parameter[name='alias_host']/values/unique/text())>0) then /Pipeline/parameters/parameter[name='alias_host']/values/unique/text() else /Pipeline/parameters/parameter[name='host']/values/unique/text()^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>xnat_host_root1</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text(),'http://','')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>xnat_host_root2</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root1']/values/unique/text(),':8080/','')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>xnat_host_root3</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root2']/values/unique/text(),'https://','')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>xnat_host_root</name>
			<values>
				<unique>^replace(/Pipeline/parameters/parameter[name='xnat_host_root3']/values/unique/text(),'/','')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>diffusion_builddir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(), '/DIFFUSION/')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>diffusion_temp_builddir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text(), '/OUT')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>start_time_file</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text(), 'processing_start.time')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text(), '/', /Pipeline/parameters/parameter[name='subjects']/values/unique/text(), '/')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>logdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text(), 'logs/')^</unique>
			</values>
		</parameter>
		
		<parameter>
			<name>db_workdir</name>
			<values>
				<unique>^concat(replace(/Pipeline/parameters/parameter[name='diffusion_temp_builddir']/values/unique/text(),/Pipeline/parameters/parameter[name='cluster_builddir_prefix']/values/unique/text(),/Pipeline/parameters/parameter[name='db_builddir_prefix']/values/unique/text()), '/')^</unique>
			</values>
		</parameter>
	</parameters>

	<steps>
		<step id="0" description="Create work directory" >
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>^/Pipeline/parameters/parameter[name='diffusion_temp_builddir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>

		<step id="0a" description="Create a T1w directory" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>T1w</value>
				</argument>
			</resource>
		</step>
		
		<step id="1" description="Run getHCPResources and save results">
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceProject']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceSession']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Structural_preproc</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="ResourcePath">
					<value>^concat('T1w/', /Pipeline/parameters/parameter[name='subjects']/values/unique/text(), '/*')^</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="2" description="Run getHCPResources and save results" >
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceProject']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceSession']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Structural_preproc</value>
				</argument>
				<argument id="ResourcePath">
					<value>T1w/</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="3" description="Run getHCPResources and save results" >
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceProject']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceSession']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Structural_preproc</value>
				</argument>
				<argument id="ResourcePath">
					<value>T1w/xfms/</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="4" description="Run getHCPResources and save results" >
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceProject']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='structuralReferenceSession']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Structural_preproc</value>
				</argument>
				<argument id="ResourcePath">
					<value>MNINonLinear/xfms/*</value>
				</argument>
				<argument id="Flatten">
					<value>False</value>
				</argument>
				<argument id="DestinationDir">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="5" description="Create a diffusion data directory" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="mkdir" location="commandlineTools">
				<argument id="p">
				</argument>
				<argument id="dirname">
					<value>DiffData</value>
				</argument>
			</resource>
		</step>
		
		<step id="6" description="Run getHCPResources and save results" >
			<resource name="getHCPResources" location="ToolsHCP/resources">
				<argument id="Server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text()^</value>
				</argument>
				<argument id="User">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="Password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="Subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="Project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="Session">
					<value>^/Pipeline/parameters/parameter[name='sessionid']/values/unique/text()^</value>
				</argument>
				<argument id="Resource">
					<value>Diffusion_unproc</value>
				</argument>
				<argument id="ResourcePath">
					<value>"*"</value>
				</argument>
				<argument id="DestinationDir">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(), 'DiffData/')^</value>
				</argument>
			</resource>
		</step>
		
		<step id="6a" description="Get the start-time" >
			<resource name="echo" location="commandlineTools">
				<argument id="string">
					<value>"$(date)"</value>
				</argument>
				<argument id="redirect_stdout_append">
					<value>^/Pipeline/parameters/parameter[name='start_time_file']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="7a"
			description="Run Diffusion Preprocessing PreEDDY Script"
			awaitApprovalToProceed="true"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="DiffPreprocPipeline_PreEddy" location="7T/Diffusion/resources">
				<argument id="negData">
					<value>^/Pipeline/parameters/parameter[name='negData']/values/unique/text()^</value>
				</argument>
				<argument id="posData">
					<value>^/Pipeline/parameters/parameter[name='posData']/values/unique/text()^</value>
				</argument>
				<argument id="path">
					<value>^/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="echospacing">
					<value>^/Pipeline/parameters/parameter[name='EchoSpacing']/values/unique/text()^</value>
				</argument>
				<argument id="PEdir">
					<value>^/Pipeline/parameters/parameter[name='PhaseEncodingDir']/values/unique/text()^</value>
				</argument>
				<argument id="b0maxbval">
					<value>100</value>
				</argument>
				<argument id="dwiname">
					<value>^/Pipeline/parameters/parameter[name='dwiname']/values/unique/text()^</value>
				</argument>
				<argument id="printcom">
					<value>''</value>
				</argument>
			</resource>
		</step>
		
		<step id="7b" 
			description="Run Diffusion Preprocessing EDDY Script"
			awaitApprovalToProceed="true"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="DiffPreprocPipeline_Eddy" location="7T/Diffusion/resources">
				<argument id="path">
					<value>^/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="dwiname">
					<value>^/Pipeline/parameters/parameter[name='dwiname']/values/unique/text()^</value>
				</argument>
				<argument id="printcom">
					<value>''</value>
				</argument>
			</resource>
		</step>
		
		<step id="7c"
			description="Run Diffusion Preprocessing POSTEDDY Script"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="DiffPreprocPipeline_PostEddy" location="7T/Diffusion/resources">
				<argument id="path">
					<value>^/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="gdcoeffs">
					<value>^concat(/Pipeline/parameters/parameter[name='configdir']/values/unique/text(), 'coeff_SC72C_Skyra.grad')^</value>
				</argument>
				<argument id="dwiname">
					<value>^/Pipeline/parameters/parameter[name='dwiname']/values/unique/text()^</value>
				</argument>
				<argument id="printcom">
					<value>''</value>
				</argument>
			</resource>
		</step>
		
		<step id="8" description="Remove the downloaded data">
			<resource name="rm" location="commandlineTools">
				<argument id="file">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(), 'DiffData/')^</value>
				</argument>
				<argument id="r" />
			</resource>
		</step>
		
		<step id="9" description="Remove the downloaded T1w/subjects dir">
			<resource name="rm" location="commandlineTools">
				<argument id="file">
					<value>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(), 'T1w/', /Pipeline/parameters/parameter[name='subjects']/values/unique/text())^</value>
				</argument>
				<argument id="r" />
			</resource>
		</step>
		
		<step id="10" description="Stripout password info from provenance file" >
			<resource name="ParseProvenanceResource" location="ToolsHCP/resources">
				<argument id="InputDirectoryFile">
					<value>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(), /Pipeline/name/text(), '_', /Pipeline/name/text(), '.xml')^</value>
				</argument>
			</resource>
		</step>

		<step id="11"
			description="Copy Log files"
			workdirectory="^/Pipeline/parameters/parameter[name='logdir']/values/unique/text()^">
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '*.err')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '*.log')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
			<resource name="cp" location="commandlineTools">
				<argument id="source">
					<value>^concat(/Pipeline/name/text(), '_Provenance.xml')^</value>
				</argument>
				<argument id="destination">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		
		<step id="11a"
			description="Copy ONLY processed data" 
			awaitApprovalToProceed="true"
			workdirectory="^/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text()^">
			<resource name="CopyFilesCreatedSinceGivenTimeResource" location="ToolsHCP/resources">
				<argument id="workingdirectory">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
				<argument id="timestampfile">
					<value>^/Pipeline/parameters/parameter[name='start_time_file']/values/unique/text()^</value>
				</argument>
				<argument id="foldertomoveto">
					<value>^/Pipeline/parameters/parameter[name='diffusion_temp_builddir']/values/unique/text()^</value>
				</argument>
				<argument id="filelist">
					<value>^concat(/Pipeline/parameters/parameter[name='diffusion_builddir']/values/unique/text(),'generated_file.lst')^</value>
				</argument>
			</resource>
		</step>
		
		<step id="12"
			description="Delete previous output files"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="XnatDataClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>DELETE</value>
				</argument>
				<argument id="remote">
					<value>
						^concat('"',
								/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text(),
								'/REST/projects/',
								/Pipeline/parameters/parameter[name='project']/values/unique/text(),
								'/subjects/',
								/Pipeline/parameters/parameter[name='subjects']/values/unique/text(),
								'/experiments/',
								/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),
								'/resources/Diffusion_preproc"')^
					</value>
				</argument>
			</resource>
		</step>
		
		<step id="13"
			description="Upload diffusion output files to XNAT"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="XnatDataClient" location="xnat_tools">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="method">
					<value>PUT</value>
				</argument>
				<argument id="remote">
					<value>
						^concat('"',
								/Pipeline/parameters/parameter[name='xnat_host']/values/unique/text(),
								'REST/projects/',
								/Pipeline/parameters/parameter[name='project']/values/unique/text(),
								'/subjects/',
								/Pipeline/parameters/parameter[name='subjects']/values/unique/text(),
								'/experiments/',
								/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),
								'/resources/Diffusion_preproc/files?reference=',
								/Pipeline/parameters/parameter[name='db_workdir']/values/unique/text(),
								'"')^
					</value>
				</argument>
			</resource>
		</step>
		
		<step id="14"
			description="Create Package"
			workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="CreatePackageResource" location="ToolsHCP/resources">
				<argument id="server">
					<value>^/Pipeline/parameters/parameter[name='xnat_host_root']/values/unique/text()^</value>
				</argument>
				<argument id="username">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="pipeline_project">
					<value>7T</value>
				</argument>
				<argument id="project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subjects']/values/unique/text()^</value>
				</argument>
				<argument id="session">
					<value>^concat(/Pipeline/parameters/parameter[name='subjects']/values/unique/text(), '_7T')^</value>
				</argument>
				<argument id="processing_type">
					<value>preproc</value>
				</argument>
				<argument id="scan_type">
					<value>Diffusion</value>
				</argument>
				<argument id="destination_dir">
					<value>^concat(
					  /Pipeline/parameters/parameter[name='packaging_outdir']/values/unique/text(), '/',
					  /Pipeline/parameters/parameter[name='project']/values/unique/text(), '/',
					  /Pipeline/parameters/parameter[name='subjects']/values/unique/text(), '/preproc')^</value>
				</argument>
				<argument id="package_type">
					<value>cinab</value>
				</argument>
			</resource>
		</step>
		
		<step id="CLEANUP" description="Remove the workdir">
			<resource name="rm" location="commandlineTools">
				<argument id="file">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
				<argument id="r" />
			</resource>
		</step>
		
		<step id="END-Notify" description="Notify">
			<resource name="Notifier" location="notifications">
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="password">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="to">
					<value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
				</argument>
				<argument id="cc">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="from">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^concat(/Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(), ' Diffusion HCP Pipeline update: NIFTI files generated for ', /Pipeline/parameters/parameter[name='subjects']/values/unique/text(), ' ', /Pipeline/parameters/parameter[name='sessionid']/values/unique/text() )^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
				</argument>
				<argument id="body">
					<value>
						^concat('Dear ', /Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', ' results  files have been generated for ', /Pipeline/parameters/parameter[name='subjects']/values/unique/text(),' from the Diffusion HCP Pipeline. Details of the session are available  &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'"&gt;', ' here. &lt;/a&gt; &lt;/p&gt;&lt;br&gt;', ' &lt;/p&gt;&lt;br&gt;', /Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(),' Team.')^
					</value>
				</argument>
			</resource>
		</step>

	</steps>
</Pipeline>
