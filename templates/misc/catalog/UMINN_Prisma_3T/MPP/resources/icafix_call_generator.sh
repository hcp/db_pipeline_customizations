#!/bin/bash

#This script will generate a job file for ICA+FIX analysis 

#DB ICA+FIX Analysis Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster

isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}


ARGS=6
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile XNAT_Password Path_to_outFile"
  exit 1
fi


paramsFile=$1
mpp_pipelineparamsFile=$2
passwd=$3
outDir=$4
mppParamsFile=$5
jsession=$6

PIPELINE_NAME="UMINN_Prisma_3T/rfMRIFIX/rfMRIFIXHCP.xml"

dirname=`dirname "$program"`
configdir="${dirname}/config"


source $paramsFile
source $mpp_pipelineparamsFile
source $mppParamsFile


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi

if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/icafix.pbs.config  
   putConfigurationForJobFile=$configdir/CHPC/icafix_put.pbs.config  
   fix_configurationForJobFile=$configdir/CHPC/icafix_packaging.pbs.config  
   logDirectiveFile=$configdir/CHPC/log.pbs.config	
elif [ $compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/NRG/icafix.sge.config
   putConfigurationForJobFile=$configdir/NRG/icafix_put.sge.config  
   fix_configurationForJobFile=$configdir/NRG/icafix_packaging.sge.config  
   logDirectiveFile=$configdir/NRG/log.sge.config	
fi

if [ ! -f $configurationForJobFile ] ; then
  echo "File at $configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $putConfigurationForJobFile ] ; then
  echo "File at $puConfigurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $fix_configurationForJobFile ] ; then
  echo "File at $fix_configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi


if [ ! -f $logDirectiveFile ] ; then
  echo "File at $logDirectiveFile doesnt exist. Aborting!"
  exit 1;
fi



###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet icafix_bp
isSet icafix_functseries

###########################################################
# Continue - looks good
#
###########################################################

index=0
for fscan in "${icafix_functseries[@]}"
do

       #For each scan create the outfile of command to launch the Functional processing pipeline
	outFile=$outDir/${subject}_${fscan}_icafix.sh
	putFile=$outDir/${subject}_${fscan}_icafix_put.sh

	touch $outFile
	touch $putFile

	if [ ! -f $outFile ] ; then
	  echo "File at $outFile doesnt exist. Aborting!"
	  exit 1;
	fi
	if [ ! -f $putFile ] ; then
	  echo "File at $putFile doesnt exist. Aborting!"
	  exit 1;
	fi

	cat $configurationForJobFile > $outFile
	cat $putConfigurationForJobFile > $putFile

	cat $logDirectiveFile >> $outFile	
	cat $logDirectiveFile >> $putFile	

	workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
	if [ $? -ne 0 ] ; then
		echo "Fetching workflow for functional failed. Aborting!"
		exit 1
	fi 

	commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline $PIPELINE_NAME -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $useremail -notify $adminemail -parameter adminemail=$adminemail -parameter useremail=$useremail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject  -parameter BP=$icafix_bp -parameter functseries=$fscan -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir  -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix -workFlowPrimaryKey $workflowID "


	echo "Creating $outFile" 

	echo "echo \" \"" >> $outFile
	echo "$commandStr" >> $outFile
	echo "rc_command=\$?" >> $outFile

	echo "echo \$rc_command \" \"" >> $outFile
	echo "echo \"Job finished  at \`date\`\"" >> $outFile


	echo "exit \$rc_command" >> $outFile

	chmod +x $outFile

###########################################################
# Create PUT JOB
#
###########################################################

	putCommandStr="$commandStr -startAt 6 "


	echo "Creating $putFile" 

	echo "echo \" \"" >> $putFile
	echo "$putCommandStr" >> $putFile
	echo "rc_command=\$?" >> $putFile

	echo "echo \$rc_command \" \"" >> $putFile
	echo "echo \"Job finished  at \`date\`\"" >> $putFile


	echo "exit \$rc_command" >> $putFile


	chmod +x $putFile
	
	
	index=$((index+1))

done


################################################################################################
# Create Package Creation JOB - this is done once AFTER all the FIX are run
#
################################################################################################
	fix_packaging_outFile=$outDir/${subject}_icafix_package.sh

	if [ ! -f $fix_packaging_outFile ] ; then
	   touch $fix_packaging_outFile
	   cat $fix_configurationForJobFile > $fix_packaging_outFile
	   cat $logDirectiveFile >> $fix_packaging_outFile	

	   xnat_hostRoot=`echo $xnat_host | awk '{gsub(/http:\/\//,""); gsub(/:8080\//,""); gsub(/https:\/\//,""); gsub(/\//,""); print}'`
	   packaging_builddir=$builddir/packaging
	   mkdir -p $packaging_builddir
           echo "Creating $fix_packaging_outFile" 

           packaging_commandStr="source $SCRIPTS_HOME/epd-python_setup.sh; source $SCRIPTS_HOME/groovy_setup.sh;   $NRG_PACKAGES/tools/packaging/callPackager.sh --host $xnat_hostRoot --user $user --pw $passwd --outDir $packaging_outdir --buildDir $builddir --project $project --subject $subject --outputFormat PACKAGE --outputType FIX"

	   echo "echo \" \"" >> $fix_packaging_outFile
	   echo "$packaging_commandStr" >> $fix_packaging_outFile

	   echo "echo \" \"" >> $fix_packaging_outFile
	   echo "echo \"Job finished  at \`date\`\"" >> $fix_packaging_outFile

	   echo "exit 0;" >> $fix_packaging_outFile
	   chmod +x $fix_packaging_outFile
        fi
	


exit 0

