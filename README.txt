########################################################
#                      README 
########################################################

The purpose of the this patch is to customize various pipeline scripts specific to use in NRG. 
The idea is that this script would be used by various other projects within NRG deploying pipeline engine. 

March 25, 2013 
**************

At this time, this repo contains PIPELINE_HOME/bin/schedule which can push jobs onto NRG cluster using DRMAA and CHPC using SSH. 
This script will need a config file. The config file is expected to be in each XNAT projects pipeline patches repo. 

